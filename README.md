# Shield
NOTICE:  THIS WEBSITE IS **GROUP PROJECT WITH NO DESIGNERS** FOR  **EDUCATIONAL PURPOSES ONLY**. <br/>REFERENCE **https://www.airtasker.com**<br/>including:<br/> 

spacing(margin, padding),
 
font-size,

color,

font-weight,

border,

box-shadow,

business workflow,

Some animation effect

* Build by React 16.12
* Project has UnitTest


## Getting Started
* npm start

### Prerequisites

```
NPM
```

### Installing

A step by step series of examples that tell you how to get a development env running
Say what the step will be

```
npm start
```

## Run tests

Explain how to run the automated tests for this system

```
npm run test
```

## Coding style

```
BEM
```

## Deployment
 
 - npm run build
 - npm run deploy

## Contributing

Please read 

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Kitman Yiu** - *[Contribute link](https://docs.google.com/document/d/1VnUMDLDbCeAo5rNOL0A5CpU8jNo9b22vTpgYNYrXNg0/edit?usp=sharing)* - [https://kitmanyiu.com](https://kitmanyiu.com)
* **Jack We** - *Initial work* - []()
* **Yingkun Tan** - *[Contribute link](https://drive.google.com/file/d/1iUk29mi8dlZYZGtAVuh8qjKEIQeZso2V/view?usp=sharing)*


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* [AirTasker](https://www.airtasker.com)
* [MadeComfy](https://madecomfy.com.au/)
