import * as React from "react/cjs/react.development";
import RateStar from "../../components/Offers/RateStar/RateStar";
import FailModal from "../../workflow/CreateOfferWorkflow/CreateOfferWorkflow";

class TaskErrorBoundary extends React.Component {
    constructor(props) {
        super(props);
        this.state = {hasError: false, errorMessage: ''};
    }

//     static getDerivedStateFromError(error) {
// // Update state so the next render will show the fallback UI.
//         return {hasError: true};
//     }

    componentDidCatch(error, errorInfo) {
        console.log("abc");
        this.setState({hasError: true, errorMessage: error});
    }

    failClick = () => {
        this.setState({failed: false});
    };


    render() {

        if (this.state.hasError) {
            return <FailModal title={"Error"} content={"I guess you need to try again?"} click={this.failClick}/>;
        }

        return this.props.children;
    }
}


export default TaskErrorBoundary;
