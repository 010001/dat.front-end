import React from 'react';
import './App.css';
import {Route, Switch, BrowserRouter as Router} from 'react-router-dom';
import CreateEvent from "./pages/Event/CreateEvent/CreateEvent";
import ViewEvent from "./pages/Event/ViewEvent/ViewEvent";
import ViewTasks from "./pages/Tasks/ViewTasks/ViewTasks";
import Navigation from "./components/Navigation/Navigation";
import ErrorPage from "./pages/ErrorPage/404"
import Home from "./pages/Home/Home";
import SignUp from "./pages/Register/Register";
import Sub_SignUp from "./pages/Register/Sub__Register";
import Login from "./pages/Login/Login";
import * as action from "./store/actions";
import {connect} from "react-redux";
import User from "./pages/User/User";
import UserStatic from "./pages/User/UserStatic";
import About from "./pages/About/About";
import MyTasks from "./pages/Tasks/MyTasks/MyTasks"
import HowItWorks from "./pages/HowItWorks/HowItWorks";
import Careers from "./pages/Careers/Careers";
import Logout from "./pages/Logout/Logout";
import ProtectedRoute from "./routes/ProtectedRoute/ProtectedRoute";
import PaymentHistory from "./pages/PaymentHistory/PaymentHistory";
import Terms from "./pages/Terms/Terms";

import './Animation.css';
import './Animation.js'
import Notice from "./pages/Notice/Notice";

class App extends React.Component {

    componentDidMount() {
        this.props.onTryAutoSettings();
         this.props.onTryAutoSignup();
    }
    //React:https://reactjs.org/docs/fragments.html
    render() {
        this.props.onTryAutoSignup();
        return (
            <React.Fragment>
                <Router>
                <Navigation/>
                <Switch>
                    <Route path="/" exact component={Home}/>
                    <Route path="/create-event/:type" exact component={CreateEvent}/>
                    {/*protected route*/}
                    <Route path="/event/:slug" exact component={ViewEvent}/>
                    <Route path="/view-tasks" exact component={ViewTasks}/>
                    <Route path="/sign-up" exact component={SignUp}/>
                    <Route path="/my-tasks" exact component={MyTasks}/>
                    <Route path="/sub-sign-up" exact component={Sub_SignUp}/>
                    <Route path="/login" exact component={Login}/>
                    <ProtectedRoute path="/user" exact component={User}/>
                    <Route path="/user/:id" exact  component={UserStatic}></Route>
                    <Route path="/About" component={About}/>
                    <Route path="/how-it-works" component={HowItWorks}/>
                    <Route path="/careers" component={Careers}/>
                    <Route path="/logout" component={Logout}/>
                    <Route path="/terms" component={Terms}/>

                    <Route path='/account/payment-history' component={PaymentHistory} />

                    <Route component={ErrorPage}/>
                </Switch>
                </Router>
            </React.Fragment>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onTryAutoSettings: () => dispatch(action.getSettings()),
        onTryAutoSignup: () => dispatch(action.authCheckState()),
    };
};
export default connect(null, mapDispatchToProps)(App);
