import {updateObject} from "../utility";
import * as actionTypes from "../actions/actionTypes";

const initialState = {
    isModalOpen: false,
    isSecondModalOpen: false,
    isThirdModalOpen: false,
};

const openModal = (state, action) => {
    return updateObject(state, {isModalOpen: true, isSecondModalOpen:false, isThirdModalOpen: false});
};

const openSecondModal = (state, action) => {
    return updateObject(state, {isModalOpen: false, isSecondModalOpen:true, isThirdModalOpen:false});
}

const openThirdModal = (state, action) => {
    return updateObject(state, {isModalOpen: false, isSecondModalOpen:false, isThirdModalOpen:true});
}
const closeModal = (state, action) => {
    return updateObject(state, {isModalOpen: false, isSecondModalOpen:false, isThirdModalOpen:false});
};

const reducer = (state = initialState, action) => {

    switch (action.type) {
        case actionTypes.OPEN_MODAL:
            return openModal(state, action);
        case actionTypes.CLOSE_MODAL:
            return closeModal(state, action);
        case actionTypes.OPEN_SECOND_MODEL:
            return openSecondModal(state, action);
        case actionTypes.OPEN_THIRD_MODEL:
            return openThirdModal(state, action);
        default:
            return state;
    }
};

export default reducer;
