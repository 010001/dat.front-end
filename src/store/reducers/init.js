import * as actionTypes from "../actions/actionTypes";
import {updateObject} from "../utility";
import axios from "axios";
import configuration from "../../config/config";

const initialState = {
    settings: null,
};

const settings = async (state, action) => {
    const response = await axios.get(configuration.api.backend_api + '/api/v1/settings');
    return updateObject(state, {settings: response});
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.GET_SETTINGS:
            return settings(state, action);
        default:
            return state;
    }
};

export default reducer;
