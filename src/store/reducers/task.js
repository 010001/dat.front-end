/* What should do */

import * as actionTypes from '../actions/actionTypes';
import {updateObject} from "../utility";

const initialState = []

const authStart = (state, action) => {
    return updateObject(state, {error: null, loading: true});
};

const authSuccess = (state, action) => {
    return updateObject(state, {
        token: action.idToken,
        profileName: action.displayName === "" ? "Kitman Yiu" : action.displayName,
        userId: action.userId,
        error: null,
        loading: false,
    });

};
const authFail = (state, action) => {
    return updateObject(state, {error: action.error, loading: false});
};

const authLogout = (state, action) => {
    return updateObject(state, {token: null, userId: null});
};

const reducer = (state = initialState, action) => {
    const payload = action.payload;
    switch (action.type) {
        case actionTypes.AUTH_START:
            return authStart(state, action);
        case actionTypes.TASKS_SUCCESS:
            return [...state, payload]
        case actionTypes.TASK_SUCCESS:
            const taskId = 'id';
            const offerId = 'id';
            return state.map(d => {
                if(d.id === taksId){
                    return {...d, d.offer.map(o => {
                        comments: pggiayload
                    })}
                }
                return d
            })
           // const index = initialState.findIndex(d => d.id = payload.id)
        case actionTypes.AUTH_FAIL:
            return authFail(state, action);
        case actionTypes.AUTH_LOGOUT:
            return authLogout(state, action);
        default:
            return state;
    }
};



export default reducer;
