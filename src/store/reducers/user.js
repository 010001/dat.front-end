import * as actionTypes from '../actions/actionTypes';
import {updateObject} from "../utility";
import { combineReducers } from 'redux';

var initialState = {
        email: "",
        password: "",
        name: {
                lastName: "",
                firstName: "",
        },
        address: {
                state: "",
                suburb: "",
                country: "",
        },
        about: {
                tagLine: "",
                description: "",
        },
        age: "",
        images: "",
        portfolio: [],
        skills:[],
}
export const fetching = (state=false, action) =>{
        switch(action.type){
                case actionTypes.FETCH_SUBURB_NAMES:
                        return true;
                case actionTypes.CHANGE_SUGGESTIONS:
                        return false;
                default:
                        return state;
        }
}
export const suggestions = (state = [], action) => {
        switch(action.type){
                case actionTypes.CLEAR_SUGGESTIONS:
                        return [];
                case actionTypes.CHANGE_SUGGESTIONS:
                        return action.payload;
                default:
                        return state;
        }
}
export const universities = (state = [], action) => {
        switch(action.type){
                case actionTypes.SEARCH_UNIVERSITY_NAMES:
                        return action.payload;
                case actionTypes.CLEAR_SUGGESTIONS:
                        return [];
                default:
                        return state;
        }
}
export const images = (state ="", action) => {
        switch(action.type){
                case actionTypes.CHANGE_USER_IMAGE:
                        return action.image;
                default:
                        return localStorage.getItem("userImage");
        }
}
export const transportations = (state = [], action) => {
        switch(action.type){
                case actionTypes.ADD_TRANSPORTATION:
                        return [...state, action.transport];
                case actionTypes.REMOVE_TRANSPORTATION:
                        state = state.filter(function(item){
                                return item !== action.transport;
                        })
                        return [...state];
                default:
                        return state;
        }
}

export const types = (state = "", action) =>{
        switch(action.type){
                case actionTypes.ADD_TRANSPORTATION:
                        return action.transport;
                case actionTypes.REMOVE_TRANSPORTATION:
                        return "";
                default:
                        return state;
        }
}

export const addressNames =  combineReducers({
        fetching,
        suggestions
})

const reducer = (state =initialState, action) => {
        switch(action.type){
                case actionTypes.REGISTER_USER:
                        state = updateObject(state, {password: action.password, email: action.email})
                        return state;
                case actionTypes.REGISTER_USER_SUB:
                        state = updateObject(state, {address: action.address, name:action.name})
                        return state;
                default:
                        return state;
        }
}

export default reducer;