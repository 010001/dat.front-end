import * as actionTypes from './actionTypes';
import axios from 'axios';
import configuration from "../../config/config";
import decode from "jwt-decode"
export const authStart = () => {
    return {
        type: actionTypes.AUTH_START,
    };
};

export const authSuccess = (token, userId, displayName) => {
    return {
        type: actionTypes.AUTH_SUCCESS,
        idToken: token,
        userId: userId,
        displayName: displayName
    };
};

export const authFail = (error) => {
    return {
        type: actionTypes.AUTH_FAIL,
        error: error,
    };
};
export const logout = () => {
    localStorage.removeItem('token');
    localStorage.removeItem('expirationDate');
    localStorage.removeItem('userId');
    localStorage.removeItem("userImage");
    localStorage.removeItem("userName");
    return {
        type: actionTypes.AUTH_LOGOUT
    };
};

export const checkAuthTimeout = (expirationTime) => {
    return dispatch => {
        setTimeout(() => {
            dispatch(logout());
        }, expirationTime);
    };
};

export const auth = (email, password, isSignup) => {
    return dispatch => {
        dispatch(authStart());
        const authData = {
            email: email,
            password: password,
            returnSecureToken: true
        };
        let url = 'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyCA09BIX4hedS0NTjmoC2oaQ_CmD8KWIA4';
        if (!isSignup) {
            // url = 'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyCA09BIX4hedS0NTjmoC2oaQ_CmD8KWIA4';
            url =configuration.api.backend_api+ "/api/v1/users/signIn";
        }
        axios.post(url, authData).then(response => {
            const {exp} = decode(response.data.token);
            var expirationDate = new Date();
            var t_s = new Date().getTime();
            expirationDate.setTime(t_s +exp/10);
            localStorage.setItem('token', response.data.token);
            localStorage.setItem('expirationDate', expirationDate);
            localStorage.setItem('userId', response.data.user._id);
            localStorage.setItem("userName", response.data.user.name.firstName + " " + response.data.user.name.lastName);
            localStorage.setItem("userImage", response.data.user.images);
            dispatch(authSuccess(response.data.token, response.data.user._id, 
                    response.data.user.name.firstName +" "+response.data.user.name.lastName));
            //dispatch(checkAuthTimeout(response.data.expiresIn));
        }).catch(err => {
            dispatch(authFail(err));
        });
    };
};

export const socialFBAuth = (user, email, images, firstName,lastName, ID, accessToken) => {
    return async  dispatch => {
        dispatch(authStart());
            const modiUser = {
                ...user.data,
                images,
                name:{
                    firstName,
                    lastName
                },
                FaceBook:{
                    email: email,
                    accessToken: accessToken,
                    FaceBookID:ID,
                },
                token: localStorage.getItem("token"),
                _id: localStorage.getItem("userId"),
            }
            const modify = await axios.put(configuration.api.backend_api+ `/api/v1/users/updateOne`, modiUser);
            localStorage.setItem("userImage", modify.data.user.images);
            localStorage.setItem("userName", modify.data.user.name.firstName + " " + modify.data.user.name.lastName);
            dispatch(authSuccess(modify.data.token, modify.data.user._id, 
                modify.data.user.name.firstName +" "+modify.data.user.name.lastName));
        }
}

export const socialGoogleAuth = (user, email, images, firstName,lastName, ID, accessToken) => {
    return async  dispatch => {
        dispatch(authStart());
            const modiUser = {
                ...user.data,
                images,
                name:{
                    firstName,
                    lastName
                },
                Google:{
                    email: email,
                    accessToken: accessToken,
                    GoogleID:ID,
                },
                token: localStorage.getItem("token"),
                _id: localStorage.getItem("userId"),
            }
            const modify = await axios.put(configuration.api.backend_api + `/api/v1/users/updateOne`, modiUser);
            localStorage.setItem("userImage", modify.data.user.images);
            localStorage.setItem("userName", modify.data.user.name.firstName + " " + modify.data.user.name.lastName);
            dispatch(authSuccess(modify.data.token, modify.data.user._id, 
                modify.data.user.name.firstName +" "+modify.data.user.name.lastName));
        }
}

export const googleAuthLogin = (user, userId, token) => {
        return async dispatch => {
            localStorage.setItem("userId", userId);
            localStorage.setItem("token", token);

            const {exp} = decode(user.data.token);
            var expirationDate = new Date();
            var t_s = new Date().getTime();
            expirationDate.setTime(t_s +exp/10);
            localStorage.setItem('expirationDate', expirationDate);
            localStorage.setItem("userImage", user.data.user.images);
            localStorage.setItem("userName", user.data.user.name.firstName + " " + user.data.user.name.lastName);
            dispatch(authSuccess(token, userId, 
                user.data.user.name.firstName +" "+user.data.user.name.lastName));
        }
}

export const authCheckState = () => {
    return dispatch => {
        const token = localStorage.getItem('token');
        if (!token) {
            dispatch(logout());
        } else {
            const expirationDate = new Date(localStorage.getItem('expirationDate'));
            if (expirationDate <= new Date()) {
                dispatch(logout());
            } else {
                const userId = localStorage.getItem('userId');
                dispatch(authSuccess(token, userId, localStorage.getItem("userName")));
                dispatch(checkAuthTimeout(expirationDate.getTime() - new Date().getTime()));
            }
        }
    }
};
