import * as actionTypes from './actionTypes';
/* What state do */

export const openModal = () => {
    return {
        type: actionTypes.OPEN_MODAL,
    }
};

export const closeModal = () => {
    return {
        type: actionTypes.CLOSE_MODAL,
    }
};

export const openThirdModal = () => {
        return {
            type: actionTypes.OPEN_THIRD_MODEL,
        }
}

export const openSecondModal = () => {
    return {
        type: actionTypes.OPEN_SECOND_MODEL
    }
   
}

