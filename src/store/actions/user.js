import * as actionTypes from './actionTypes';
import axios from 'axios';

export const registerUser = (email, password) => {
        return {
                type: actionTypes.REGISTER_USER,
                email : email,
                password: password,
        }
}

export const registerSubUser = async (firstName, lastName, suburb, state, country) => {
        return {
                type: actionTypes.REGISTER_USER_SUB,
                name: {
                        firstName: firstName,
                        lastName: lastName
                },
                address: {
                        suburb: suburb,
                        country: country,
                        state: state
                }
        }
}

export const changeSuggestions = suggestions => ({
        type: actionTypes.CHANGE_SUGGESTIONS,
        payload: suggestions
})

export const searchSuburbNames = value => dispatch => {
        dispatch({
                type: actionTypes.FETCH_SUBURB_NAMES
        })
        axios.get("https://raw.githubusercontent.com/michalsn/australian-suburbs/master/data/suburbs.json")
        .then(suggestions => {
                var partSuggestions = [];
                partSuggestions = suggestions.data.data.filter(function(item){
                        let length = value.length;
                        if (item.suburb.slice(0, length).toLowerCase() === value.toLowerCase()){
                        }
                        return item.suburb.slice(0, length).toLowerCase() === value.toLowerCase();
                })
                dispatch({
                        type: actionTypes.CHANGE_SUGGESTIONS,
                        payload:partSuggestions,
                })
                }).catch(error => {
                        console.log(error);
                })
        } 

export const addTransportation = ({transport}) =>{
        return {
                type: actionTypes.ADD_TRANSPORTATION,
                transport
        }
}
export const clearSuggestions = () => ({
        type: actionTypes.CLEAR_SUGGESTIONS,
})
export const removeTransportation = ({transport}) => {
        return {
                type: actionTypes.REMOVE_TRANSPORTATION,
                transport
        }
}

export const searchUniversities = value => dispatch => {
        axios.get("https://raw.githubusercontent.com/doselect/data/master/universities.json")
                .then(universities => {
                        
                        var unis = universities.data.filter(function(item){
                                var length = value.length;
                                return item.name.slice(0, length).toLowerCase() === value.toLowerCase();
                        });
                        dispatch({
                                type:actionTypes.SEARCH_UNIVERSITY_NAMES,
                                payload: unis,
                        })
                }).catch(error => {
                        console.log(error);
                })
}

export const changeUserImage = (image) => {
        localStorage.setItem("userImage", image);
        return {
                type: actionTypes.CHANGE_USER_IMAGE,
                image
        }
}
