import React from 'react';
import './PaymentWorkflow.css';
import Modal from "../../components/UI/Modal/Modal";
import {connect} from "react-redux";
import * as action from "../../store/actions";

import {PaymentSuccess} from "../../components/Modal/PaymentSuccess/PaymentSuccess";
import {PaymentModal} from "../../components/Modal/PaymentModal/PaymentModal";
import {FailModal} from "../../components/Modal/FailModal/FailModal";


export class PaymentWorkflow extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            steps: [<PaymentModal price={this.props.price} modalClose={this.props.close}
                                  successPayment={this.paymentSuccess}
                                  failPayment={this.paymentFailed} payerUserID={props.payerUserID} payeeUserID={props.payeeUserID} taskID={props.taskID}/>, <PaymentSuccess click={this.props.close}/>],
            currentStep: 0,
            failed: false,
        };
    }

    paymentFailed = () => {
        this.setState({failed: true});
    };


    paymentSuccess = async () => {
        this.setState({currentStep: 1});
    };

    failClick = () => {
        this.setState({failed: false});
    };

    completed = () => {
        this.props.success();
        this.props.close();
    };

    render() {
        return (
            <Modal show={true} class={"modal--accept"}>
                {!this.state.failed ? this.state.steps[this.state.currentStep] :
                    <FailModal title={"Error"} content={"I guess you need to try again?"} click={this.failClick}/>}
            </Modal>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        closeModal: () => dispatch(action.closeModal())
    };
};

const mapStateToProps = (state) => {
    return {
        isModalOpen: state.modal.isModalOpen,
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(PaymentWorkflow);

