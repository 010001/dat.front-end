import Enzyme, {configure, mount, shallow} from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';

import React from "react";
import moxios from 'moxios';
import {PaymentWorkflow} from "./PaymentWorkflow";
import StripeCheckout from 'react-stripe-checkout';

Enzyme.configure({adapter: new EnzymeAdapter()});


describe('Payment Offer Workflow', () => {
    beforeEach(() => {
        moxios.install();
    });

    afterEach(() => {
        moxios.uninstall();
    });

    const setup = (props = {}, state = null) => {
        const wrapper = mount(<PaymentWorkflow {...props} />);
        if (state) wrapper.setState(state);
        return wrapper;
    };

    const findByTestAttr = (wrapper, val) => {
        return wrapper.find(`[data-test="${val}"]`);
    };

    const findComponment = (wrapper, comp) => {
        return wrapper.find(comp);
    };

    test('renders without error', () => {
        const wrapper = setup({price:50,currentUserID:"", paymentUserID:"dsf"});
        const component = findComponment(wrapper, PaymentWorkflow);
        expect(component.length).toBe(1);
    });

    test('test success workflow', () => {
        const wrapper = setup({id: "5de068040d89e700176d588b"});
        const btn = findComponment(wrapper, StripeCheckout);
        expect(btn.length).toBe(1);
    });
});
