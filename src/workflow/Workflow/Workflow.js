import React from 'react';
import './Workflow.css';
import {connect} from "react-redux";
import * as action from "../../store/actions";
import FailModal from "../../components/Modal/FailModal/FailModal";

class Workflow extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            steps: props.steps,
            currentStep: 0,
            failed: false,
        };
    }

    failClick = () => {
        this.setState({failed: false});
    };

    closeModal = () => {
        this.setState({failed: false});
    };

    render() {
        return (
            !this.state.failed ? this.props.steps[this.state.currentStep] :
                    <FailModal title={"Error"} content={"I guess you need to try again?"} click={this.failClick}/>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        closeModal: () => dispatch(action.closeModal())
    };
};

const mapStateToProps = (state) => {
    return {
        isModalOpen: state.modal.isModalOpen,
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Workflow);

