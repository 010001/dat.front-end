import React from 'react';
import './UpdateEventWorkflow.css';
import {connect} from "react-redux";
import * as action from "../../store/actions";
import {FailModal} from "../../components/Modal/FailModal/FailModal";
import {SuccessModal} from "../../components/Modal/SuccessModal/SuccessModal";
import {UpdateEvent} from "../../pages/Event/UpdateEvent/UpdateEvent";


export class UpdateEventWorkflow extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            steps: [<UpdateEvent data={this.props.data} success={this.updateTasks} failed={this.submitFailed} cancelClick={this.props.cancelClick} id={props.id}/>,
                <SuccessModal click={this.completed}/>],
            currentStep: 0,
            failed: false,
            data:[]
        };
    }

    updateTasks = async (data) => {
        this.setState({currentStep: 1,data});
    };

    submitFailed = () => {
        this.setState({failed: true});
    };


    failClick = () => {
        this.setState({failed: false});
    };

    completed = () => {
        this.props.closeModal();
        this.setState({currentStep: 0});
        this.props.success(this.state.data);
    };

    render() {
        return (!this.state.failed ? this.state.steps[this.state.currentStep] :
            <FailModal title={"Error"} content={"I guess you need to try again?"} click={this.failClick}/>);
    }
}

const mapDispatchToProps = dispatch => {
    return {
        closeModal: () => dispatch(action.closeModal())
    };
};

const mapStateToProps = (state) => {
    return {
        isModalOpen: state.modal.isModalOpen,
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(UpdateEventWorkflow);

