import Enzyme, {configure, mount, shallow} from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';

import React from "react";
import moxios from 'moxios';
import {UpdateEventWorkflow} from "./UpdateEventWorkflow";


Enzyme.configure({adapter: new EnzymeAdapter()});


describe('Update Event Workflow', () => {
    beforeEach(() => {
        moxios.install();
    });

    afterEach(() => {
        moxios.uninstall();
    });

    const setup = (props = {}, state = null) => {
        const wrapper = mount(<UpdateEventWorkflow {...props} />);
        if (state) wrapper.setState(state);
        return wrapper;
    };

    const findByTestAttr = (wrapper, val) => {
        return wrapper.find(`[data-test="${val}"]`);
    };

    const findComponment = (wrapper, comp) => {
        return wrapper.find(comp);
    };

    test('renders without error', () => {
        const wrapper = setup();
        const component = findComponment(wrapper, UpdateEventWorkflow);
        expect(component.length).toBe(1);
    });

    test('test success workflow', (done) => {
        const wrapper = setup({id: "5de068040d89e700176d588b"});

        const btn = findByTestAttr(wrapper, 'updateEvent');
        btn.simulate('click');

        moxios.wait(function () {
            let request = moxios.requests.mostRecent();
            request.respondWith({
                status: 201,
                response: {
                    "offer": {
                        "_id": "5dedc9e212d3510017ca1896",
                        "user_id": {
                            "name": "xiaoyang",
                            "address": {
                                "suburb": "",
                                "state": "",
                                "country": ""
                            },
                            "about": {
                                "tagLine": "",
                                "description": ""
                            },
                            "FaceBook": {
                                "email": "",
                                "FaceBookID": "",
                                "accessToken": ""
                            },
                            "Google": {
                                "email": "",
                                "GoogleID": "",
                                "accessToken": ""
                            },
                            "age": "18",
                            "images": "",
                            "rating": 0,
                            "totalPeople": 0,
                            "totalRating": 0,
                            "portfolio": [],
                            "phoneNumber": "",
                            "_id": "5dc0f6b9148a102309ac460e",
                            "email": "2518574582@qq.com",
                            "createdAt": "2019-11-05T04:12:41.131Z",
                            "updatedAt": "2019-11-05T04:12:41.131Z",
                            "__v": 0,
                            "skills": [],
                            "badges": []
                        },
                        "price": 100,
                        "comments": "bvcbbbvbbvcb",
                        "createdAt": "2019-12-09T04:13:22.572Z",
                        "updatedAt": "2019-12-09T04:13:22.572Z",
                        "__v": 0
                    }
                }
            }).then(function () {
                wrapper.update();
                const component = findByTestAttr(wrapper, 'success');
                expect(component.length).toBe(1);
                done();
            });
        });
    });

    test('test fail workflow', (done) => {
        const wrapper = setup({id: "5de068040d89e700176d588b"});

        const btn = findByTestAttr(wrapper, 'updateEvent');
        btn.simulate('click');

        moxios.wait(function () {
            let request = moxios.requests.mostRecent();
            request.respondWith({
                status: 500,
                response: {
                }
            }).then(function () {
                wrapper.update();
                const component = findByTestAttr(wrapper, 'fail');
                expect(component.length).toBe(1);
                done();
            });
        });
    });

// describe('<CreateOfferWorkflow/>', () => {
//     let wrapper;
//
//     beforeEach(() => {
//         wrapper = shallow(<CreateOfferWorkflow/>);
//     });
//
//     it('should show create Offer', () => {
//         const input = wrapper.find("[data-test='price']");
//         input.value = '11';
//         expect(input.value).toBe('11');
//
//         const input2 = wrapper.find("[data-test='comments']");
//         input2.value = 'abd';
//         expect(input2.value).toBe('abd');
//
//         //expect(wrapper.find(CreateOffer)).toHaveLength(1);
//     })
// });
});
