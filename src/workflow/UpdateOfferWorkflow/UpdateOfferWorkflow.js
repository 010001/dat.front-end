import React from 'react';
import './UpdateOfferWorkflow.css';

import {connect} from "react-redux";
import * as action from "../../store/actions";
import {FailModal} from "../../components/Modal/FailModal/FailModal";
import {UpdateOffer} from "../../components/Offers/UpdateOffer/UpdateOffer";
import {SuccessModal} from "../../components/Modal/SuccessModal/SuccessModal";
//Styled Components, CSS Modules
export class UpdateOfferWorkflow extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            steps: [<UpdateOffer success={this.submitSuccess} failed={this.submitFailed}
                                 cancelClick={this.props.closeModal} id={props.offerId}/>,
                <SuccessModal click={this.completed}/>],
            currentStep: 0,
            failed: false,
            data: []
        };
    }

    submitSuccess = (data) => {
        this.setState({currentStep: 1, data});
    };

    submitFailed = () => {
        this.setState({failed: true});
    };

    failClick = () => {
        this.setState({failed: false});
    };

    completed = () => {
        this.props.closeModal();
        this.setState({currentStep: 0});
        this.props.success(this.state.data);
    };

    render() {

        return (!this.state.failed ? this.state.steps[this.state.currentStep] :
                <FailModal title={"Error"} content={"I guess you need to try again?"} click={this.failClick}/>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        closeModal: () => dispatch(action.closeModal())
    };
};

const mapStateToProps = (state) => {
    return {
        isModalOpen: state.modal.isModalOpen,
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(UpdateOfferWorkflow);


// function create({steps}) {
//     class UpdateOfferWorkflow extends React.Component {
//         constructor(props) {
//             super(props);
//             console.log(props);
//             this.state = {
//                 currentStep: 0,
//                 failed: false,
//                 data: []
//             };
//         }
//
//         submitSuccess = (data) => {
//             this.setState({currentStep: 1, data});
//         };
//
//         submitFailed = () => {
//             this.setState({failed: true});
//         };
//
//         failClick = () => {
//             this.setState({failed: false});
//         };
//
//         completed = () => {
//             this.props.closeModal();
//             this.props.success(this.state.data);
//         };
//
//         render() {
//             if (steps[this.state.currentStep] === 'UpdateOffer') {
//                 return <UpdateOffer success={this.submitSuccess} failed={this.submitFailed}
//                                     cancelClick={this.props.closeModal} id={props.offerId}/>
//             }
//             if (steps[this.state.currentStep] === 'SuccessModal') {
//                 return <SuccessModal click={this.completed}/>;
//             } else
//                 return <div>no such step</div>;
//
//             return (!this.state.failed ? this.state.steps[this.state.currentStep] :
//                     <FailModal title={"Error"} content={"I guess you need to try again?"} click={this.failClick}/>
//             );
//         }
//     }
//
//     const mapDispatchToProps = dispatch => {
//         return {
//             closeModal: () => dispatch(action.closeModal())
//         };
//     };
//
//     const mapStateToProps = (state) => {
//         return {
//             isModalOpen: state.modal.isModalOpen,
//         }
//     };
//
//     return connect(mapStateToProps, mapDispatchToProps)(UpdateOfferWorkflow);
// }
//
// export const Update = create(["UpdateOffer", "SuccessModal"]);
// export const Create = create([<CreateOffer success={this.submitSuccess} failed={this.submitFailed}
//                                            cancelClick={this.props.closeModal} id={props.id}/>,
//     <SuccessModal click={this.completed}/>]);

