import React from 'react';
import './JoinEventWorkflow.css';
import {connect} from "react-redux";
import * as action from "../../store/actions";
import FailModal from "../../components/Modal/FailModal/FailModal";
import {SuccessModal} from "../../components/Modal/SuccessModal/SuccessModal";
import {CreateOffer} from "../../components/Offers/CreateOffer/CreateOffer";


export class JoinEventWorkflow extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            currentStep: 0,
            failed: false,
            data: []
        };
    }

    submitSuccess = (data) => {
        this.setState({currentStep: 1, data});
    };

    submitFailed = () => {
        this.setState({failed: true});
    };

    failClick = () => {
        this.setState({failed: false});
    };

    completed = () => {
        this.props.closeModal();
        this.props.success(this.state.data);
    };

    getStep(props) {
        const steps = [
            <CreateOffer success={this.submitSuccess} failed={this.submitFailed} id={props.id} userId={this.props.userId}/>,
            <SuccessModal click={this.completed}/>
        ];
        return steps[this.state.currentStep];
    }

    render() {
        return (
            !this.state.failed ? this.getStep(this.props) :
                <FailModal title={"Error"} content={"I guess you need to try again?"} click={this.failClick}/>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        closeModal: () => dispatch(action.closeModal())
    };
};

const mapStateToProps = (state) => {
    return {
        isModalOpen: state.modal.isModalOpen,
        userId: state.auth.userId
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(JoinEventWorkflow);

