import axios from "axios";
import configuration from "../config/config";

export const searchTask = async (search) => {
    let res = await axios.get(configuration.api.backend_api + `/api/v1/search?title=${search}`);
    return res.data;
};
