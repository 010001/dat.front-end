import axios from "axios";
import configuration from "../config/config";

export const getEvents = async (slug) => {
    let config = {
        headers: {'Access-Control-Allow-Origin': '*'}
    };
    let res = await axios.get(configuration.api.backend_api + '/api/v1/events', config);
    return res.data;
};

export const storeTask = () => {

};


export const createEvent = async (name, status, user_id, location, start_date, comments, price, slug, type_id) => {
    let config = {
        headers: {'Access-Control-Allow-Origin': '*'}
    };
    let res = await axios.post(configuration.api.backend_api + '/api/v1/events', {
        name,
        status,
        user_id,
        location,
        start_date,
        comments,
        price,
        slug,
        type_id
    }, config);
    return res.data.event;
};

export const viewEvent = async (slug) => {
    let config = {
        headers: {'Access-Control-Allow-Origin': '*'}
    };
    let res = await axios.get(configuration.api.backend_api + '/api/v1/events/' + slug, config);
    return res.data;
};

export const updateEvent = async (id, data) => {
    let res = await axios.put(configuration.api.backend_api + '/api/v1/events/' + id, data);
    return res;
};

export const completeEvent = async (data) => {
    return await axios.post(configuration.api.backend_api + '/api/v1/complete-task', data);
};
