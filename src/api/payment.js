import axios from "axios";
import configuration from "../config/config";

export const getPayment = async () => {
    let res = await axios.get(configuration.api.backend_api + '/api/v1/transactions/me');
    return res.data;
};
