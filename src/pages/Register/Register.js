import React from 'react';
// import {Route, withRouter, Link} from 'react-router-dom';
import { Link } from 'react-router-dom';
import './Register.css';
import axios from 'axios';
import { connect } from "react-redux";
import * as action from "../../store/actions";
import FacebookLogin from "react-facebook-login"
import configuration from "../../config/config";
import GoogleLogin from "react-google-login";
import { TiSocialFacebookCircular } from "react-icons/ti/index"
import PasswordStrengthChecker from '../../components/PasswordStrengthChecker/PasswordStrengthChecker'
import SquareAnimation from "../../components/SquareAnimation/SquareAnimation";

class Register extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: {
                elementConfig: {
                    placeholder: ""
                },
                validation: {
                    required: true,
                    isEmail: true
                },
                errorMessage: {
                    email: "Not valid Email",
                    required: "Email is not required"
                },
                valid: false,
                value: '',
                cssClass: '',
            },
            password: {
                elementConfig: {
                    placeholder: ''
                },
                validation: {
                    minLength: 9,
                    maxLength: 25,
                    required: true
                },
                errorMessage: {
                    minLength: "The password is too short",
                    maxLength: "The password is too long",
                    required: 'Password is required',
                },
                valid: false,
                value: "",
                cssClass: ""
            },
            checked: false,
            images: "",
            errorClass: "no-active",
            errormsg:false,
            errormsgClass:"no-active",
            showPasswordCheckBox: false,
        }
        //React:https://reactjs.org/docs/refs-and-the-dom.html
        //React:https://reactjs.org/docs/forwarding-refs.html(did not use but maybe useful pass ref to child component)
        this.Signup = React.createRef("Signup");
    }
    checkValidity(value, rules) {
        let isValid = true;
        if (!rules) {
            return true;
        }

        if (rules.required) {
            isValid = value.trim() !== '' && isValid;
        }

        if (rules.minLength) {
            isValid = value.length >= rules.minLength && isValid
        }

        if (rules.maxLength) {
            isValid = value.length <= rules.maxLength && isValid
        }
        if (rules.isEmail) {
            // const pattern = /^([A-Za-z0-9_\-\.\u4e00-\u9fa5])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,8})$/;
            // isValid = pattern.test(value) && isValid
            //invaild pattern format cause warning, double check with it
        }
        if (rules.isNumeric) {
            const pattern = /^\d+$/;
            isValid = pattern.test(value) && isValid
        }

        return isValid;
    }

    handleSubmit = (e) => {
        e.preventDefault();
        const { email, password } = this.state;
        var _this = this;
        axios.post(configuration.api.backend_api+`/api/v1/users/signUp`, {email:email.value, password:password.value}).then(res => {
                if (res.status === 201) {
                    this.setState({errorClass:"no-active"});
                    this.setState({ errormsg:false });
                    this.props.onAuth(this.state.email.value, this.state.password.value);
                    _this.props.history.push("/sub-sign-up");
                }
        }).catch(err => {
            if(email.value&&password.value){
            this.setState({ errormsg:false });
            this.setState({ errorClass: "errorClass" });
            }else if(email.value===""||password.value===""){
            this.setState({ errormsg:true });
            this.setState({ errormsgClass: "errorClass" });
            }
        })
    }
    onClose = (e) => {
    }

    onFocusPassword = () =>{
        this.setState({
            showPasswordCheckBox: true,
        })
    }

    onBlurPassword = () => {
        this.setState({
            showPasswordCheckBox: false,
        })
    }

    onChange = (e) => {
        const updatedFormElement = {
            ...this.state[e.target.name]
        };
        let isValid = this.checkValidity(e.target.value, updatedFormElement.validation);
        updatedFormElement.valid = isValid;
        if (!isValid) {
            updatedFormElement.cssClass = 'color-red';
        } else {
            updatedFormElement.cssClass = '';
        }
        updatedFormElement.value = e.target.value;
        this.setState({ [e.target.name]: updatedFormElement });
        // if (e.target.name === "password") {
        //     this.setState({
        //         showPasswordCheckBox: true,
        //     })
        //     // clearTimeout(mark);
        //     setTimeout(() => (this.setState({
        //         showPasswordCheckBox: false,
        //     })), 4000)
        // }
    };

    changeCheckboxStatus = (e) => {
        this.state.checked === true ? this.setState({ checked: false, }) : this.setState({ checked: true, })
    };
    responseFacebook = async (response) => {
        let { email, images } = this.state;
        email.value = response.email;
        images = response.picture.data.url
        let nameArray = response.name.split(" ");
        let firstName = nameArray[0];
        let lastName = nameArray[1];
        let FaceBookID = response.userID;
        try {
                const data = await axios.post(configuration.api.backend_api+"/api/v1/facebook", {FaceBook:{
                        email: email.value,
                        accessToken: response.accessToken,
                        FaceBookID,
                    }});
                this.props.socialLogin(data, data.data.user._id, data.data.token);
        } catch(err) {
                var _this = this;
                axios.post(configuration.api.backend_api + `/api/v1/users/signUp`, {email:email.value, password:response.accessToken}).then(
                    async res => {
                    if (res.status === 201) {
                        this.setState({errorClass:"no-active"});
                        await this.props.onAuth(this.state.email.value, response.accessToken);
                        await this.props.authFBSuccess(res, email.value, images, firstName, lastName, FaceBookID, response.accessToken);
                        _this.props.history.push("/sub-sign-up");
                    }
                }).catch(err => {
                    this.setState({errorClass:"errorClass"});
                })
        }
    }
    responseGoogle = async (response) => {
        let { email, images, password } = this.state;
        email.value = response.w3.U3.trim();
        password.value = response.accessToken;
        images = response.profileObj.imageUrl;
        let firstName = response.profileObj.givenName;
        let lastName = response.profileObj.familyName;
        let GoogleID = response.googleId;
        try {
            const data = await axios.post(configuration.api.backend_api+"/api/v1/google", {Google:{
                    email: email.value,
                    accessToken: password.value,
                    GoogleID,
                }});
            this.props.socialLogin(data, data.data.user._id, data.data.token);
        } catch(err) {
            var _this = this;
                axios.post(configuration.api.backend_api+`/api/v1/users/signUp`, {email:email.value, password:response.accessToken}).then(
                    async res => {
                    if (res.status === 201) {
                        this.setState({errorClass:"no-active"});
                        await this.props.onAuth(this.state.email.value, response.accessToken);
                        await this.props.authGoogleSuccess(res.data, email.value, images, firstName, lastName, GoogleID, password.value);
                        _this.props.history.push("/sub-sign-up");
                    }
                }).catch(err => {
                    this.setState({errorClass:"errorClass"});
                })
        }
    };
    render() {
        const { history } = this.props;
        if (this.props.isAuth) {
            history.goBack();
        }

        return (
            <div className="Signup_Background">
                <div className="Signup__Container">
                    <SquareAnimation/>
                    <div className="Signup flex flex__column">
                        <div id="title-box" className="title-box">
                            <p>加入我们</p>
                        </div>

                        <div className="Signup-body">
                            <form className="Signup-form flex flex__column">
                                <div className="Signup-form-field flex flex__column">
                                    <label>电子邮件: </label>
                                    <input id="email" name="email" placeholder="电邮" value={this.state.email.value}
                                        onChange={this.onChange}></input>
                                </div>
                                <div className="Signup-form-field flex flex__column ">
                                    <label>密码: </label>
                                    <input id="password" name="password" type="password" placeholder="密码"
                                        value={this.state.password.value} onChange={this.onChange} onFocus={this.onFocusPassword} onBlur={this.onBlurPassword}/>
                                    <div className={this.state.showPasswordCheckBox ? '' : 'componentUnVisible'}><PasswordStrengthChecker password={this.state.password} /></div>
                                </div>

                                <div style={{ color: 'red' }} className={this.state.errormsg? 'errorClass':'no-active'}>电子邮件或密码为必填项</div>
                                <div style={{ color: 'red' }} className={this.state.errorClass}>用户已经注册</div>
                                <button className="submit-btn" onClick={this.handleSubmit} >注册</button>
                                <div className="other-signup-field">
                                    <span id="other--option">或注册</span>
                                    <div className="external-signup-option flex flex__row flex--space-between">
                                        <FacebookLogin
                                            appId="581613205714087"
                                            fields="name,email,picture"
                                            autoLoad={false}
                                            callback={this.responseFacebook}
                                            cssClass="option-fb"
                                            textButton="Facebook"
                                            icon={<TiSocialFacebookCircular />}
                                        />
                                        <GoogleLogin
                                            clientId="265681817663-gt34057isbt70sj0ibo3baeu2k6h9usc.apps.googleusercontent.com"
                                            buttonText="Google"
                                            autoLoad={false}
                                            onSuccess={this.responseGoogle}
                                            onFailure={(this.responseGoogle)}
                                            cookiePolicy={'single_host_origin'}
                                            render={renderProps => (
                                                <button id="option-Gg" onClick={renderProps.onClick} disabled={renderProps.disabled}>
                                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                        <path
                                                            d="M22 12.23c0-.707-.063-1.393-.183-2.045h-9.613v3.871h5.491c-.235 1.253-.957 2.31-2.035 3.017v2.511h3.296C20.888 17.837 22 15.275 22 12.23z"
                                                            fill="#3E82F1"></path>
                                                        <path
                                                            d="M12.204 22c2.757 0 5.067-.893 6.752-2.421l-3.296-2.512c-.911.602-2.08.956-3.456.956-2.66 0-4.907-1.759-5.71-4.124h-3.41v2.595C4.763 19.76 8.213 22 12.203 22z"
                                                            fill="#32A753"></path>
                                                        <path
                                                            d="M6.494 13.899A5.848 5.848 0 0 1 6.174 12c0-.657.114-1.298.32-1.899v-2.59h-3.41A9.863 9.863 0 0 0 2 12c0 1.612.396 3.14 1.083 4.489l3.411-2.59z"
                                                            fill="#F9BB00"></path>
                                                        <path
                                                            d="M12.204 5.978c1.496 0 2.843.505 3.898 1.494l2.929-2.87C17.265 2.988 14.955 2 12.204 2c-3.99 0-7.44 2.242-9.12 5.511l3.41 2.59c.803-2.365 3.05-4.123 5.71-4.123z"
                                                            fill="#E74133"></path>
                                                    </svg>
                                                    Google</button>
                                            )}
                                        />
                                    </div>
                                </div>
                            </form>
                            <div className="switchToLogin flex">
                                <p>已经有帐号 ?</p>
                                <Link to='/login' className="switchLogin"><p>登录</p></Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        users: state.user,
        error: state.auth.error,
        isAuth: state.auth.token !== null,
    }
}
const mapDispatchToProps = dispatch => {
    return {
        onAuth: (email, password) => dispatch(action.auth(email, password, false)),
        socialLogin: (user, userId, token) => dispatch(action.googleAuthLogin(user, userId, token)),
        authFBSuccess: (user, email, images,  firstName, lastName, ID, accessToken) => dispatch(action.socialFBAuth(user, email, images,  firstName, lastName, ID, accessToken)),
        authGoogleSuccess: (user, email, images, firstName, lastName, ID, accessToken) => dispatch(action.socialGoogleAuth(user, email, images, firstName, lastName, ID, accessToken)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Register);
