import React from 'react';
import './Sub__Register.css';
import "../../store/reducers/auth"
import {connect} from "react-redux";
import configuration from "../../config/config";
import {registerSubUser, registerUser,searchSuburbNames,clearSuggestions,changeSuggestions} from "../../store/actions/user";
import axios from 'axios';
import SquareAnimation from "../../components/SquareAnimation/SquareAnimation";
class Sub__Register extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            firstName: "",
            lastName: "",
            suburb: "",
            checkStatusForDoneThing: false,
            CheckStatusForEarnMoney: false,
            users: {},
            isLoading:false,
        }
        this.handleClick = this.handleClick.bind(this);
    }

    changeDoneThingCheckbox = (e) => {
        this.state.checkStatusForDoneThing === true ? this.setState({ checkStatusForDoneThing: false, }) : this.setState({ checkStatusForDoneThing: true, })
    }

    changeEarnMoneyCheckbox = (e) => {
        this.state.CheckStatusForEarnMoney === true ? this.setState({ CheckStatusForEarnMoney: false, }) : this.setState({ CheckStatusForEarnMoney: true, })
    }

   async handleClick(e) {
        const {firstName, lastName, suburb} = this.state;
        const addressList = suburb.split(",");
       this.setState({ isLoading: true });
       var _this = this;
       const userId = localStorage.getItem("userId");
       const token = localStorage.getItem("token");
       const user = await axios.get(configuration.api.backend_api + `/api/v1/users/me${userId}/${token}`);
       if (user.status === 200) {
           const users = {
               ...user.data,
               address: {
                   suburb: addressList[0],
                   state: addressList[1],
                   country: "Australia",
               },
               name: {
                   firstName, lastName
               },
               token,
               _id: userId,
           }
           axios.put(configuration.api.backend_api + `/api/v1/users/updateOne`, users).then(res => {
               _this.setState({ isLoading: false });
               localStorage.setItem("userName", firstName+" "+lastName);
               _this.props.history.push("/user");
           })
       } else {
           this.props.history.push("/404");
       }
    }
    onChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value,
        })
    }
    componentDidMount(){

    }
    componentWillUpdate(nextProps, nextState){
        if (this.state.suburb !== nextState.suburb){
            var _this = this;
            _this.state = nextState
        }
    }
    render() {
        const {suggestions, onSpecialChange, onClear} = this.props;
        if (!this.state.isLoading){
            return (
                <div className="Signup__Container">
                    <SquareAnimation/>
                    <div className="Signup flex flex__column">
                        <div className="subpage-title-box">
                            <p id="title">Welcome to Shieldtasker</p>
                            <svg role="button" tabIndex="0" aria-label="close modal" className="modal_close_button__StyledCloseSimple-hpeofp-0 close-btn" width="24" height="24" viewBox="0 0 24 24" data-ui-test="modal-close-button"><path d="M13.17 12l6.41-6.42a.82.82 0 0 0-1.16-1.16L12 10.83 5.58 4.42a.82.82 0 0 0-1.16 1.16L10.83 12l-6.41 6.42a.8.8 0 0 0 0 1.16.8.8 0 0 0 1.16 0L12 13.17l6.42 6.41a.8.8 0 0 0 1.16 0 .8.8 0 0 0 0-1.16z"></path></svg>
                        </div>

                        <div className="Signup-body">
                            <form className="Signup-form flex flex__column">
                                <p id="profile--content">Tell us about yourself to set up your profile</p>
                                <div className="Signup-form-field flex flex__column">
                                    <label>First Name</label>
                                    <input name="firstName" placeholder="First Name" onChange={this.onChange} autoComplete="off"
                                            value={this.state.firstName}></input>
                                    {<div style={this.state.firstName? { display: 'none' } : { color: 'red' }}>Please enter a valid firstName</div>}
                                </div>
                                <div className="Signup-form-field flex flex__column">
                                    <label>Last Name</label>
                                    <input name="lastName" placeholder="Last Name" value={this.state.lastName} autoComplete="off"
                                    onChange={this.onChange}></input>
                                    {<div style={this.state.lastName? { display: 'none' } : { color: 'red' }}>Please enter a valid lastName</div>}
                                </div>
                                <div className="Signup-form-field flex flex__column">
                                    <label>Enter your home suburb</label>
                                    <input  placeholder="Enter a suburb" name="suburb" autoComplete="off"  value={this.state.suburb} onChange={(e) => {
                                            this.setState({[e.target.name]: e.target.value})
                                            onSpecialChange(this.state.suburb)}}/>
                                            {<div style={this.state.suburb? { display: 'none' } : { color: 'red' }}>Please enter a valid suburb</div>}
                                    <div className="suggestions">
                                        {suggestions.map((items, index) =>
                                            <div key={index} onClick = {() => {
                                                this.setState({suburb: items.suburb+","+items.state})
                                                onClear();
                                            }}>{items.suburb+","+items.state}</div>
                                        )}
                                </div>
                                </div>
                                <p>What would you like to use Shieldtasker for ?</p>
                                <div className="flex ">

                                    <div className="flex flex--start checkBox">
                                        <div type="checkbox" focusable="true" className={this.state.checkStatusForDoneThing === true ? "checkbox--active" : "checkbox--unactive"} onClick={(e) => this.changeDoneThingCheckbox(e)}><svg className="InputOption__Tick-sc-1c9czfj-4 fvQRjB" fill="white" height="8" viewBox="0 0 12 8" width="12"><path d="M9.994.304a.75.75 0 1 1 1.063 1.057L4.758 7.696a.75.75 0 0 1-1.096-.035L.91 4.513a.75.75 0 1 1 1.13-.987l2.222 2.542z"></path></svg></div>
                                        <p className="checkbox--content">I want to get things done.</p>
                                    </div>

                                    <div className="flex flex--start checkBox">
                                        <div type="checkbox" focusable="true" className={this.state.CheckStatusForEarnMoney === true ? "checkbox--active" : "checkbox--unactive"} onClick={(e) => this.changeEarnMoneyCheckbox(e)}><svg className="InputOption__Tick-sc-1c9czfj-4 fvQRjB" fill="white" height="8" viewBox="0 0 12 8" width="12"><path d="M9.994.304a.75.75 0 1 1 1.063 1.057L4.758 7.696a.75.75 0 0 1-1.096-.035L.91 4.513a.75.75 0 1 1 1.13-.987l2.222 2.542z"></path></svg></div>
                                        <p className="checkbox--content">I want to earn money.</p>
                                    </div>
                                </div>
                                <button className="submit-btn" onClick={this.handleClick}>Continue</button>

                            </form>
                        </div>

                    </div>
                </div>
            )
        } else {
                return <div></div>
        }
    }
}
const mapStateToProps = (state) => {
    return {
        users: state.user,
        userId: state.auth.userId,
        token: state.auth.token,
        email: state.user.email,
        suggestions: state.address.suggestions,
        fetching: state.address.fetching,
    }
}

const mapDispatchToProps = dispatch => ({
    onSpecialChange(value) {
        if (value){
            dispatch(
                searchSuburbNames(value)
            )
        } else {
            dispatch(
                clearSuggestions()
            )
        }
    },
    onClear(){
        dispatch(
            clearSuggestions(),
        )
    },
    registerSubUser,
     registerUser,
    searchSuburbNames,
    changeSuggestions,
    clearSuggestions,
})
export default connect( mapStateToProps, mapDispatchToProps)(Sub__Register);
