import React from 'react';
import "./User.css";
import { connect } from "react-redux";
import axios from "axios";
import configuration from "../../config/config";
import {
    registerSubUser, registerUser, addTransportation, removeTransportation,
    clearSuggestions, searchUniversities, changeUserImage
} from "../../store/actions/user";
import { authCheckState } from "../../store/actions/auth"
// import Badges from "../../components/Users/Badges"
import UserBasicInformation from "../../components/Users/UserBasicInformation/UserBasicInformation"
import About from "../../components/Users/UserDetailInformation/About/About"
// import Portfolio from "../../components/Users/UserDetailInformation/Portfolio/Portfolio"
import Skills from "../../components/Users/UserDetailInformation/Skills/Skills"
import Reviews from "../../components/Users/UserDetailInformation/Reviews/Reviews"
import SquareAnimation from "../../components/SquareAnimation/SquareAnimation";


export class User extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user: {},
            isLoading: true,
            checked: true,
            tagLine: "",
            description: "",
            checkboxList: [false, false, false, false, false, false],
            transportation: ["Bicycle", "Car", "Online", "Scooter", "Truck", "Walk"],
            specialities: "",
            languages: "",
            work: "",
            education: "",
            firstName: "",
            lastName: "",
            allTransports: [],
            fileImages: {},
            userImage: "",
            rating: 0,
            totalPeople: 0,
            otherUser: false,
        }
        this.handleFileChange = this.handleFileChange.bind(this);
    }
    handleCheckBox = (index) => {
        var { checkboxList, transportation } = this.state
        checkboxList[index] = !checkboxList[index];
        this.setState({ checkboxList });
        if (checkboxList[index]) {
            this.props.onAdd({ transport: transportation[index] });
        } else {
            this.props.onRemove({ transport: transportation[index] });
        }
    }
    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    saveNameChange = async () => {
        const newUser = {
            ...this.state.user,
            name: {
                firstName: this.state.firstName,
                lastName: this.state.lastName,
            },
            images: this.state.userImage,
            token: this.props.token,
            _id: this.props.userId,
        }
        const data = await axios.put(configuration.api.backend_api + "/api/v1/users/updateOne", newUser);
        this.setState({ user: data.data.user });
        this.props.onChangeImage(this.state.userImage);
    }

    saveSkills = async () => {
        const { transports } = this.props;
        const newUser = {
            ...this.state.user,
            skills: [{
                education: this.state.education,
                specialities: this.state.specialities,
                languages: this.state.languages,
                work: this.state.work,
                transportation: transports,
            }],
            token: this.props.token,
            _id: this.props.userId,
        }
        const data = await axios.put(configuration.api.backend_api + "/api/v1/users/updateOne", newUser);
        this.setState({ allTransports: transports });
        this.setState({ user: data.data.user });
    }

    saveDescription = async () => {
        const newUser = {
            ...this.state.user,
            about: {
                tagLine: this.state.tagLine,
                description: this.state.description
            },
            token: this.props.token,
            _id: this.props.userId,
        }
        const data = await axios.put(configuration.api.backend_api + "/api/v1/users/updateOne", newUser);
        this.setState({ user: data.data.user });
    }
    componentWillUpdate(nextProps, nextState) {
        if (nextState.description !== this.state.description || nextState.tagLine !== this.state.tagLine ||
            nextState.firstName !== this.state.firstName || nextState.lastName !== this.state.lastName ||
            nextState.education !== this.state.education || nextState.specialities !== this.state.specialities
            || nextState.user !== this.state.user) {
            var _this = this;
            _this.state = nextState;
        }
    }
    checkSkillsTransportationList(res) {
        if (this.state.user.skills.length > 0) {
            this.setState({
                education: res.data.skills[0].education,
                specialities: res.data.skills[0].specialities, languages: res.data.skills[0].languages,
                work: res.data.skills[0].work, allTransports: res.data.skills[0].transportation
            })
            var indexList = res.data.skills[0].transportation;
            var { checkboxList } = this.state;
            for (var i = 0; i < this.state.transportation.length; i++) {
                this.props.onRemove({ transport: this.state.transportation[i] });
            }
            this.addIndexTransportationList(indexList, checkboxList);
        }
    }
    addIndexTransportationList(indexList, checkboxList) {
        for (var i = 0; i < this.state.transportation.length; i++) {
            for (var item in indexList) {
                if (indexList[item].trim() === this.state.transportation[i].trim()) {
                    this.props.onAdd({ transport: this.state.transportation[i] });
                    checkboxList[i] = true;
                }
            }
        }
        this.setState({ checkboxList });
    }

    handleEducationValue = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
        this.props.onSpecialChange(this.state.education)
    }

    handleEducationhintValue = (items) => {
        this.setState({ education: items.name })
        this.props.onClear();
    }
    async componentDidMount() {
        const userId = localStorage.getItem("userId");
        const token = localStorage.getItem("token");
        this.setState({ isLoading: true });
        axios.get(configuration.api.backend_api + `/api/v1/users/me${userId}/${token}`).then(res => {
            this.setState({
                user: res.data, isLoading: false, tagLine: res.data.about.tagLine,
                description: res.data.about.description, firstName: res.data.name.firstName,
                lastName: res.data.name.lastName, userImage: res.data.images, rating: res.data.rating,
                totalPeople: res.data.totalPeople
            });
            this.checkSkillsTransportationList(res);
        }).catch(err => {
            this.props.history.push("/404");
        })
    }
    async handleFileChange(selectorFiles) {
        let data = new FormData();
        data.append("photos", selectorFiles[0]);
        let res = await axios.post(configuration.api.backend_api + '/api/v1//upload', data, {});
        this.setState({ userImage: res.data[0].location });
    }
    render() {
        if (!this.state.isLoading) {
            const { user } = this.state;
            let { onSpecialChange, onClear, universities } = this.props;
            return (
                <div className="user_page">
                    <SquareAnimation cssStyle={"absolute"} />
                    <div className="user_page__content">
                        <div className="user-profile-screen">
                            <div className="user-profile">
                                <UserBasicInformation user={user} userImage={this.state.userImage} firstName={this.state.firstName} lastName={this.state.lastName}
                                    handleChange={this.handleChange} saveNameChange={this.saveNameChange} handleFileChange={this.handleFileChange}
                                    updateTime={user.updatedAt} createTime={user.createdAt} otherUser={this.state.otherUser} rating={this.state.rating} totalPeople={this.state.totalPeople} />
                                <div className="user-part-flex">
                                    {/* <Badges /> */}
                                    <div className="personals-container">
                                        <About tagLine={this.state.tagLine} description={this.state.description} handleChange={this.handleChange} saveDescription={this.saveDescription} otherUser={this.state.otherUser} />
                                        <Skills education={this.state.education} specialities={this.state.specialities} languages={this.state.languages} work={this.state.work} allTransports={this.state.allTransports} handleChange={this.handleChange}
                                            saveSkills={this.saveSkills} onSpecialChange={onSpecialChange} onClear={onClear} universities={universities} transportation={this.state.transportation} otherUser={this.state.otherUser}
                                            handleCheckBox={this.handleCheckBox} checkboxList={this.state.checkboxList} handleEducationValue={this.handleEducationValue} handleEducationhintValue={this.handleEducationhintValue} />
                                        <Reviews currentUserId={localStorage.getItem("userId")} />
                                    </div>
                                </div>
                            </div >
                        </div >
                    </div >
                </div >
            )
        } else {
            return <div></div>
        }
    }
}
const mapStateToProps = (state) => {
    return {
        users: state.user,
        email: state.user.email,
        transports: state.transportation,
        suggestions: state.address.suggestions,
        fetching: state.address.fetching,
        universities: state.universities,
        error: state.auth.error,
        isAuth: state.auth.token !== null,
        userId: state.auth.userId,
        token: state.auth.token,
    }
}
const mapDispatchToProps = dispatch => ({
    onSpecialChange(value) {
        if (value) {
            dispatch(
                searchUniversities(value)
            )
        } else {
            dispatch(
                clearSuggestions()
            )
        }
    },
    checkAuth() {
        dispatch(
            authCheckState()
        );
    },
    onClear() {
        dispatch(
            clearSuggestions(),
        )
    },
    registerSubUser,
    registerUser,
    searchUniversities,
    clearSuggestions,
    onAdd(value) {
        dispatch(addTransportation(value));
    },
    onRemove(value) {
        dispatch(
            removeTransportation(value))
    },
    onChangeImage(image) {
        dispatch(
            changeUserImage(image)
        )
    },
})
export default connect(mapStateToProps, mapDispatchToProps)(User);
