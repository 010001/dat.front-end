import React from 'react';
import "./User.css";
import axios from "axios";
import configuration from "../../config/config";
// import Badges from "../../components/Users/Badges"
import UserBasicInformation from "../../components/Users/UserBasicInformation/UserBasicInformation"
import About from "../../components/Users/UserDetailInformation/About/About"
// import Portfolio from "../../components/Users/UserDetailInformation/Portfolio/Portfolio"
import Skills from "../../components/Users/UserDetailInformation/Skills/Skills"
import Reviews from "../../components/Users/UserDetailInformation/Reviews/Reviews"
import {connect} from "react-redux";

class UserStatic extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user: {},
            isLoading: true,
            checked: true,
            tagLine: "",
            description: "",
            checkboxList: [false, false, false, false, false, false],
            transportation: ["Bicycle", "Car", "Online", "Scooter", "Truck", "Walk"],
            specialities: "",
            languages: "",
            work: "",
            education: "",
            firstName: "",
            lastName: "",
            allTransports: [],
            fileImages: {},
            userImage: "",
            otherUser:true,
            currentUserId:null,
        }
    }
    componentWillUpdate(nextProps, nextState) {
        if (nextState.description !== this.state.description || nextState.tagLine !== this.state.tagLine ||
            nextState.firstName !== this.state.firstName || nextState.lastName !== this.state.lastName ||
            nextState.education !== this.state.education || nextState.specialities !== this.state.specialities
            || nextState.user !== this.state.user || this.state.isLoading !== nextState.isLoading || 
            this.state.currentUserId !== nextState.currentUserId) {
            var _this = this;
            _this.state = nextState;
        }
    }
    checkSkillsTransportationList(res) {
        if (res.data[0].skills.length  > 0) {
            this.setState({
                education: res.data[0].skills[0].education,
                specialities: res.data[0].skills[0].specialities, languages: res.data[0].skills[0].languages,
                work: res.data[0].skills[0].work, allTransports: res.data[0].skills[0].transportation
            })
        }
    }
    componentWillMount(){
        if (this.props.match.params.id != null){
            this.setState({currentUserId: this.props.match.params.id})
        }
    }
    async componentDidMount() {
        if (this.props.match.params.id){
             this.setState({isLoading: true});
             await axios.get(configuration.api.backend_api+ `/api/v1/users/me/?id=${this.props.match.params.id}`).then(res => {
                    this.setState({ user: res.data[0], isLoading: false});
                    this.checkSkillsTransportationList(res);
            })
        }
    }
    render() {
        if (this.props.match.params.id === this.props.userId){
                this.props.history.push("/user");
        }
        if (!this.state.isLoading) {
            const { user } = this.state;
            return (
                <div className="user_page">
                    <div className="user_page__content">
                        <div className="user-profile-screen">
                            <div className="user-profile">
                                <UserBasicInformation user={user} userImage={user.images} firstName={user.name.firstName} lastName={user.name.lastName}
                                    updateTime={user.updatedAt} createTime={user.createdAt} otherUser={this.state.otherUser} currentUserId={this.state.currentUserId}/>
                                <div className="user-part-flex">
                                    {/* <Badges /> */}
                                    <div className="personals-container">
                                        <About tagLine={user.about.tagLine} description={user.about.description} otherUser={this.state.otherUser} />
                                        <Skills education={this.state.education} specialities={this.state.specialities} languages={this.state.languages} work={this.state.work} allTransports={this.state.allTransports} 
                                           transportation={user.skills.transportation} otherUser={this.state.otherUser} checkboxList={this.state.checkboxList}/>
                                        <Reviews currentUserId={this.state.currentUserId}/>
                                    </div>
                                </div>
                            </div >
                        </div >
                    </div >
                </div >
            )
        } else {
            return <div></div>
        }
    }
}
const mapStateToProps = (state) => {
        return {
            userId: state.auth.userId,
        }
    };
export default connect(mapStateToProps, null)(UserStatic);
