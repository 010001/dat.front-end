import React from 'react'
import {User} from './User'
import Enzyme, { render, mount } from 'enzyme';
import {UserBasicInformation} from '../../components/Users/UserBasicInformation/UserBasicInformation'
import {About} from '../../components/Users/UserDetailInformation/About/About'
import {Skills} from '../../components/Users/UserDetailInformation/Skills/Skills'
import EnzymeAdapter from 'enzyme-adapter-react-16';
import toJson from "enzyme-to-json";

Enzyme.configure({ adapter: new EnzymeAdapter() });



describe('User', () => {

    beforeEach(()=>{
        jest.clearAllMocks();
      });

    const User__render = render(<User/>)
    expect(toJson(User__render)).toMatchSnapshot();

    const setup = (props = {}, state = null) => {
        const wrapper = mount(<User {...props} />);
        if (state) wrapper.setState(state);
        return wrapper;
    };
    
    const findByTestAttr = (wrapper, val) => {
        return wrapper.find(`[data-test="${val}"]`);
    };

    const wrapper = setup();
    let user={
        address: {
            country:"Australia",
            state:"",
            suburb:"Kensford"
        }
    };

    test('renders without error', () => {
        expect(wrapper.find('UserBasicInformation').exists());
        expect(wrapper.find('About').exists());
        expect(wrapper.find('Skills').exists());
        expect(wrapper.find('Reviews').exists());
    });

    test('edit user name information', () => {
        const wrapper = mount(<UserBasicInformation user={user}/>);
        let container = findByTestAttr(wrapper, 'nametoggle');
        container.simulate('click');
        expect(wrapper.state().nameTagFlag).toEqual(false);
        let container__cancel = findByTestAttr(wrapper, 'nametoggle__cancel');
        container__cancel.simulate('click');
        expect(wrapper.state().nameTagFlag).toEqual(true);
    });

    test('tab can be change between tasker and poster', () => {
        const wrapper = mount(<UserBasicInformation user={user}/>);       
        let container = findByTestAttr(wrapper, 'user__tasktab');
        expect(container.hasClass("tab half worker small active")).toEqual(true);
        container.simulate('click');
        expect(container.hasClass("tab half worker small")).toEqual(true);
    });

    test('jump link is correct', () => {
        const wrapper = mount(<UserBasicInformation user={user}/>);
        let viewTasksHref = findByTestAttr(wrapper, 'user__linkToViewTasks');
        expect(viewTasksHref.html()).toEqual('<Link to="/view-tasks" data-test="user__linkToViewTasks">Let’s browse available tasks.</Link>');
        let PostTasksHref = findByTestAttr(wrapper, 'user__linkToPostTask');
        expect(PostTasksHref.html()).toEqual('<Link to="/" data-test="user__linkToPostTask">Let’s go post a task.</Link>');
    });

    test('edit user about information', () => {
        const wrapper = mount(<About/>);
        let container = findByTestAttr(wrapper, 'abouttoggle');
        container.simulate('click');
        expect(wrapper.state().descriptionFlag).toEqual(false);
        let container__cancel = findByTestAttr(wrapper, 'abouttoggle__cancel');
        container__cancel.simulate('click');
        expect(wrapper.state().descriptionFlag).toEqual(true);
    });

    test('edit user skill information', () => {
        const wrapper = mount(<Skills universities={[
            {
                "alpha_two_code": "US",
                "country": "United States",
                "domain": "acu.edu",
                "name": "Abilene Christian University",
                "web_page": "http://www.acu.edu/"
            },
            {
                "alpha_two_code": "US",
                "country": "United States",
                "domain": "adelphi.edu",
                "name": "Adelphi University",
                "web_page": "http://www.adelphi.edu/"
            },
            {
                "alpha_two_code": "US",
                "country": "United States",
                "domain": "agnesscott.edu",
                "name": "Agnes Scott College",
                "web_page": "http://www.agnesscott.edu/"
            }]} 
            transportation={["Bicycle", "Car", "Online", "Scooter", "Truck", "Walk"]}
            checkboxList={[false, false, false, false, false, false]}/>);
        let container = findByTestAttr(wrapper, 'skilltoggle');
        container.simulate('click');
        expect(wrapper.state().skillsFlag).toEqual(false);
        let container__cancel = findByTestAttr(wrapper, 'skilltoggle__cancel');
        container__cancel.simulate('click');
        expect(wrapper.state().skillsFlag).toEqual(true);
    });

})

