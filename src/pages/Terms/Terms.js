import React from 'react';
import "./Terms.css";
import Footer from "../../components/Footer/Footer";

export default class Terms extends React.Component {


    render() {
        return (
            <main>
                <div className="terms">
                    <div className="terms__screen">
                        <div className="terms__content">
                            <div>
                                <div>
                                    <h1>Terms and Conditions</h1>
                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor . Aenean massa.&nbsp;
                                        <a href="/contact/" data-internal-link="/contact/">here</a>&nbsp;Aenean commodo ligula eget dolor. Aenean massa. .</p>
                                    <p>The Shieldtasker terms &amp; Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient , Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient .</p>
                                    <h2>dolor sit amet: www.shieldtasker.com</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. commodo adipiscing ligula.</p>
                                    <p>Please read these terms and all Policies including the&nbsp;<a href="/community-guidelines/" data-internal-link="/community-guidelines/">Community Guidelines</a>&nbsp;Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient .</p>
                                    <p>All defined terms in this Agreement have the meaning given to them in the Shieldtasker Glossary.</p>
                                    <h3>1.  Lorem ipsum dolor sit amet</h3>
                                    <ul>
                                        <li>
                                            <p>1.1  Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes,.</p>
                                        </li>
                                        <li>
                                            <p>1.2 Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes,</p>
                                        </li>
                                        <li>
                                            <p>1.3 Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes,.</p>
                                        </li>
                                        <li>
                                            <p>1.4 Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes,.</p>
                                        </li>
                                        <li>
                                            <p>1.5 Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes,.</p>
                                        </li>
                                        <li>
                                            <p>1.6 Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes,.</p>
                                        </li>
                                        <li>
                                            <p>1.7 Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes,.</p>
                                        </li>
                                        <li>
                                            <p>1.8 Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes,.</p>
                                        </li>
                                        <li>
                                            <p>1.9 Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes,.</p>
                                        </li>
                                        <li>
                                            <p>1.10 Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes,.</p>
                                        </li>
                                        <li>
                                            <p>1.11 Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes,.</p>
                                        </li>
                                        <li>
                                            <p>1.12 Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes,.</p>
                                        </li>
                                        <li>
                                            <p>1.13 Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes,.</p>
                                        </li>
                                        <li>
                                            <p>1.14 Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes,.</p>
                                        </li>
                                        <li>
                                            <p>1.15 Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes,.</p>
                                        </li>
                                        <li>
                                            <p>1.16 Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes,.</p>
                                        </li>
                                        <li>
                                            <p>1.17 Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes,.</p>
                                        </li>
                                    </ul>
                                    <h3>2.  Lorem ipsum dolor sit amet</h3>
                                    <ul>
                                        <li>
                                            <p>2.1 Shieldtasker provides the Shieldtasker Platform only, enabling Users to publish Posted Tasks and make Offers on Posted Tasks.</p>
                                        </li>
                                        <li>
                                            <p>2.2 Shieldtasker only permits individuals over 18 years of age to become Users.</p>
                                        </li>
                                        <li>
                                            <p>2.3 Users must be natural persons, but can specify within their account description that they represent a business entity.</p>
                                        </li>
                                        <li>
                                            <p>2.4 At its absolute discretion, Shieldtasker may refuse to allow any person to register or create an account with Shieldtasker or cancel or suspend or modify any existing account including if Shieldtasker reasonably forms the view that a User's conduct (including a breach of this Agreement) is detrimental to the operation of the Shieldtasker Platform.</p>
                                        </li>
                                        <li>
                                            <p>2.5 Registering and creating an account with Shieldtasker is free. There is no charge for a Poster to post tasks, or for other Shieldtasker Users to review content on the Shieldtasker Platform, including Posted Tasks.</p>
                                        </li>
                                        <li>
                                            <p>2.6 Shieldtasker accepts no liability for any aspect of the Poster and Tasker interaction, including but not limited to the description, performance or delivery of Services.</p>
                                        </li>
                                        <li>
                                            <p>2.7 Shieldtasker has no responsibility and makes no warranty as to the truth or accuracy of any aspect of any information provided by Users, including, but not limited to, the ability of Taskers to perform tasks or supply items, or the honesty or accuracy of any information provided by Posters or the Posters' ability to pay for the Services requested.</p>
                                        </li>
                                        <li>
                                            <p>2.8 Except for liability in relation to any Non-excludable Condition, the Shieldtasker Service is provided on an "as is" basis, and without any warranty or condition, express or implied. To the extent permitted by law, we and our suppliers specifically disclaim any implied warranties of title, merchantability, fitness for a particular purpose and non-infringement.</p>
                                        </li>
                                        <li>
                                            <p>2.9 Shieldtasker has no obligation to any User to assist or involve itself in any dispute between Users, although may do so to improve User experience.</p>
                                        </li>
                                    </ul>
                                    <h3>3.  Lorem ipsum </h3>
                                    <ul>
                                        <li>
                                            <p>3.1 You will at all times:</p>
                                            <ul>
                                                <li>
                                                    <p>(a) comply with this Agreement (including all Policies) and all applicable laws and regulations;</p>
                                                </li>
                                                <li>
                                                    <p>(b) only post accurate information on the Shieldtasker Platform;</p>
                                                </li>
                                                <li>
                                                    <p>(c) ensure that You are aware of any laws that apply to You as a Poster or Tasker, or in relation to using the Shieldtasker Platform.</p>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <p>3.2 You agree that any content (whether provided by Shieldtasker, a User or a third party) on the Shieldtasker Platform may not be used on third party sites or for other business purposes without Shieldtasker's prior permission.</p>
                                        </li>
                                        <li>
                                            <p>3.3 You must not use the Shieldtasker Platform for any illegal or immoral purpose.</p>
                                        </li>
                                        <li>
                                            <p>3.4 You must maintain control of Your Shieldtasker account at all times. This includes not allowing others to use Your account, or by transferring or selling Your account or any of its content to another person.</p>
                                        </li>
                                        <li>
                                            <p>3.5 You grant Shieldtasker an unrestricted, worldwide, royalty-free licence to use, reproduce, modify and adapt any content and information posted on the Shieldtasker Platform for the purpose of publishing material on the Shieldtasker Platform and as otherwise may be required to provide the Shieldtasker Service, for the general promotion of the Shieldtasker Service, and as permitted by this Agreement.</p>
                                        </li>
                                        <li>
                                            <p>3.6 You agree that any information posted on the Shieldtasker Platform must not, in any way whatsoever, be potentially or actually harmful to Shieldtasker or any other person. Harm includes, but is not limited to, economic loss that will or may be suffered by Shieldtasker.</p>
                                        </li>
                                        <li>
                                            <p>3.7 Without limiting any provision of this Agreement, any information You supply to Shieldtasker or publish in a Posted Task (including as part of an Offer) must be up to date and kept up to date and must not:</p>
                                            <p>*(a) be false, inaccurate or misleading or deceptive;</p>
                                            <p>*(b) be fraudulent or involve the sale of counterfeit or stolen items;</p>
                                            <p>*(c) infringe any third party's copyright, patent, trademark, trade secret or other proprietary rights or intellectual property rights, rights of publicity, confidentiality or privacy;</p>
                                            <p>*(d) violate any applicable law, statute, ordinance or regulation (including, but not limited to, those governing export and import control, consumer protection, unfair competition, criminal law, antidiscrimination and trade practices/fair trading laws);</p>
                                            <p>*(e) be defamatory, libellous, threatening or harassing;</p>
                                            <p>*(f) be obscene or contain any material that, in Shieldtasker's sole and absolute discretion, is in any way inappropriate or unlawful, including, but not limited to obscene, inappropriate or unlawful images; or</p>
                                            <p>*(g) contain any malicious code, data or set of instructions that intentionally or unintentionally causes harm or subverts the intended function of any Shieldtasker Platform, including, but not limited to viruses, trojan horses, worms, time bombs, cancelbots, easter eggs or other computer programming routines that may damage, modify, delete, detrimentally interfere with, surreptitiously intercept, access without authority or expropriate any system, data or Personal Information.</p>
                                        </li>
                                        <li>
                                            <p>3.8 Shieldtasker Platform may from time to time engage location-based or map-based functionality. The Shieldtasker Platform may display the location of Posters and Taskers to persons browsing the Shieldtasker Platform. A Poster should never disclose personal details such as the Poster's full name, street number, phone number or email address in a Posted Task or in any other public communication on the Shieldtasker Platform.</p>
                                        </li>
                                        <li>
                                            <p>3.9 If You are a Tasker, You must have the right to provide Services under a Task Contract and to work in the jurisdiction where the Services are performed. You must comply with tax and regulatory obligations in relation to any payment (including Tasker Funds) received under a Task Contract.</p>
                                        </li>
                                        <li>
                                            <p>3.10 You must not, when supplying Services, charge a Poster any fees on top of the Tasker Funds. However, the parties to a Task Contract may agree to amend the Agreed Price through the Shieldtasker Platform.</p>
                                        </li>
                                        <li>
                                            <p>3.11 You must not request payments outside of the Shieldtasker Platform from the Poster for the Services except to the extent permitted by clause 3.12 and only if the Shieldtasker Platform does not facilitate the reimbursement via the Payment Account of costs considered in clause 3.12.</p>
                                        </li>
                                        <li>
                                            <p>3.12 If a Tasker agrees to pay some costs of completing the Services (such as equipment to complete the Services), the Tasker is solely responsible for obtaining any reimbursement from the Poster. Shieldtasker advises Taskers not to agree to incur costs in advance of receiving the payment for these costs, unless the Tasker is confident the Poster will reimburse the costs promptly.</p>
                                        </li>
                                        <li>
                                            <p>3.13 For the proper operation of the Shieldtasker Platform (including insurance, proper pricing and compliance with Policies), the Tasker must ensure that, if it subcontracts any part of the performance of the Services to a third party in accordance with a Task Contract, then that third party must also be a registered User of the Shieldtasker Platform.</p>
                                        </li>
                                        <li>
                                            <p>3.14 If Shieldtasker determines at its sole discretion that You have breached any obligation under this clause 3 or that You have breached one or more Task Contracts, it reserves the rights to remove any content, Posted Task or Offer You have submitted to the Shieldtasker Service or cancel or suspend Your account and/or any Task Contracts.</p>
                                        </li>
                                    </ul>
                                    <h3>4. Lorem</h3>
                                    <ul>
                                        <li>
                                            <p>4.1 Upon the creation of a Task Contract, the Tasker and Poster each owes Shieldtasker the respective portion of the Service Fee. The Service Fee will automatically be deducted from the Agreed Price held in the Payment Account.</p>
                                        </li>
                                        <li>
                                            <p>4.2 If the Posted Task requires a Tasker to incur costs in completing the Services, the cost incurred will not be included in any calculation of Fees.</p>
                                        </li>
                                        <li>
                                            <p>4.3 Fees do not include any fees that may be due to Third Party Service providers. All Third Party Service providers are paid pursuant to a User's separate agreement with that Third Party Service provider.</p>
                                        </li>
                                        <li>
                                            <p>4.4 All Fees and charges payable to Shieldtasker are non-cancellable and non-refundable, save for Your rights under any Non-Excludable Conditions.</p>
                                        </li>
                                        <li>
                                            <p>4.5 If Shieldtasker introduces a new service on the Shieldtasker Platform, the Fees applying to that service will be payable as from the launch of the service.</p>
                                        </li>
                                        <li>
                                            <p>4.6 Shieldtasker may set-off any Fees against any Tasker Funds or other amounts held by Shieldtasker on behalf of a User.</p>
                                        </li>
                                        <li>
                                            <p>4.7 Shieldtasker may restrict a User's account until all Fees have been paid.</p>
                                        </li>
                                    </ul>
                                    <h3>5.   Lorem ipsum ipsum</h3>
                                    <ul>
                                        <li>
                                            <p>5.1 If the Task Contract is cancelled for any reason (by a Poster, a Tasker or under this Agreement) prior to the commencement of the Task Contract, then if Shieldtasker is reasonably satisfied that the Agreed Price should be returned to the Poster then the Agreed Price will be refunded to the Poster as Stored Value and a Cancellation Admin Fee will be due to Shieldtasker by the User who the cancellation of the Task Contract is attributable to under clause 5.5 or 5.6.</p>
                                        </li>
                                        <li>
                                            <p>5.2 Shieldtasker may decide in its absolute discretion to refund the Agreed Price back onto the Poster's credit card instead of Stored Value or waive the Cancellation Admin Fee.</p>
                                        </li>
                                        <li>
                                            <p>5.3 Any amount returned by Shieldtasker to a Poster on behalf of a Tasker under clause 5.1 will be a debt owed by the Tasker to Shieldtasker and may be offset by Shieldtasker against any other payments owed at any time to the Tasker.</p>
                                        </li>
                                        <li>
                                            <p>5.4 Any outstanding Cancellation Admin Fee owed by a User under clause 5.1 will be a debt owed by that User to Shieldtasker and may also be offset by Shieldtasker against any other payments owed at any time to the User.</p>
                                        </li>
                                        <li>
                                            <p>5.5 Cancellation of a Task Contract will be attributable to the Tasker where:</p>
                                            <ul>
                                                <li>
                                                    <p>(a) the Poster and the Tasker mutually agree to cancel the Task Contract; or</p>
                                                </li>
                                                <li>
                                                    <p>(b) following reasonable but unsuccessful attempts by a Poster to contact a Tasker to perform the Task Contract, the Task Contract is cancelled by the Poster; or</p>
                                                </li>
                                                <li>
                                                    <p>(c) the Tasker cancels the Task Contract; or</p>
                                                </li>
                                                <li>
                                                    <p>(d) a Task Contract is cancelled in accordance with clause 3.14 as a result of the Tasker’s actions or breach.</p>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <p>5.6 A Cancellation of a Task Contract will be attributable to a Poster where:</p>
                                            <ul>
                                                <li>
                                                    <p>(a) the Poster cancels the Task Contract (other than in accordance with clause 5.5(b); or</p>
                                                </li>
                                                <li>
                                                    <p>(b) a Task Contract is cancelled in accordance with clause 3.14 as a result of the Poster’s actions or breach..</p>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <p>5.7 If the parties agree to any additional cancellation fee payable under the Task Contract, it is the responsibility of the party aggrieved to claim any amount owed directly from the other.</p>
                                        </li>
                                        <li>
                                            <p>5.8 Shieldtasker may take up to 14 days to return the Agreed Price (less the Cancellation Admin Fee, if applicable) to the Poster.</p>
                                        </li>
                                        <li>
                                            <p>5.9 If, for any reason, the Tasker Funds cannot be transferred or otherwise made to the Tasker or returned to the Poster (as the case may be) or no claim is otherwise made for the Tasker Funds, the Tasker Funds will remain in the Payment Account until paid or otherwise for up to three months from the date the Poster initially paid the Agreed Price into the Payment Account.</p>
                                        </li>
                                        <li>
                                            <p>5.10 Following the 3 months referred to in clause 5.6, and provided there is still no dispute in respect of the Tasker Funds, the Tasker Funds will be credited to the Poster as Stored Value.</p>
                                        </li>
                                        <li>
                                            <p>5.11 If the Task Contract is cancelled and a User who is party to the Task Contract can show that work under a Task Contract was commenced, then the amount of the Agreed Price to be returned to the Poster will be conditional upon the mediation and dispute process in clause 18. However, the Cancellation Admin Fee will always be due in accordance with clause 5.1.</p>
                                        </li>
                                    </ul>
                                    <h3>6. STORED VALUE</h3>
                                    <ul>
                                        <li>
                                            <p>6.1 Stored Value :</p>
                                            <ul>
                                                <li>
                                                    <p>(a) can be used by the credited User to pay for any new Services via the Shieldtasker Platform;</p>
                                                </li>
                                                <li>
                                                    <p>(b) are not refundable or redeemable for cash;</p>
                                                </li>
                                                <li>
                                                    <p>(c) cannot be replaced, exchanged or reloaded or transferred to another card or account;</p>
                                                </li>
                                                <li>
                                                    <p>(d) are valid for 12 months from the date on which that particular Stored Value is applied to a User's account, the date of issue or purchase or any expiry date applied by Shieldtasker (conditional upon any contrary specific jurisdictional legislative requirements);</p>
                                                </li>
                                                <li>
                                                    <p>(e) if the Stored Value is acquired other than under this Agreement, it may also be conditional on compliance with additional, or different, terms and conditions, as specified in relation to Stored Value, such as a restriction on when the Stored Value is redeemable (for example only for a User's first Task Contract), specify a minimum Services value, or specify a maximum credit or discount value; and</p>
                                                </li>
                                                <li>
                                                    <p>(f) must not be reproduced, copied, distributed, or published directly or indirectly in any form or by any means for use by an entity other than the credited User, or stored in a data retrieval system, without Shieldtasker's prior written permission.</p>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <p>6.2 The User credited with a Stored Value is solely responsible for the security of any Stored Value. Save for the Non-Excludable Conditions, Shieldtasker will have no liability for any loss or damage to the Stored Value and does not have any obligation to replace Stored Value.</p>
                                        </li>
                                        <li>
                                            <p>6.3 Shieldtasker will not accept, and may refuse or cancel, any Stored Value, which it reasonably determines in its discretion, have been used in breach of this Agreement or have been forged, tampered with, or are otherwise fraudulent and Shieldtasker reserves the right to refer any suspected fraudulent activity to relevant law enforcement authorities. In particular, Stored Value, such as promotional coupons, vouchers or codes distributed or circulated without our approval, for example on an internet message board or on a "bargains" website, are not valid for use and may be refused or cancelled.</p>
                                        </li>
                                        <li>
                                            <p>6.4 Shieldtasker is entitled to any value on Stored Value which is not redeemed before the Stored Value expires or is cancelled by Shieldtasker.</p>
                                        </li>
                                    </ul>
                                    <h3>7. BUSINESS PARTNERS</h3>
                                    <ul>
                                        <li>
                                            <p>7.1 Shieldtasker may enter into agreements with Business Partners and may seek to engage Taskers in the provision of Business Services. Taskers who agree to perform Business Services for Business Partners acknowledge and agree that Shieldtasker and the Business Partner may on-sell Services supplied to third parties for an increased fee.</p>
                                        </li>
                                        <li>
                                            <p>7.2 Business Partners may require Taskers providing Business Services to be approved or hold particular qualifications. Shieldtasker may assist Business Partners to locate suitably qualified Taskers. Shieldtasker makes no warranty that it will promote any or all suitably qualified Taskers to Business Partners.</p>
                                        </li>
                                        <li>
                                            <p>7.3 Business Partners may require Taskers to enter into a Business Partner Contract before providing Business Services.</p>
                                        </li>
                                        <li>
                                            <p>7.4 Where a Tasker accepts a Posted Task with a Business Partner:</p>
                                            <ul>
                                                <li>
                                                    <p>(a) the Tasker must provide Business Services to the Business Partner in accordance with the Task Contract and any applicable Business Partner Contract; and</p>
                                                </li>
                                                <li>
                                                    <p>(b) the terms of the Business Partner Contract will prevail to the extent of any inconsistency.</p>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                    <h3>8. PAYMENT FACILITY</h3>
                                    <ul>
                                        <li>
                                            <p>8.1 Shieldtasker uses a Payment Provider to operate the Payment Account.</p>
                                        </li>
                                        <li>
                                            <p>8.2 In so far as it is relevant to the provision of the Payment Account, the terms at&nbsp;https://stripe.com/au/ssa&nbsp;are incorporated into this Agreement and will prevail over this Agreement to the extent of any inconsistency in relation to the provision of the Payment Account.</p>
                                        </li>
                                        <li>
                                            <p>8.3 If Shieldtasker changes its Payment Provider You may be asked to agree to any further additional terms with those providers. If you do not agree to them, you will be given alternative means of payment.</p>
                                        </li>
                                    </ul>
                                    <h3>9. THIRD PARTY SERVICES</h3>
                                    <ul>
                                        <li>
                                            <p>9.1 Shieldtasker may from time to time include Third Party Services on the Shieldtasker Platform. These Third Party Services are not provided by Shieldtasker.</p>
                                        </li>
                                        <li>
                                            <p>9.2 Third Party Services are offered to Users pursuant to the third party's terms and conditions. Third Party Services may be promoted on the Shieldtasker Platform as a convenience to our Users who may find the Third Party Services of interest or of use.</p>
                                        </li>
                                        <li>
                                            <p>9.3 If a User engages with any Third Party Service provider, the agreement will be directly between the User and that Third Party Service provider.</p>
                                        </li>
                                        <li>
                                            <p>9.4 Shieldtasker makes no representation or warranty as to the Third Party Services. However, to help us continue to improve our Shieldtasker Platform, Users may inform Shieldtasker of their Third Party Service experience&nbsp;<a href="https://www.shieldtasker.com/contact/">here</a>.</p>
                                        </li>
                                    </ul>
                                    <h3>10. VERIFICATION &amp; BADGES</h3>
                                    <ul>
                                        <li>
                                            <p>10.1 Shieldtasker may use Identity Verification Services.</p>
                                        </li>
                                        <li>
                                            <p>10.2 You agree that Shieldtasker Identity Verification Services may not be fully accurate as all Shieldtasker Services are dependent on User-supplied information and/or information or Verification Services provided by third parties.</p>
                                        </li>
                                        <li>
                                            <p>10.3 You are solely responsible for identity verification and Shieldtasker accepts no responsibility for any use that is made of an Shieldtasker Identity Verification Service.</p>
                                        </li>
                                        <li>
                                            <p>10.4 Shieldtasker Identity Verification Services may be modified at any time.</p>
                                        </li>
                                        <li>
                                            <p>10.5 The Shieldtasker Platform may also include a User-initiated feedback system to help evaluate Users.</p>
                                        </li>
                                        <li>
                                            <p>10.6 Shieldtasker may make Badges available to Taskers. The available Badge can be requested by the Tasker via the Shieldtasker Platform, and arranged on behalf of the Tasker and issued by Shieldtasker, for a fee. Obtaining Badges may be conditional upon the provision of certain information or documentation by the Tasker and determined by Shieldtasker or a third party verifier which shall be governed by its terms.</p>
                                        </li>
                                        <li>
                                            <p>10.7 You acknowledge that Badges are point in time checks and may not be accurate at the time it is displayed. You acknowledge that to the extent You relied on a Badge in entering into a Task Contract, you do so aware of this limitation. You should seek to verify any Badge with the Tasker prior to commencing the task.</p>
                                        </li>
                                        <li>
                                            <p>10.8 It remains the Tasker's responsibility to ensure that information or documentation it provides in obtaining a Badge is true and accurate and must inform Shieldtasker immediately if a Badge is no longer valid.</p>
                                        </li>
                                        <li>
                                            <p>10.9 Shieldtasker may, at its discretion, issue Badges to Taskers for a fee.</p>
                                        </li>
                                        <li>
                                            <p>10.10 The issue of a Badge to a Tasker remains in the control of Shieldtasker and the display and use of a Badge is licensed to the Tasker for use on the Shieldtasker Platform only. Any verification obtained as a result of the issue of a Badge may not be used for any other purpose outside of the Shieldtasker Platform.</p>
                                        </li>
                                        <li>
                                            <p>10.11 Shieldtasker retains the discretion and right to not issue, or remove without notice, a Badge if You are in breach of any of the terms of this Agreement, the Badge has been issued incorrectly, obtained falsely, has expired, is no longer valid or for any other reason requiring its removal by Shieldtasker.</p>
                                        </li>
                                    </ul>
                                    <h3>11. INSURANCE</h3>
                                    <ul>
                                        <li>
                                            <p>11.1 Shieldtasker may offer its Users an opportunity to obtain insurance for certain Task Contracts. All such insurance will be offered by a third party. Any application and terms and conditions for such third party insurance will be displayed on the Shieldtasker website when they are available. Shieldtasker confirms that all insurance policies are Third Party Services and governed by further terms set out for Third Party Services.</p>
                                        </li>
                                        <li>
                                            <p>11.2 Shieldtasker does not represent that any insurance it acquires or which is offered via the Shieldtasker Platform is adequate or appropriate for any particular User.</p>
                                        </li>
                                        <li>
                                            <p>11.3 Each User must make its own enquiries about whether any further insurance is required and Taskers remain responsible for ensuring that they have, and maintain, sufficient insurance to cover the Services provided to other Users of the Shieldtasker Platform.</p>
                                        </li>
                                        <li>
                                            <p>11.4 Shieldtasker may also take out other insurance itself and that insurance may at Shieldtasker's option extend some types of cover to Users. Shieldtasker reserves the right to change the terms of its insurance policies with the third party insurance providers at any time. A summary of the policies are available on the Shieldtasker website and the policy details can be requested via Shieldtasker. Users are responsible for familiarising themselves with these details.</p>
                                        </li>
                                        <li>
                                            <p>11.5 You acknowledge and agree that in the event that a claim is made relating to any services performed and/or goods provided by a Tasker, and the insurance taken out by Shieldtasker (if any) responds to that claim then this clause applies. If a claim is made against a Tasker, Shieldtasker may (provided that the Tasker consents) elect to make a claim under any applicable policy and if the claim is successful, Shieldtasker reserves its right to recover any excess or deductible payable in respect of the claim from the Tasker. Where Shieldtasker makes a claim and the insurer assesses that the Tasker is responsible, Shieldtasker is entitled to rely on that assessment. If You do not pay any excess due under this clause, Shieldtasker may also elect to set this amount off some or all of the excess paid by it against future moneys it may owe to You.</p>
                                        </li>
                                        <li>
                                            <p>11.6 You acknowledge and agree that in the event that a claim is made relating to any services performed and/or goods provided by a Tasker, and the insurance taken out by Shieldtasker (if any) does not respond to the claim or the claim is below the excess payable to the insurer, then this clause applies. Shieldtasker may elect to reject or pay an amount to settle a claim not covered by Shieldtasker's own insurance policies. To the extent that the Tasker was or would be liable for the amount of the claim, if Shieldtasker elects to pay an amount to settle the claim the amount paid by Shieldtasker may be recovered by Shieldtasker from the Tasker. Shieldtasker may also elect to set this amount off against future moneys it may owe to the Tasker.</p>
                                        </li>
                                    </ul>
                                    <h3>12. FEEDBACK</h3>
                                    <ul>
                                        <li>
                                            <p>12.1 You can complain about any comment made on the Shieldtasker Platform using the 'Report' function of the Shieldtasker Platform or contact Shieldtasker via the Shieldtasker Platform.</p>
                                        </li>
                                        <li>
                                            <p>12.2 Shieldtasker is entitled to suspend or terminate Your account at any time if Shieldtasker, in its sole and absolute discretion, is concerned by any feedback about You, or considers Your feedback rating to be problematic for other Shieldtasker Users.</p>
                                        </li>
                                    </ul>
                                    <h3>13. LIMITATION OF LIABILITY</h3>
                                    <ul>
                                        <li>Please see Your Country Specific Terms for the applicable exclusions and limitations of liability.</li>
                                    </ul>
                                    <h3>14. PRIVACY</h3>
                                    <ul>
                                        <li>
                                            <p>14.1 Third Party Service providers will provide their service pursuant to their own Privacy Policy. Prior to acceptance of any service from a third party, You must review and agree to their terms of service including their privacy policy.</p>
                                        </li>
                                        <li>
                                            <p>14.2 Shieldtasker will endeavour to permit you to transact anonymously on the Shieldtasker Platform. However in order to ensure Shieldtasker can reduce the incidence of fraud and other behaviour in breach of the Community Guidelines, Shieldtasker reserves the right to ask Users to verify themselves in order to remain a User.</p>
                                        </li>
                                    </ul>
                                    <h3>15. MODIFICATIONS TO THE AGREEMENT</h3>
                                    <ul>
                                        <li>
                                            <p>15.1 Shieldtasker may modify this Agreement or the Policies (and update the Shieldtasker pages on which they are displayed) from time to time. Shieldtasker will send notification of such modifications to Your Shieldtasker account or advise You the next time You login.</p>
                                        </li>
                                        <li>
                                            <p>15.2 When You actively agree to amended terms (for example, by clicking a button saying "I accept") or use the Shieldtasker Platform in any manner, including engaging in any acts in connection with a Task Contract, the amended terms will be effective immediately. In all other cases, the amended terms will automatically be effective 30 days after they are initially notified to You.</p>
                                        </li>
                                        <li>
                                            <p>15.3 If You do not agree with any changes to this Agreement (or any of our Policies), You must either terminate your account or You must notify Shieldtasker who will terminate Your Shieldtasker account, and stop using the Shieldtasker Service.</p>
                                        </li>
                                    </ul>
                                    <h3>16. NO AGENCY</h3>
                                    <ul>
                                        <li>16.1 No agency, partnership, joint venture, employee-employer or other similar relationship is created by this Agreement. In particular You have no authority to bind Shieldtasker, its related entities or affiliates in any way whatsoever. Shieldtasker confirms that all Third Party Services that may be promoted on the Shieldtasker Platform are provided solely by such Third Party Service providers. To the extent permitted by law, Shieldtasker specifically disclaims all liability for any loss or damage incurred by You in any manner due to the performance or non-performance of such Third Party Service.</li>
                                    </ul>
                                    <h3>17. NOTICES</h3>
                                    <ul>
                                        <li>
                                            <p>17.1 Except as stated otherwise, any notices must be given by registered ordinary post or by email, either to Shieldtasker's contact address as displayed on the Shieldtasker Platform, or to Shieldtasker Users' contact address as provided at registration. Any notice shall be deemed given:</p>
                                            <ul>
                                                <li>
                                                    <p>(a) if sent by email, 24 hours after email is sent, unless the User is notified that the email address is invalid or the email is undeliverable, and</p>
                                                </li>
                                                <li>
                                                    <p>(b) if sent by pre-paid post, three Business Days after the date of posting, or on the seventh Business Day after the date of posting if sent to or posted from outside the jurisdiction in which You have Your Shieldtasker Platform account.</p>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <p>17.2 Notices related to performance of any Third Party Service must be delivered to such third party as set out in the Third Party Service provider's terms and conditions.</p>
                                        </li>
                                    </ul>
                                    <h3>18. MEDIATION AND DISPUTE RESOLUTION</h3>
                                    <ul>
                                        <li>
                                            <p>18.1 Shieldtasker encourages You to try and resolve disputes (including claims for returns or refunds) with other Users directly. Accordingly, You acknowledge and agree that Shieldtasker may, in its absolute discretion, provide Your information as it decides is suitable to other parties involved in the dispute.</p>
                                        </li>
                                        <li>
                                            <p>18.2 If a dispute arises with another User, You must co-operate with the other User and make a genuine attempt to resolve the dispute.</p>
                                        </li>
                                        <li>
                                            <p>18.3 Shieldtasker may elect to assist Users resolve disputes. Any User may refer a dispute to Shieldtasker. You must co-operate with any investigation undertaken by Shieldtasker. Shieldtasker reserves the right to make a final determination (acting reasonably) based on the information supplied by the Users and direct the Payment Provider to make payment accordingly. You may raise your dispute with the other User or Shieldtasker's determination in an applicable court or tribunal.</p>
                                        </li>
                                        <li>
                                            <p>18.4 Shieldtasker has the right to hold any Agreed Price that is the subject of a dispute in the Payment Account, until the dispute has been resolved.</p>
                                        </li>
                                        <li>
                                            <p>18.5 Shieldtasker may provide access to a Third Party Dispute Service. If such a service is provided, either party may request the other party to submit to the Third Party Dispute Service if the parties have failed to resolve the dispute directly. Terms and conditions for the Third Party Dispute Service will be available on request. The Third Party Dispute Service is a Third Party Service and Users are responsible for paying any costs associated with the Third Party Dispute Service in accordance with the Third Party Dispute Service terms and conditions.</p>
                                        </li>
                                        <li>
                                            <p>18.6 Disputes with any Third Party Service provider must proceed pursuant to any dispute resolution process set out in the terms of service of the Third Party Service provider.</p>
                                        </li>
                                        <li>
                                            <p>18.7 If You have a complaint about the Shieldtasker Service please contact us&nbsp;<a href="/contact/" data-internal-link="/contact/">here</a>.</p>
                                        </li>
                                        <li>
                                            <p>18.8 If Shieldtasker provides information about other Users to You for the purposes of resolving disputes under this clause, You acknowledge and agree that such information will be used only for the purpose of resolving the dispute (and no other purpose) and that you will be responsible and liable to Shieldtasker for any costs, losses or liabilities incurred by Shieldtasker in relation to any claims relating to any other use of information not permitted by this Agreement.</p>
                                        </li>
                                    </ul>
                                    <h3>19. TERMINATION</h3>
                                    <ul>
                                        <li>
                                            <p>19.1 Either You or Shieldtasker may terminate your account and this Agreement at any time for any reason.</p>
                                        </li>
                                        <li>
                                            <p>19.2 Termination of this Agreement does not affect any Task Contract that has been formed between Shieldtasker Users.</p>
                                        </li>
                                        <li>
                                            <p>19.3 Third Party Services are conditional upon, and governed by, Third Party Service provider terms and conditions.</p>
                                        </li>
                                        <li>
                                            <p>19.4 Sections 4 (Fees), 13 (Limitation of Liability) and 18 (Mediation and Dispute Resolution) and any other terms which by their nature should continue to apply, will survive any termination or expiration of this Agreement.</p>
                                        </li>
                                        <li>
                                            <p>19.5 If Your account or this Agreement are terminated for any reason then You may not without Shieldtasker's consent (in its absolute discretion) create any further accounts with Shieldtasker and we may terminate any other accounts You operate.</p>
                                        </li>
                                    </ul>
                                    <h3>20. GENERAL</h3>
                                    <ul>
                                        <li>
                                            <p>20.1 This Agreement is governed by the laws specified in Your Country Specific Terms.</p>
                                        </li>
                                        <li>
                                            <p>20.2 The provisions of this Agreement are severable, and if any provision of this Agreement is held to be invalid or unenforceable, such provision may be removed and the remaining provisions will be enforceable.</p>
                                        </li>
                                        <li>
                                            <p>20.3 This Agreement may be assigned or novated by Shieldtasker to a third party without your consent. In the event of an assignment or novation the User will remain bound by this Agreement.</p>
                                        </li>
                                        <li>
                                            <p>20.4 This Agreement sets out the entire understanding and agreement between the User and Shieldtasker with respect to its subject matter.</p>
                                        </li>
                                    </ul>
                                    <p><strong>Revised July 2019 copyright Shieldtasker 2019</strong></p>
                                    <h1>APPENDIX A:</h1>
                                    <h2>MODEL TASK CONTRACT</h2>
                                    <p>The terms used in this Task Contract have the meaning set out in the Shieldtasker Glossary. A Task Contract is created in accordance with the Shieldtasker Agreement. Unless otherwise agreed, the Poster and the Tasker enter into a Task Contract on the following terms:</p>
                                    <ul>
                                        <li>
                                            <h3>1 COMMENCEMENT DATE AND TERM</h3>
                                            <ul>
                                                <li>
                                                    <p>1.1 The Task Contract is created when the Poster accepts the Tasker's Offer on a Posted Task to provide Services. When using Search Assist, the Task Contract is created when the Tasker makes an Instant Claim.</p>
                                                </li>
                                                <li>
                                                    <p>1.2 The Contract will continue until terminated in accordance with clause 7.</p>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <h3>2 SERVICES</h3>
                                            <ul>
                                                <li>
                                                    <p>2.1 The Tasker will perform Services in a proper and workmanlike manner.</p>
                                                </li>
                                                <li>
                                                    <p>2.2 The Tasker must perform the Services at the time and location agreed.</p>
                                                </li>
                                                <li>
                                                    <p>2.3 The parties must perform their obligations in accordance with any other terms or conditions agreed by the parties during or subsequent to the creation of the Task Contract.</p>
                                                </li>
                                                <li>
                                                    <p>2.4 The parties acknowledge that the Task Contract is one of personal service where the Poster selected the Tasker to perform the Services. Therefore the Tasker must not subcontract any part of the Services to any third party without the Poster's consent.</p>
                                                </li>
                                                <li>
                                                    <p>2.5 The Tasker remains responsible and liable at all times to the Poster for any acts or omissions of a subcontractor as if those acts or omissions had been made by the Tasker.</p>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <h3>3 WARRANTIES</h3>
                                            <ul>
                                                <li>
                                                    <p>3.1 Each party warrants that the information provided in the creation of the Task Contract is true and accurate.</p>
                                                </li>
                                                <li>
                                                    <p>3.2 The Tasker warrants that they have (and any subcontractor has) the right to work and provide Services and hold all relevant licences in the jurisdiction where the Services are performed.</p>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <h3>4 PAYMENT OR CANCELLATION</h3>
                                            <ul>
                                                <li>
                                                    <p>4.1 Upon the creation of the Task Contract, the Poster must pay the Agreed Price into the Payment Account.</p>
                                                </li>
                                                <li>
                                                    <p>4.2 Upon the Services being completed, the Tasker will provide notice on the Shieldtasker Platform.</p>
                                                </li>
                                                <li>
                                                    <p>4.3 The Poster will be prompted to confirm the Services are complete. If the Tasker has completed the Services in accordance with clause 2, the Poster must use the Shieldtasker Platform to release the Tasker Funds from the Payment Account. For Recurring Services the Tasker Funds for an Occurrence will automatically be released by Shieldtasker from the Payment Account to the Tasker unless paused by the Poster in accordance with the User's Shieldtasker Agreement.</p>
                                                </li>
                                                <li>
                                                    <p>4.4 If the parties agree to cancel the Task Contract, or the Poster is unable to contact the Tasker to perform the Task Contract, the Tasker Funds will be dealt with in accordance with the User's Shieldtasker Agreement.</p>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <h3>5 LIMITATION OF LIABILITY</h3>
                                            <ul>
                                                <li>
                                                    <p>5.1 Except for liability in relation to a breach of a Non-excludable Condition, the parties exclude all Consequential Loss arising out of or in connection to the Services, and any claims by any third person, or the Task Contract, even if the party causing the breach knew the loss was possible or the loss was otherwise foreseeable.</p>
                                                </li>
                                                <li>
                                                    <p>5.2 Subject to any insurance or agreement to the contrary, the liability of each party to the other except for a breach of any Non-Excludable Condition is capped at the Agreed Price.</p>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <h3>6 DISPUTES</h3>
                                            <ul>
                                                <li>
                                                    <p>6.1 If a dispute arises between the parties, the parties will attempt to resolve the dispute within 14 days by informal negotiation (by phone, email or otherwise).</p>
                                                </li>
                                                <li>
                                                    <p>6.2 If the parties are unable to resolve the dispute in accordance with clause 6.1, either party may refer the dispute to Shieldtasker and act in accordance with clause 18 of the Shieldtasker Agreement.</p>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <h3>7 TERMINATION OF CONTRACT</h3>
                                            <p>The Task Contract will terminate when:</p>
                                            <ul>
                                                <li>
                                                    <p>(a) the Services are completed and the Agreed Price is released from the Payment Account;</p>
                                                </li>
                                                <li>
                                                    <p>(b) a party is terminated or suspended from the Shieldtasker Platform, at the election of the other party;</p>
                                                </li>
                                                <li>
                                                    <p>(c) otherwise agreed by the parties or the Third Party Dispute Service; or</p>
                                                </li>
                                                <li>
                                                    <p>(d) notified by Shieldtasker in accordance with the party's Shieldtasker Agreement.</p>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <h3>8 APPLICATION OF POLICIES</h3>
                                            <p>The parties incorporate by reference the applicable Policies.</p>
                                        </li>
                                        <li>
                                            <h3>9 GOVERNING LAW</h3>
                                            <p>The Task Contract is governed by the laws of the jurisdiction where the Posted Task was posted on the Shieldtasker Platform.</p>
                                        </li>
                                    </ul>
                                    <p><strong>Revised  July 2019 copyright Shieldtasker 2019</strong></p>
                                    <h1>APPENDIX B:</h1>
                                    <h2>COUNTRY SPECIFIC TERMS</h2>
                                    <h3>1.  Australian Terms</h3>
                                    <p>If You are a User who has Your Shieldtasker Platform account in (or the Services are performed in) Australia then the following terms will also apply to or may vary this Agreement to the extent specified:</p>
                                    <p>a)  a reference to A$, $A, dollar or $ is to Australian currency;</p>
                                    <p>b)  "Shieldtasker" means Shieldtasker Pty Ltd ACN 149 850 457;</p>
                                    <p>c)  "ACL" means the Australian Consumer Law;</p>
                                    <p>d)  "Consumer Guarantees" means the consumer guarantees contained in Part 3-2 of the ACL;</p>
                                    <p>e)  "Personal Information" has the same meaning given to it in the Privacy Act 1988 (Cth);</p>
                                    <p>f)  This Agreement is governed by the laws of New South Wales, Australia. You and Shieldtasker submit to the exclusive jurisdiction of the courts of New South Wales, Australia; and</p>
                                    <p>g)  The following is added as clause 3.3 in the Model Contract of Appendix A "3.3 The parties incorporate the Consumer Guarantees into the Task Contract, even if they are not already incorporated by law.";</p>
                                    <p>h)  The exclusions and limitations of liability shall be as follows:</p>
                                    <ul>
                                        <li>
                                            <p>(1)   Except for liability in relation to breach of Non-excludable Condition, to the extent permitted by law, Shieldtasker specifically disclaims all liability for any loss or damage (actual, special, direct, indirect and consequential) of every kind and nature, known and unknown, suspected and unsuspected, disclosed and undisclosed (including, without limitation, loss or damage relating to any inaccuracy of information provided, or the lack of fitness for purpose of any goods or service supplied), arising out of or in any way connected with any transaction between Posters and Taskers.</p>
                                        </li>
                                        <li>
                                            <p>(2)   Except for liability in relation to a breach of any Non-excludable Condition, to the extent permitted by law, Shieldtasker specifically disclaims all liability for any loss or damage (actual, special, direct, indirect and consequential) of every kind and nature, known and unknown, suspected and unsuspected, disclosed and undisclosed (including, without limitation, loss or damage relating to any inaccuracy of information provided, or the lack of fitness for purpose of any goods or service supplied), arising out of or in any way connected with any transaction between You and any Third Party Service provider who may be included from time to time on the Shieldtasker Platform.</p>
                                        </li>
                                        <li>
                                            <p>(3)   Except for liability in relation to a breach of any Non-excludable Condition, and to the extent permitted by law, Shieldtasker is not liable for any Consequential Loss arising out of or in any way connected with the Shieldtasker Services.</p>
                                        </li>
                                        <li>
                                            <p>(4)   Except for liability in relation to a breach of any Non-excludable Condition, Shieldtasker's liability to any User of the Shieldtasker Service is limited to the total amount of payment made by that User to Shieldtasker during the twelve month period prior to any incident causing liability of Shieldtasker, or AUD$50.00, whichever is greater.</p>
                                        </li>
                                        <li>
                                            <p>(5)   Shieldtasker's liability to You for a breach of any Non-excludable Condition (other than a Non-excludable Condition that by law cannot be limited) is limited, at our option to any one of resupplying, replacing or repairing, or paying the cost of resupplying, replacing or repairing goods in respect of which the breach occurred, or supplying again or paying the cost of supplying again, services in respect of which the breach occurred.</p>
                                        </li>
                                    </ul>
                                    <h3>2.  UK Terms</h3>
                                    <p>If You are a User who has Your Shieldtasker Platform account in (or the Services are performed in) the UK then the following terms will also apply to or may vary this Agreement to the extent specified:</p>
                                    <p>a)  "Shieldtasker" means Shieldtasker UK Limited is a company registered in England and Wales under company number 10905556, with its registered office at 11 Ashley Piece, Ramsbury, Marlborough, United Kingdom, SN8 2QE, email support@shieldtasker.com and VAT number 280770787;</p>
                                    <p>b)  A reference to "£" is to Great British pound sterling;</p>
                                    <p>c)  The implied terms included in the Non-Excludable Conditions include the implied terms under the Consumer Rights Act 2015, which include (without limitation) that Shieldtasker's services under this Agreement are performed with reasonable care and skill;</p>
                                    <p>d)  The exclusions and limitations of liability shall be as follows:</p>
                                    <ul>
                                        <li>
                                            <p>(1) Nothing in this Agreement excludes or limits Shieldtasker's liability for:</p>
                                            <ul>
                                                <li>
                                                    <p>(i) death or personal injury caused by our negligence;</p>
                                                </li>
                                                <li>
                                                    <p>(ii) fraud or fraudulent misrepresentation; and</p>
                                                </li>
                                                <li>
                                                    <p>(iii) any matter in respect of which it would be unlawful for Shieldtasker to exclude or restrict its liability.</p>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <p>(2) If Shieldtasker fails to comply with the terms of this Agreement, Shieldtasker is responsible for loss or damage You suffer that is a foreseeable result of Shieldtasker's breach of this Agreement or Shieldtasker's negligence, but Shieldtasker is not responsible for any loss or damage that is not foreseeable. Loss or damage is foreseeable if it was an obvious consequence of Shieldtasker's breach or if it was contemplated by You and Shieldtasker at the time that this Agreement became binding.</p>
                                        </li>
                                        <li>
                                            <p>(3) Save as set out in paragraph (1) above, Shieldtasker shall not be liable to You for any loss or damage arising out of or in any way connected with:</p>
                                            <ul>
                                                <li>
                                                    <p>(i) any transaction between Posters and Taskers; and</p>
                                                </li>
                                                <li>
                                                    <p>(ii) any Third Party Service provider who may be included from time to time on the Shieldtasker Platform.</p>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <p>(4) Save as set out in paragraph (1) above, Shieldtasker's liability to any User under this Agreement is limited to the total amount of payment made by that User to Shieldtasker during the twelve month period prior to any incident causing liability of Shieldtasker, or £30.00, whichever is greater;</p>
                                        </li>
                                    </ul>
                                    <p>e) The governing law and jurisdiction shall be as follows:</p>
                                    <ul>
                                        <li>
                                            <p>(1) This Agreement is governed by English law. This means that Your access to and use of the Shieldtasker Platform, and any dispute or claim arising out of or in connection therewith (including non-contractual disputes or claims) will be governed by English law.</p>
                                        </li>
                                        <li>
                                            <p>(2) You can bring proceedings in respect of this Agreement in the English courts. However, as a consumer, if You live in another European Union member state You can bring legal proceedings in respect of this Agreement in either the English courts or the courts of that Member State.</p>
                                        </li>
                                        <li>
                                            <p>(3) As a consumer, if You are resident in the European Union and Shieldtasker directs the Shieldtasker Platform to the member state in which You are resident, You will benefit from any mandatory provisions of the law of the country in which You are resident. Nothing in this Agreement, including paragraph (1) above, affects Your rights as a consumer to rely on such mandatory provisions of local law.</p>
                                        </li>
                                        <li>
                                            <p>(4) If we are not able to satisfy Your complaint about our Services, then Your complaint can be addressed to the Online Dispute Resolution website at http://ec.europa.eu/consumers/odr/ an official website managed by the European Commission dedicated to helping consumers and traders resolve their disputes out-of-court.</p>
                                        </li>
                                    </ul>
                                    <p>f) The following tax provisions shall apply:</p>
                                    <ul>
                                        <li>
                                            <p>(1) If You are a Tasker, You must have the right to provide Services under a Task Contract and to work in the United Kingdom and You must not be established for VAT purposes outside the United Kingdom. You must comply with tax and regulatory obligations in relation to any payment (including the Tasker Funds) received under a Task Contract. You must not sell goods as a separate supply through the Shieldtasker Platform. If there is an incidental supply of goods as part of a supply of Services, then paragraph (2) will apply.</p>
                                        </li>
                                        <li>
                                            <p>(2) If the supply of any goods is part of a single supply of services for VAT purposes (and is not a separate supply), then the consideration for such goods will be part of the Agreed Price. If You are a Tasker and you supply any goods in conjunction with Your Services and such goods would constitute a separate supply for VAT purposes, then:</p>
                                            <ul>
                                                <li>
                                                    <p>(i) You must supply such goods outside of the Shieldtasker Platform and under a separate contract between You and the Poster; and</p>
                                                </li>
                                                <li>
                                                    <p>(ii) the Agreed Price shall not constitute consideration for such goods and be dealt with in accordance with paragraph (1) above.</p>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                    <p>g) The Service Fee and/or the Cancellation Admin Fee only becomes payable to Shieldtasker under this Agreement once Shieldtasker has performed its services. Therefore, You are not entitled to cancellation rights under the Consumer Contracts (Information, Cancellation and Additional Charges) Regulations 2013 in relation to the Service Fee and/or the Cancellation Admin Fee.</p>
                                    <h3>3.  <strong>Ireland Terms</strong></h3>
                                    <p>If You are a User who has Your Shieldtasker Platform account in (or the Services are performed in) Ireland then the following terms will also apply to or may vary this Agreement to the extent specified:</p>
                                    <p>a)  "Shieldtasker" means Shieldtasker UK Limited a company registered in England and Wales under company number 10905556, with its registered office at 11 Ashley Piece, Ramsbury, Marlborough, United Kingdom, SN8 2QE,  email support@shieldtasker.com and VAT number 280770787;</p>
                                    <p>b)  A reference to "€" is to Euro, the currency of certain European Union countries including Ireland;</p>
                                    <p>c)  The Non-Excludable Conditions include (i) the implied terms under the Sale of Goods and Supply of Services Act 1893 -1980 (as amended), which include (without limitation) that Shieldtasker's Services under this Agreement will be performed with due skill, care and diligence and that materials used will be sound and fit for purpose.</p>
                                    <p>d)  The exclusions and limitations of liability shall be as follows:</p>
                                    <ul>
                                        <li>
                                            <p>(1) Nothing in this Agreement excludes or limits Shieldtasker's liability for:</p>
                                            <ul>
                                                <li>
                                                    <p>(i) death or personal injury caused by Shieldtasker’s negligence;</p>
                                                </li>
                                                <li>
                                                    <p>(ii) fraud or fraudulent misrepresentation; and</p>
                                                </li>
                                                <li>
                                                    <p>(iii) any matter in respect of which it would be unlawful for Shieldtasker to exclude or restrict its liability.</p>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <p>(2) If Shieldtasker fails to comply with the terms of this Agreement, Shieldtasker is responsible for loss or damage You suffer that is a foreseeable result of Shieldtasker's breach of this Agreement or Shieldtasker's negligence, but Shieldtasker is not responsible for any loss or damage that is not foreseeable. Loss or damage is foreseeable if it was an obvious consequence of Shieldtasker's breach or if it was contemplated by You and Shieldtasker at the time that this Agreement became binding.</p>
                                        </li>
                                        <li>
                                            <p>(3) Save as set out in paragraph (1) above, Shieldtasker shall not be liable to You for any loss or damage arising out of or in any way connected with:</p>
                                            <ul>
                                                <li>
                                                    <p>(i) any transaction between Posters and Taskers; and</p>
                                                </li>
                                                <li>
                                                    <p>(ii) any Third Party Service provider who may be included from time to time on the Shieldtasker Platform.</p>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <p>(4) Save as set out in paragraph (1) above, Shieldtasker's liability to any User under this Agreement is limited to the total amount of payment made by that User to Shieldtasker during the twelve month period prior to any incident causing liability of Shieldtasker, or €30.00, whichever is greater;</p>
                                        </li>
                                    </ul>
                                    <p>e) The governing law and jurisdiction shall be as follows:</p>
                                    <ul>
                                        <li>
                                            <p>(1) This Agreement shall be governed by Irish law.  This means that Your access to and use of the Shieldtasker Platform, and any dispute or claim arising out of or in connection with it (including non-contractual disputes or claims) will be governed by Irish law.</p>
                                        </li>
                                        <li>
                                            <p>(2) You can bring proceedings in respect of this Agreement in the Irish courts.  However, as a consumer, if You live in another European Union Member State You can bring legal proceedings in respect of this Agreement in either the Irish courts or the courts of that Member State.</p>
                                        </li>
                                        <li>
                                            <p>(3) As a consumer, if You are resident in the European Union and Shieldtasker directs the Shieldtasker Platform to the Member State in which You are resident, You will benefit from any mandatory provisions of the law of the country in which You are resident.  Nothing in this Agreement, including paragraph (1) above, affects Your rights as a consumer to rely on such mandatory provisions of local law.</p>
                                        </li>
                                        <li>
                                            <p>(4) If we are not able to satisfy Your complaint about our Services, then Your complaint can be addressed to the Online Dispute Resolution website at http://ec.europa.eu/consumers/odr/  an official website managed by the European Commission dedicated to helping consumers and traders resolve their disputes out-of-court.</p>
                                        </li>
                                    </ul>
                                    <p>f) The following tax provisions shall apply:</p>
                                    <ul>
                                        <li>
                                            <p>(1) If You are a Tasker, You must have the right to provide Services under a Task Contract and to work in the Republic of Ireland and You must not be established for VAT purposes outside the Republic of Ireland. You must comply with tax and regulatory obligations in relation to any payment (including the Tasker Funds) received under a Task Contract. You must not sell goods as a separate supply through the Shieldtasker Platform. If there is an incidental supply of goods as part of a supply of Services, then paragraph (2) will apply.</p>
                                        </li>
                                        <li>
                                            <p>(2) If the supply of any goods is part of a single supply of services for VAT purposes (and is not a separate supply), then the consideration for such goods will be part of the Agreed Price. If You are a Tasker and You supply any goods in conjunction with Your Services and such goods would constitute a separate supply for VAT purposes, then:</p>
                                            <ul>
                                                <li>
                                                    <p>(i) You must supply such goods outside of the Shieldtasker Platform and under a separate contract between You and the Poster; and</p>
                                                </li>
                                                <li>
                                                    <p>(ii) the Agreed Price shall not constitute consideration for such goods and be dealt with in accordance with paragraph (1) above.</p>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                    <p>g) The Service Fee and/or the Cancellation Admin Fee only becomes payable to Shieldtasker under this Agreement once Shieldtasker has performed its services. Therefore, You are not entitled to cancellation rights under the European Union (Consumer Information, Cancellation and Other Rights) Regulations 2013 in relation to the Service Fee and/or the Cancellation Admin Fee.</p>
                                    <h1>Shieldtasker Glossary</h1>
                                    <p>"<strong>Agreement</strong>" means the most updated version of the agreement between Shieldtasker and a User.</p>
                                    <p>"<strong>Agreed Price</strong>" means agreed price for Services (including any variation) paid into the Payment Account made by the Poster but does not include any costs incurred by the Tasker when completing Services which the Poster agrees to reimburse.</p>
                                    <p>"<strong>Shieldtasker</strong>" means "<strong>we</strong>" "<strong>us</strong>" "<strong>our</strong>" means the legal entity prescribed in Your Country Specific Terms.</p>
                                    <p>"<strong>Shieldtasker Badge</strong>" means a badge that may be issued to a User based on the User meeting certain qualifications or other thresholds, including Verification Icons, as determined and set by Shieldtasker.</p>
                                    <p>"<strong>Shieldtasker Platform</strong>" means the Shieldtasker website at&nbsp;<a href="http://www.shieldtasker.com/">http://www.shieldtasker.com/</a>, Shieldtasker smartphone app, and any other affiliated platform that may be introduced from time to time.</p>
                                    <p>"<strong>Shieldtasker Service</strong>" means the service of providing the Shieldtasker Platform.</p>
                                    <p>"<strong>Badge</strong>" means an Shieldtasker Badge and Verification Icon.</p>
                                    <p>"<strong>Business Day</strong>" means a day on which banks are open for general business in the jurisdiction where Users have their Shieldtasker Platform account, other than a Saturday, Sunday or public holiday.</p>
                                    <p>"<strong>Business Partner Contract</strong>" means a contract between a Business Partner and a Tasker to perform Business Services.</p>
                                    <p>"<strong>Business Partner</strong>" means the business or individual that enters into an agreement with Shieldtasker to acquire Business Services.</p>
                                    <p>"<strong>Business Services</strong>" means Services provided by a Tasker to a Business Partner acquired for the purpose of on selling to a third party (such as the Business Partner's customer).</p>
                                    <p>"<strong>Cancellation Admin Fee</strong>" means the Fee payable by a Poster or a Tasker for cancelling a Task Contract and will not exceed 22% of the Agreed Price.</p>
                                    <p>"<strong>Consequential Loss</strong>" means any loss, damage or expense recoverable at law:</p>
                                    <ul>
                                        <li>
                                            <p>(a) other than a loss, damage or expense that would be suffered or incurred by any person in a similar situation to the person suffering or incurring the loss, damage or expense; or</p>
                                        </li>
                                        <li>
                                            <p>(b) which is a loss of:</p>
                                            <ul>
                                                <li>
                                                    <p>a. opportunity or goodwill;</p>
                                                </li>
                                                <li>
                                                    <p>b. profits, anticipated savings or business;</p>
                                                </li>
                                                <li>
                                                    <p>c. data; or</p>
                                                </li>
                                                <li>
                                                    <p>d. value of any equipment,</p>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                    <p>and any costs or expenses incurred in connection with the foregoing.</p>
                                    <p>"<strong>Country Specific Terms</strong>" means those terms set out in Appendix B.</p>
                                    <p>"<strong>Fees</strong>" means all fees payable to Shieldtasker by Users including the Service Fee.</p>
                                    <p>"<strong>Identity Verification Services</strong>" means the tools available to help Users verify the identity, qualifications or skills of other Users including mobile phone verification, verification of payment information, References, integration with social media, Shieldtasker Badges and Verification Icons.</p>
                                    <p>"<strong>Instant Claim</strong>" means the acceptance of an Offer by a Tasker via the Search Assist function.</p>
                                    <p>"<strong>Marketing Material</strong>" means any updates, news and special offers in relation to Shieldtasker or its Third Party Services.</p>
                                    <p>"<strong>Non-excludable Condition</strong>" means any implied condition, warranty or guarantee in a contract, the exclusion of which would contravene the law or cause any part of the contract to be void. Further detail on the Non-excludable Conditions for consumers in the United Kingdom and in Ireland is set out in the relevant Your Country Specific Terms.</p>
                                    <p>"<strong>Occurrence</strong>" means each individual occurrence of services to be performed by a Tasker that form part of Recurring Services.</p>
                                    <p>"<strong>Offer</strong>" means an offer made by a Tasker in response to a Posted Task to perform the Services. or an offer made by a Poster for the performance of Services by a Tasker when using Search Assist</p>
                                    <p>"<strong>Payment Account</strong>" means the account operated by the Payment Provider.</p>
                                    <p>"<strong>Payment Provider</strong>" means an entity appointed by Shieldtasker that manages and operates the Payment Account including accepting payments from and making payments to Users.</p>
                                    <p>"<strong>Personal Information</strong>" has the same meaning as described in Your Country Specific Terms.</p>
                                    <p>"<strong>Policies</strong>" means the policies posted by Shieldtasker on the Shieldtasker Platform, including but not limited to the Community Guidelines.</p>
                                    <p>"<strong>Poster</strong>" means a User that posts on the Shieldtasker Platform in search of particular Services.</p>
                                    <p>"<strong>Posted Task</strong>" means the Poster's request for Services published on the Platform (including via Search Assist), and includes the deadline for completion, price and description of the Services to be provided.</p>
                                    <p>"<strong>Poster Service Fee</strong>" means the fee payable by the Poster to Shieldtasker as consideration for the Shieldtasker Services (and comprised as part of the Agreed Price) displayed to a Poster prior to entering into each Task Contract</p>
                                    <p>"<strong>Recurring Services</strong>" means the same services procured by a Poster using the Search Assist feature from the same Tasker on a recurring basis, for example weekly, fortnightly or monthly.</p>
                                    <p>"<strong>Reference</strong>" means a feature allowing a User to request other Users to post a reference on the Shieldtasker Platform endorsing that User.</p>
                                    <p>"<strong>Search Assist</strong>" means a feature of the Shieldtasker Platform whereby a Poster can submit specific details of a Posted Task with Shieldtasker's assistance to calculate the Agreed Price and find potential Taskers to perform the Services. This may also be referred to on the Shieldtasker Platform as "Instant Booking".</p>
                                    <p>"<strong>Service Fee</strong>" means the Poster Service Fee and the Tasker Service Fee.</p>
                                    <p>"<strong>Services</strong>" means the services to be rendered as described in the Posted Task, including any variations or amendments agreed before or subsequent to the creation of a Task Contract and for Recurring Services the Services are the services to be performed under each Occurrence.</p>
                                    <p>"<strong>Stored Value</strong>" means the physical or virtual card, coupon, voucher or code containing credit or a discount or refund provided as credit or anything else identified or described as 'Stored Value' in this Agreement, for use on the Shieldtasker Platform.</p>
                                    <p>"<strong>Task Contract</strong>" means the separate contract which is formed between a Poster and a Tasker for Services. In the absence of, or in addition to, any terms specifically agreed, the model terms of which are included in Appendix A to the Agreement apply to Task Contracts.</p>
                                    <p>"<strong>Tasker</strong>" means a User who provides Services to Posters.</p>
                                    <p>"<strong>Tasker Funds</strong>" means the Agreed Price less the Service Fee.</p>
                                    <p>"<strong>Tasker Service Fee</strong>" means the fee payable by the Tasker to Shieldtasker as consideration for the Shieldtasker Services (and comprised as part of the Agreed Price) displayed to a Tasker prior to entering into each Task Contract.</p>
                                    <p>"<strong>Third Party Dispute Service</strong>" means a third party dispute resolution service provider used to resolve any disputes between Users.</p>
                                    <p>"<strong>Third Party Service</strong>" means the promotions and links to services offered by third parties as may be featured on the Shieldtasker Platform from time to time.</p>
                                    <p>"<strong>User</strong>" or "<strong>You</strong>" means the person who has signed up to use the Shieldtasker Platform, whether as the Poster, Tasker, or otherwise.</p>
                                    <p>"<strong>Verification Icons</strong>" means the icons available to be displayed on a User's profile and any such posts on the Shieldtasker Platform to confirm details such as a User's qualification, license, certificate or other skill.</p>
                                    <h1>Rules of Interpretation:</h1>
                                    <p>In the Shieldtasker Agreement and all Policies, except where the context otherwise requires:</p>
                                    <ul>
                                        <li>
                                            <p>(a) the singular includes the plural and vice versa, and a gender includes other genders;</p>
                                        </li>
                                        <li>
                                            <p>(b) another grammatical form of a defined word or expression has a corresponding meaning;</p>
                                        </li>
                                        <li>
                                            <p>(c) a reference to a document or instrument includes the document or instrument as novated, altered, supplemented or replaced from time to time;</p>
                                        </li>
                                        <li>
                                            <p>(d) the applicable currency shall be the currency specified in Your Country Specific Terms;</p>
                                        </li>
                                        <li>
                                            <p>(e) a reference to a person includes a natural person, partnership, body corporate, association, governmental or local authority or agency or other entity;</p>
                                        </li>
                                        <li>
                                            <p>(f) a reference to a statute, ordinance, code or other law includes regulations and other instruments under it and consolidations, amendments, re-enactments or replacements of any of them;</p>
                                        </li>
                                        <li>
                                            <p>(g) the meaning of general words is not limited by specific examples introduced by including, for example or similar expressions;</p>
                                        </li>
                                        <li>
                                            <p>(h) headings are for ease of reference only and do not affect interpretation;</p>
                                        </li>
                                        <li>
                                            <p>(i) any agreement, representation, warranty or indemnity in favour of two or more parties (including where two or more persons are included in the same defined term) is for the benefit of them jointly and severally; and</p>
                                        </li>
                                        <li>
                                            <p>(j) a rule of construction does not apply to the disadvantage of a party because the party was responsible for the preparation of this agreement or any part of it.</p>
                                        </li>
                                    </ul>
                                    <p><strong>July 2019 copyright Shield 2019</strong></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <Footer/>
            </main>
        )
    }
}
