import React from "react"
import './ForgetPassword.css'

class ForgetPassword extends React.Component{
        constructor(props){
                super(props);
                this.state = {
                }
        }
        render(){
                return (
                        <div className="modal-forgetPassword">
                                <h2 className="modal-forgetPassword__header">Forget Password?</h2>
                                <div className="modal-forgetPassword__body">
                                        <p className="modal-forgetPassword__headline">Enter your email below and we will send you instructions on how to reset your password</p>
                                        <form>
                                        <div className="Signup-form-field flex flex__column">
                                                <label htmlFor="email">Email: </label>
                                                <input name="email" type="email"  value={this.props.email} onChange={(e) => {this.props.onChange(e)}}></input>
                                        </div>
                                        <div  className="create-request-form-button-field">
                                                <button onClick={this.props.closeModal} className="button space--around">Cancel</button>
                                                <button  className="button button--green space--around" onClick={(e) => {
                                                        e.preventDefault();
                                                        this.props.handleSubmit()
                                                        this.props.closeModal()}}>Send</button>
                                        </div>
                                        </form>
                                </div>
                        </div>
                )
        }      
}
export default ForgetPassword;