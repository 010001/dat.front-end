import React from 'react';
import {Link} from 'react-router-dom';
import './Login.css';
import {connect} from "react-redux";
import * as action from "../../store/actions";
import Loader from "../../components/UI/Loader/Loader";
import Backdrop from "../../components/UI/Backdrop/Backdrop";
import Modal from '../../components/UI/Modal/Modal';
import ForgetPassword from "./ForgetPassword/ForgetPassword"
import axios from 'axios';
import configuration from "../../config/config";
import FacebookLogin from "react-facebook-login"
import GoogleLogin from "react-google-login";
import {TiSocialFacebookCircular} from "react-icons/ti/index"
import SquareAnimation from "../../components/SquareAnimation/SquareAnimation";

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: {
                elementConfig: {
                    placeholder: 'kitmanwork@gmail.com'
                },
                validation: {
                    required: true,
                    isEmail: true
                },
                errorMessage: {
                    email: "Not valid Email",
                    required: 'Email is required',
                },
                valid: false,
                value: '',
                cssClass: '',
            },
            password: {
                elementConfig: {
                    placeholder: 'kitmanwork@gmail.com'
                },
                validation: {
                    required: true,
                },
                errorMessage: {
                    required: 'Password is required',
                },
                valid: false,
                value: '',
                cssClass: '',
            },
            checkForget: false,
            checked: true,
            images: ""
        };
        this.modalClose = this.closeModal.bind(this);
    }

    componentWillUnmount(){
        //clear error nessage
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.onAuth(this.state.email.value, this.state.password.value);
    };

    checkValidity(value, rules) {
        let isValid = true;
        if (!rules) {
            return true;
        }

        if (rules.required) {
            isValid = value.trim() !== '' && isValid;
        }

        if (rules.minLength) {
            isValid = value.length >= rules.minLength && isValid
        }

        if (rules.maxLength) {
            isValid = value.length <= rules.maxLength && isValid
        }

        if (rules.isEmail) {
            const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
            isValid = pattern.test(value) && isValid
        }

        if (rules.isNumeric) {
            const pattern = /^\d+$/;
            isValid = pattern.test(value) && isValid
        }

        return isValid;
    }

    onChange = (e) => {
        const updatedFormElement = {
            ...this.state[e.target.name]
        };

        let isValid = this.checkValidity(e.target.value, updatedFormElement.validation);
        if (!isValid) {
            updatedFormElement.cssClass = 'color--red';
        } else {
            updatedFormElement.cssClass = '';
        }
        updatedFormElement.value = e.target.value;
        this.setState({[e.target.name]: updatedFormElement});
    };

    getErrorMessage = (message) => {
        switch (message) {
            case 400:
                return "Incorrect Username or password";
            case 401:
                return "Incorrect Username or password";
            default:
                return "Server Error";
        }
    };
    closeModal = () => {
        this.props.closeModal();
    };
    handleSubmitForgetMessage = async () => {
        this.setState({checkForget: true});
        await axios.post(configuration.api.backend_api + `/api/v1/users/forget${this.state.email.value}`);
    };
    handleFaceBookLogin = async () => {
        console.log("clicked");
    };
    responseGoogle = async (response) => {
        let {email, images, password} = this.state;
        email.value = response.w3.U3.trim();
        let firstName = response.profileObj.givenName;
        let lastName = response.profileObj.familyName;
        password.value = response.accessToken;
        images = response.profileObj.imageUrl;
        let GoogleID = response.googleId;
        try {
                const data = await axios.post(configuration.api.backend_api+"/api/v1/google", {Google:{
                    email: email.value,
                    accessToken: password.value,
                    GoogleID,
                }});
                this.props.socialLogin(data, data.data.user._id, data.data.token);
        } catch (err) {
            var _this = this;
            axios.post(configuration.api.backend_api + `/api/v1/users/signUp`, {
                email: email.value,
                password: response.accessToken
            }).then(async res => {
                if (res.status === 201) {
                    this.setState({errorClass: "no-active"});
                    await this.props.onAuth(this.state.email.value, response.accessToken);
                    await this.props.authGoogleSuccess(res, email.value, images, firstName, lastName, GoogleID, password.value);
                    _this.props.history.push("/sub-sign-up");
                }
            }).catch(err => {
                this.setState({errorClass: "errorClass"});
            })
        }

    }
    responseFacebook = async response => {
        let {email, images} = this.state;
        email.value = response.email;
        // let nameArray = response.name.split(" ");
        let firstName = "";
        let lastName = "";
        images = response.picture.data.url
        let FaceBookID = response.userID;
        try {
            const data = await axios.post(configuration.api.backend_api+"/api/v1/facebook", {FaceBook:{
                    email: email.value,
                    accessToken: response.accessToken,
                    FaceBookID,
                }});
            this.props.socialLogin(data, data.data.user._id, data.data.token);
        } catch (err) {
            var _this = this;
            axios.post(configuration.api.backend_api + `/api/v1/users/signUp`, {
                email: email.value,
                password: response.accessToken
            }).then(async res => {
                if (res.status === 201) {
                    this.setState({errorClass: "no-active"});
                    await this.props.onAuth(this.state.email.value, response.accessToken);
                    await this.props.authFBSuccess(res, email.value, images, firstName, lastName, FaceBookID, response.accessToken);
                    _this.props.history.push("/sub-sign-up");
                }
            }).catch(err => {
                this.setState({errorClass: "errorClass"});
            })
        }
    };

    render() {
        const {history} = this.props;
        if (this.props.isAuth) {
            history.goBack();
            return <div></div>;
        }

        let loader = (
            <div>
                <Backdrop show={true}/>
                <Loader/>
            </div>
        );

        return (
            <div className="Signup__Container SignIn">
                {!!this.props.loading && loader}
                <SquareAnimation/>
                <div className="Signup flex flex__column">
                    <div className="title-box">
                        <p>登入</p>

                    </div>
                    <div className="Signup-body">
                        <form className="Signup-form flex flex__column">
                            <div className="Signup-form-field flex flex__column">
                                <label htmlFor="email">电子邮件: </label>
                                <input name="email" placeholder="电邮" className={this.state.email.cssClass}
                                       value={this.state.email.value}
                                       onChange={this.onChange}></input>
                            </div>
                            <div className="Signup-form-field flex flex__column">
                                <label htmlFor="password">密码: </label>
                                <input name="password" type="password" placeholder="密码"
                                       value={this.state.password.value} className={this.state.password.cssClass}
                                       onChange={this.onChange}></input>
                            </div>
                            {!!this.props.error &&
                            <p className="color--red error-message">{this.getErrorMessage(this.props.error.request.status)}</p>}
                            {this.state.checkForget &&
                            <p className="color--red error-message">We have sent an email for you to reset your
                                passwords</p>}
                                <div className="login-fotpas" onClick={this.props.openModal}>忘记密码?</div>
                            <button className="submit-btn login-btn" onClick={this.handleSubmit}>登录</button>
                            <div className="other-signup-field">
                                <span>或注册</span>
                                <div className="external-signup-option flex flex__row flex--space-between">
                                    <FacebookLogin
                                        appId="581613205714087"
                                        fields="name,email,picture"
                                        autoLoad={false}
                                        redirectUri="http://localhost:3000/login"
                                        callback={this.responseFacebook}
                                        cssClass="option-fb"
                                        textButton="Facebook"
                                        icon={<TiSocialFacebookCircular/>}
                                    />
                                    <GoogleLogin
                                        clientId="265681817663-gt34057isbt70sj0ibo3baeu2k6h9usc.apps.googleusercontent.com"
                                        buttonText="Google"
                                        autoLoad={false}
                                        onSuccess={this.responseGoogle}
                                        onFailure={(this.responseGoogle)}
                                        cookiePolicy={'single_host_origin'}
                                        render={renderProps => (
                                            <button id="option-Gg" onClick={renderProps.onClick}
                                                    disabled={renderProps.disabled}>
                                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                    <path
                                                        d="M22 12.23c0-.707-.063-1.393-.183-2.045h-9.613v3.871h5.491c-.235 1.253-.957 2.31-2.035 3.017v2.511h3.296C20.888 17.837 22 15.275 22 12.23z"
                                                        fill="#3E82F1"></path>
                                                    <path
                                                        d="M12.204 22c2.757 0 5.067-.893 6.752-2.421l-3.296-2.512c-.911.602-2.08.956-3.456.956-2.66 0-4.907-1.759-5.71-4.124h-3.41v2.595C4.763 19.76 8.213 22 12.203 22z"
                                                        fill="#32A753"></path>
                                                    <path
                                                        d="M6.494 13.899A5.848 5.848 0 0 1 6.174 12c0-.657.114-1.298.32-1.899v-2.59h-3.41A9.863 9.863 0 0 0 2 12c0 1.612.396 3.14 1.083 4.489l3.411-2.59z"
                                                        fill="#F9BB00"></path>
                                                    <path
                                                        d="M12.204 5.978c1.496 0 2.843.505 3.898 1.494l2.929-2.87C17.265 2.988 14.955 2 12.204 2c-3.99 0-7.44 2.242-9.12 5.511l3.41 2.59c.803-2.365 3.05-4.123 5.71-4.123z"
                                                        fill="#E74133"></path>
                                                </svg>
                                                Google</button>
                                        )}
                                    />
                                </div>
                            </div>
                        </form>
                        <div className="switchToSignup">
                            <p>没有帐号 ?</p>
                            <Link to='/sign-up' className="switchSignup"><p>注册</p></Link>
                        </div>
                    </div>
                </div>
                <Modal className="modal--offer" show={this.props.isModalOpen} modalClosed={this.modalClose}>
                    <ForgetPassword closeModal={this.modalClose} email={this.state.email.value} onChange={this.onChange}
                                    handleSubmit={this.handleSubmitForgetMessage}/>
                </Modal>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        loading: state.auth.loading,
        error: state.auth.error,
        isAuth: state.auth.token !== null,
        isModalOpen: state.modal.isModalOpen,
    };
};
const mapDispatchToProps = dispatch => {
    return {
        onAuth: (email, password) => dispatch(action.auth(email, password, false)),
        authFBSuccess: (user, email, images, firstName, lastName, ID, accessToken) => dispatch(action.socialFBAuth(user, email, images, firstName, lastName, ID, accessToken)),
        authGoogleSuccess: (user, email, images, firstName, lastName, ID, accessToken) => dispatch(action.socialGoogleAuth(user, email, images, firstName, lastName, ID, accessToken)),
        socialLogin: (user, userId, token) => dispatch(action.googleAuthLogin(user, userId, token)),
        openModal: () => dispatch(action.openModal()),
        closeModal: () => dispatch(action.closeModal()),
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(Login);
