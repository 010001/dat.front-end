import React from 'react';
import {withRouter} from 'react-router-dom';
import SecondaryMenu from "../../../components/MyTasks/SecondaryMenu/SecondaryMenu";
import axios from "axios";
import * as action from "../../../store/actions";
import {connect} from "react-redux";
import configuration from "../../../config/config";
import Modal from "../../../components/UI/Modal/Modal";
import TaskList from "../../../components/MyTasks/TaskList/TaskList"
import EmptyPage from "../../../components/MyTasks/EmptyPage/EmptyPage";
import CreateRequest from "../../../components/MyTasks/CreateRequest/CreateRequest"
import DetailCreateRequest from "../../../components/MyTasks/DetailCreateRequest/DetailCreateRequest";
import PriceCreateRequest from "../../../components/MyTasks/PriceCreateRequest/PriceCreateRequest"

class MyTasks extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: '',
            isUserLogin: false,
            taskTitle:{
                validation:{
                    required:true,
                    minLength: 10
                },
                errorMessage: {
                    required:"Title is required",
                    minLength:"Title should at least has 15 words"
                },
                valid: false,
                value:"",
                cssClass:'',
            },
            taskDetail:{
                validation:{
                    required: true,
                    minLength: 25
                },
                errorMessage: {
                    required:"Detail is required",
                    minLength:"Detail should at least has 25 words"
                },
                valid: false,
                value:"",
                cssClass:'',
            },
            dateTime:new Date(),
            address: {
                validation:{
                    required: true
                },
                errorMessage: {
                    required:"Address is required"
                },
                valid: false,
                value:"",
                cssClass:'',
            },
            currentTaskType:"all",
            titleList:["所有活动", "发布活动",
            "已加入活动"],
            titleTypeList:["all", "open",  "assign", "complete"],
            userId:"",
            price:{
                validation:{
                },
                errorMessage: {
                },
                valid: false,
                value:"",
                cssClass:'',
            },
            totalBudget:{
                validation:{
                },
                errorMessage: {
                },
                valid: false,
                value:"",
                cssClass:'',
            },
            finalBudget: 0,
            budgetTime:{
                validation:{
                },
                errorMessage: {
                },
                valid: false,
                value:"",
                cssClass:'',
            },
            myTasks:[],
            isLoading: false,
            currentMyTasks:[],
            typeLists:["Clean"],
            checkTypeList:[false],
            errorClass:"no-active",
        }
        this.modalClose = this.closeModal.bind(this);
    }
    handleCheckBox = (index) => {
        var {checkTypeList,typeLists} = this.state
        checkTypeList[index] = !checkTypeList[index];
        this.setState({checkTypeList});
        if (checkTypeList[index]){
            this.props.onAdd({transport:typeLists[index]});
        } else {
            this.props.onRemove({transport:typeLists[index]});
        }
    }
    checkValidity(value, rules) {
        let isValid = true;
        if (!rules) {
            return true;
        }
        if (rules.required) {
            isValid = value.trim() !== '' && isValid;
        }
        if (rules.minLength) {
            isValid = value.length >= rules.minLength && isValid
        }
        if (rules.isNumeric) {
            const pattern = /^\d+$/;
            isValid = pattern.test(value) && isValid
        }
        return isValid;
    }
    async componentDidMount(){
        const userId = localStorage.getItem("userId");
        const token = localStorage.getItem("token");
        if (userId != null){
            this.setState({isLoading:true});
            const data = await axios.get(configuration.api.backend_api+`/api/v1/users/me${userId}/${token}`);
            this.setState({
                userId: data.data._id
            })
            const dataTask = await axios.get(configuration.api.backend_api+`/api/v1/events/me/get${this.state.userId}`);
            this.setState({myTasks: dataTask.data, currentMyTasks:dataTask.data, isLoading: false});
        }
    }
    openPostModal =async () => {
        await this.props.openModal();
    }
    onChange = (e) => {
            const updatedFormElement = {
                ...this.state[e.target.name]
            };
            let isValid = this.checkValidity(e.target.value, updatedFormElement.validation);
            updatedFormElement.valid = isValid;
            if (!isValid) {
                    updatedFormElement.cssClass = "color--red";
            } else {
                updatedFormElement.cssClass = "";
            }
            updatedFormElement.value = e.target.value;
            this.setState({[e.target.name]: updatedFormElement});
            if (e.target.name === "price" || e.target.name === "budgetTime"){
                this.setState({finalBudget: this.state.price.value* this.state.budgetTime.value});
            } else {
                this.setState({finalBudget: this.state.totalBudget.value});
            }
    }
    handleChange = date => {
            this.setState({dateTime:date});
    }
    handleChangeText = text => {
        let {address} = this.state;
        address.value = text;
    }
    closeModal =() => {
            this.props.closeModal();
    }
    clickModal = () => {
            if (!this.state.taskTitle.valid || !this.state.taskDetail.valid || !this.props.types.length){
                this.setState({ errorClass: "errorClass" });
            } else {
                this.setState({ errorClass: "no-active" });
                this.props.openSecondModal();
            }
    }
    componentWillUpdate(nextProps, nextState){
        var _this = this
        if (this.state.price !== nextState.price ||  this.state.budgetTime !== nextState.budgetTime ){
            nextState.finalBudget = nextState.price.value*nextState.budgetTime.value;
            _this.state = nextState
           this.setState({finalBudget: nextState.price.value*nextState.budgetTime.value})
        } else if (this.state.totalBudget !== nextState.totalBudget){
            nextState.finalBudget = nextState.totalBudget.value;
            _this.state = nextState;
            this.setState({finalBudget: nextState.totalBudget.value});
        } else if (this.state.myTasks !== nextState.myTasks){
            _this.state = nextState;
            this.setState({myTasks: nextState.myTasks});
        } else if (this.state.currentTaskType !== nextState.currentTaskType){
             _this.state.currentTaskType = nextState.currentTaskType;
            _this.setState({currentTaskType:nextState.currentTaskType});
        } else if (this.state.myTasks !== nextState.myTasks){
                _this.state.myTasks = nextState.myTasks;
                _this.setState({myTasks:  nextState.myTasks});
        } else if (this.state.currentMyTasks !== nextState.currentMyTasks){
            _this.state.currentMyTasks = nextState.currentMyTasks;
            _this.setState({currentMyTasks: nextState.currentMyTasks});
        }
    }
    changeMyTasksType = index => {
           if(this.state.titleTypeList[index] === "all"){
                this.setState({currentMyTasks: this.state.myTasks});
                if (this.state.currentMyTasks.length > 0 && document.getElementsByClassName("open-tasks").length > 0){
                    document.getElementsByClassName("open-tasks")[0].className = " open-tasks active-second";
                }
           } else {
               var _this = this;
               this.setState({currentMyTasks: this.state.myTasks.filter(function(item){
                   return item.status === _this.state.titleTypeList[index];
               })})
               const testTask = this.state.myTasks.filter(function(item){
                   return item.status === _this.state.titleTypeList[index];
                })
               if (testTask.length > 0 && document.getElementsByClassName("open-tasks").length > 0){
                    document.getElementsByClassName("open-tasks")[0].className = "open-tasks active-second";
               }
           };
    }
    handleFormat = (title) => {
        const reg = /\s/g;
        const FormattedTitle = title.replace(reg, '-');
        return FormattedTitle
    }
    handleCreateTask = async () => {
            // const {types} = this.props;
            // const settingD = await axios.get(configuration.api.backend_api+"/api/v1/settings");
            // const taskType = settingD.data.tasks_type;
            // let taskId = 0;
            // for (var i = 0; i < taskType.length; i++){
            //     if (types.trim() === taskType[0].name.trim()){
            //             taskId = taskType[0]._id;
            //             break;
            //     }
            // }
            const task  = {
                name: this.state.taskTitle.value,
                slug: this.handleFormat(this.state.taskTitle.value),
                address: this.state.address.value === "" ? "Online Discussion" : this.state.address.value,
                offerPrice: this.state.finalBudget,
                time: this.state.dateTime,
                user_id: this.props.userId,
                comments: this.state.taskDetail.value,
                status:"open",
                type_id: 1
            }
            try {
                await axios.post(configuration.api.backend_api+`/api/v1/tasks`, task);
                const data = await axios.get(configuration.api.backend_api+`/api/v1/tasks/me/get${this.state.userId}`);
                this.setState({myTasks: data.data});
                this.setState({currentMyTasks: this.state.myTasks});
            } catch(err) {
                    //TODO add one error page
            }
    }
    render() {
        if (!this.state.isLoading){
            return (
                <div className="view-tasks-page">
                    <SecondaryMenu titleList={this.state.titleList} changeTypes={this.changeMyTasksType}/>
                    {this.state.currentMyTasks.length <= 0 ? <EmptyPage openPostModal={this.openPostModal} />
                        : <TaskList tasks={this.state.currentMyTasks}  />}
                    <Modal className="model--offer" show={this.props.isModalOpen} modalClosed={this.modalClose}>
                            <CreateRequest onChange={this.onChange} taskDetail={this.state.taskDetail} taskTitle={this.state.taskTitle}
                                    openSecondModal={this.props.openSecondModal} click={this.clickModal} typeList={this.state.typeLists}
                                    checkboxList={this.state.checkTypeList} handleCheckBox={this.handleCheckBox} errorClass={this.state.errorClass}/>
                    </Modal>
                    <Modal className="modal--offer" show={this.props.isSecondModalOpen} modalClosed={this.modalClose}>
                            <DetailCreateRequest onChange={this.handleChange} dateTime={this.state.dateTime}
                                address={this.state.address} onChangeText={this.onChange}
                                onChangeAddress= {this.handleChangeText} closeModal={this.modalClose}
                                openThirdModal={this.props.openThirdModal}/>
                    </Modal>
                    <Modal className="modal--offer" show={this.props.isThirdModalOpen} modalClosed={this.modalClose}>
                            <PriceCreateRequest price={this.state.price} totalBudget={this.state.totalBudget}
                                budgetTime={this.state.budgetTime}  openSecondModal={this.props.openSecondModal}
                                onChange={this.onChange} handleSubmit={this.handleCreateTask}  closeModal={this.modalClose}/>
                    </Modal>
                </div>
            );
        } else {
            return <div></div>
        }
    }
}
const mapDispatchToProps = dispatch => {
        return {
            openModal: () => dispatch(action.openModal()),
            closeModal: () => dispatch(action.closeModal()),
            openSecondModal:() =>dispatch(action.openSecondModal()),
            openThirdModal:()=> dispatch(action.openThirdModal()),
            onAdd(value){dispatch(action.addTransportation(value));},
            onRemove(value){dispatch(action.removeTransportation(value))},
        };
    };
const mapStateToProps = (state) => {
        return {
            userId: state.auth.userId,
            isModalOpen: state.modal.isModalOpen,
            isSecondModalOpen: state.modal.isSecondModalOpen,
            isThirdModalOpen: state.modal.isThirdModalOpen,
            setting: state.setting,
            types: state.types,
        }
    };
export default connect(
        mapStateToProps, mapDispatchToProps
)(withRouter(MyTasks));
