import React from 'react';
import {withRouter} from 'react-router-dom';
import SecondaryMenu from "../../../components/ViewTasks/SecondaryMenu/SecondaryMenu";
import OpenTasks from "../../../components/ViewTasks/OpenTasks/OpenTasks"
import "./ViewTasks.css";
import axios from "axios";
import Loader from "../../../components/ViewTasks/Loader"
import '@mapbox/mapbox-gl-geocoder/dist/mapbox-gl-geocoder.css';
import 'mapbox-gl/dist/mapbox-gl.css';
import Hexagon from "../../../components/Hexagon/Hexagon";
import {searchTask} from "../../../api/search";
import {getEvents} from "../../../api/event";


export class ViewTasks extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: '',
            distanceMenuFlag : false,
            priceMenuFlag : false,
            typeMenuFlag : false,
            filteredData:[],
            isLoading:false,
            price: [0, 300],
            priceFilteredData:[]
        }
    }

    async loadMapBox() {
        var mapboxgl = require('mapbox-gl/dist/mapbox-gl.js');
        mapboxgl.accessToken = 'pk.eyJ1Ijoia2l0bWFuMjAwMjIwMDIiLCJhIjoiY2sxd3BjOG54MDQ3ajNucWw0NzBqajRyciJ9.oCz-m_TQXx68DtTXL07nSA';
        let map = new mapboxgl.Map({
            container: 'map',
            style: 'mapbox://styles/mapbox/streets-v11',
            center: [-122.420679, 37.772537],
            zoom: 15,
        });

        await this.getLocation();
        const center = this.state.data;
        this.addMarker(center, mapboxgl, map);
        map.setCenter(center);
    }

    getLocation = async () => {
        const url = 'https://api.mapbox.com/geocoding/v5/mapbox.places/10%20cowper%20street%20randwick%20sydney.json?access_token=pk.eyJ1Ijoia2l0bWFuMjAwMjIwMDIiLCJhIjoiY2sxd3BjOG54MDQ3ajNucWw0NzBqajRyciJ9.oCz-m_TQXx68DtTXL07nSA&limit=1';
        const response = await axios.get(url);
        let res = response.data.features[0].center;
        this.setState({data: res});
    };

    addMarker(center, mapboxgl, map) {
        var geojson = {
            type: 'FeatureCollection',
            features: [
                {
                    type: 'Feature',
                    geometry: {
                        type: 'Point',
                        coordinates: center
                    },
                    properties: {
                        title: 'Clean house',
                        description: 'Sydney University Village, 90 Avenue street, Newtown, Australia'
                    }
                },{
                    type: 'Feature2',
                    geometry: {
                        type: 'Point',
                        coordinates: [151.2,-33.9]
                    },
                    properties: {
                        title: 'Movehouse',
                        description: 'Upper Turon,NSW'
                    }
                },{
                    type: 'Feature3',
                    geometry: {
                        type: 'Point',
                        coordinates: [151.234,-33.894]
                    },
                    properties: {
                        title: 'Move house on Monday',
                        description: 'Artamon,Sydney,NSW'
                    }
                },{
                    type: 'Feature4',
                    geometry: {
                        type: 'Point',
                        coordinates: [151.117,-33.858]
                    },
                    properties: {
                        title: 'Catch visitor from kingsford airport',
                        description: '724.1 UNSW Village Gate 2 High Street.'
                    }
                }
            ]
        };

        geojson.features.forEach(function (marker) {
            // create a HTML element for each feature
            var el = document.createElement('div');
            el.className = 'marker';

            // make a marker for each feature and add to the map
            new mapboxgl.Marker(el)
                .setLngLat(marker.geometry.coordinates)
                .setPopup(new mapboxgl.Popup({ offset: 25 }) // add popups
                .setHTML('<h3>' + marker.properties.title + '</h3><p>' + marker.properties.description + '</p>'))
                .addTo(map);
        });
    }
    async componentDidMount() {
        await this.loadMapBox();
        //API View Event
        //this does not need to called twice it will be called in the parent
    }
    priceChange = (price) => {
        this.setState({
            price
        })
    }

    priceFilter = async() => {
        this.setState({isLoading:true});
        let res = await getEvents();
        let data = res;
        let price = this.state.price;
        let filteredData = []
        for(let i = 0; i < data.length; i++) {
            if(data[i].offerPrice >= price[0] && data[i].offerPrice <= price[1]){
                filteredData.push(data[i])
            }
        }
        this.setState({isLoading:false});
        this.setState({priceFilteredData:filteredData});

    }



    elementDisplay = (index) => {
        const {distanceMenuFlag, priceMenuFlag, typeMenuFlag} = this.state;
        if(index === 0) {
            this.setState({distanceMenuFlag: !distanceMenuFlag, priceMenuFlag: false, typeMenuFlag: false});
        }
        else if(index === 1) {
            this.setState({distanceMenuFlag: false, priceMenuFlag: !priceMenuFlag, typeMenuFlag: false});
        }
        else if(index === 2) {
            this.setState({distanceMenuFlag: false, priceMenuFlag: false, typeMenuFlag: !typeMenuFlag});
        }
    }

    elementNotDisplay = () => {
        this.setState({distanceMenuFlag: false, priceMenuFlag: false, typeMenuFlag: false});
    }

    searchTasks = async() => {
        this.setState({isLoading:true});
        let input = document.getElementById("search-input");
        let search = input.value.toLowerCase();
        let data = await searchTask(search);
        this.setState({filteredData: data, isLoading: false});
    };


    render() {
        const { distanceMenuFlag,priceMenuFlag,typeMenuFlag } = this.state;
        return (
            <div className={"view-tasks-page__frame "}>
                <SecondaryMenu searchTasks={this.searchTasks} elementDisplay={this.elementDisplay} elementNotDisplay = {this.elementNotDisplay} distanceMenuFlag={distanceMenuFlag} priceMenuFlag={priceMenuFlag} typeMenuFlag={typeMenuFlag} priceChange={this.priceChange} priceFilter={this.priceFilter} price={this.state.price}/>
                <div id="page-and-screen-content relative" onClick = {() => this.elementNotDisplay()} data-test="viewtasks__background">
                    <main className="view-tasks-page view-tasks-page__no-margin">
                        <div className="view-tasks">
                            <section className="open-tasks__allTasks">
                                <OpenTasks filteredData={this.state.filteredData} priceFilteredData={this.state.priceFilteredData}/>
                            </section>
                            <section className="click-view-page" id="map">

                            </section>
                        </div>
                    </main>
                    <Hexagon size={"lg"} classes={"position-7"}/>
                    <Hexagon size={"lg"} classes={"position-8"}/>
                    <Hexagon size={"lg"} classes={"position-9"}/>
                    <Hexagon size={"xs"} classes={"position-10 "}/>
                    <Hexagon size={"xs"} classes={"position-11 "}/>
                </div>
                <div className="view-tasks-loading">
                    {this.state.isLoading ? (<Loader/>):("") }
                </div>

            </div>
        );
    }
}

export default withRouter(ViewTasks);
