import React from 'react';
import {Link} from 'react-router-dom';
import './Home.css';
import HowItWorks from "../../components/HowItWorks/HowItWorks";
import Services from "../../components/Services/Services";
import Thing from '../../components/Thing/Thing';
import Footer from "../../components/Footer/Footer";
import * as action from "../../store/actions";
import {connect} from "react-redux";
import ShieldVideo from "../../components/ShieldVideo/ShieldVideo";
import CardTask from "../../components/CardTask/CardTask";
import axios from "axios";
import configuration from "../../config/config";
import bgimage from '../../img/header_bg.png';
import bgimg from '../../img/brame.png';
import icon1 from '../../img/jam_activity.png';
import icon2 from '../../img/ion_logo-buffer.png';
import icon3 from '../../img/bx_bxs-spreadsheet.png';
import jr from '../../img/logo-jr-academy-transparent-md.png';
import Hexagon from "../../components/Hexagon/Hexagon";
import {onScrollEvent} from '../../components/onScrollEvent/onScrollEvent';
import Mouse from "../../components/Mouse/Mouse";
import {thingAniSheet} from '../../components/Thing/ThingAnimationStylesheet/ThingAnimationSheet'

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            locations: [],
            animationDevice: thingAniSheet.animationDevice,
            animationGroup: thingAniSheet.animationGroup,
            animationBadges: thingAniSheet.animationBadges,
            cookieAlertFlag: true
        };
    }

    async loadMapBox() {
        var mapboxgl = require('mapbox-gl/dist/mapbox-gl.js');
        mapboxgl.accessToken = 'pk.eyJ1Ijoia2l0bWFuMjAwMjIwMDIiLCJhIjoiY2sxd3BjOG54MDQ3ajNucWw0NzBqajRyciJ9.oCz-m_TQXx68DtTXL07nSA';
        let map = new mapboxgl.Map({
            container: 'map2',
            style: 'mapbox://styles/mapbox/streets-v11',
            center: [-122.420679, 37.772537],
            zoom: 15,
        });

        await this.getLocation();
        const center = this.state.locations;
        this.addMarker(center, mapboxgl, map);
        map.setCenter(center);
    }

    getLocation = async () => {
        const url = 'https://api.mapbox.com/geocoding/v5/mapbox.places/10%20cowper%20street%20randwick%20sydney.json?access_token=pk.eyJ1Ijoia2l0bWFuMjAwMjIwMDIiLCJhIjoiY2sxd3BjOG54MDQ3ajNucWw0NzBqajRyciJ9.oCz-m_TQXx68DtTXL07nSA&limit=1';
        const response = await axios.get(url);
        let res = response.data.features[0].center;
        this.setState({locations: res});
    };

    addMarker(center, mapboxgl, map) {
        var geojson = {
            type: 'FeatureCollection',
            features: [
                {
                    type: 'Feature',
                    geometry: {
                        type: 'Point',
                        coordinates: center
                    },
                    properties: {
                        title: 'Clean house',
                        description: 'Sydney University Village, 90 Avenue street, Newtown, Australia'
                    }
                }, {
                    type: 'Feature2',
                    geometry: {
                        type: 'Point',
                        coordinates: [151.2, -33.9]
                    },
                    properties: {
                        title: 'Movehouse',
                        description: 'Upper Turon,NSW'
                    }
                }, {
                    type: 'Feature3',
                    geometry: {
                        type: 'Point',
                        coordinates: [151.234, -33.894]
                    },
                    properties: {
                        title: 'Move house on Monday',
                        description: 'Artamon,Sydney,NSW'
                    }
                }, {
                    type: 'Feature4',
                    geometry: {
                        type: 'Point',
                        coordinates: [151.117, -33.858]
                    },
                    properties: {
                        title: 'Catch visitor from kingsford airport',
                        description: '724.1 UNSW Village Gate 2 High Street.'
                    }
                }
            ]
        };

        geojson.features.forEach(function (marker) {
            // create a HTML element for each feature
            var el = document.createElement('div');
            el.className = 'marker';

            // make a marker for each feature and add to the map
            new mapboxgl.Marker(el)
                .setLngLat(marker.geometry.coordinates)
                .setPopup(new mapboxgl.Popup({offset: 25}) // add popups
                    .setHTML('<h3>' + marker.properties.title + '</h3><p>' + marker.properties.description + '</p>'))
                .addTo(map);
        });
    }

    componentDidMount = async () => {
        window.addEventListener('scroll', this.handleOnScroll);
        let res = await axios.get(configuration.api.backend_api + '/api/v1/events');
        let data = res.data;
        this.setState({
            data,
        });
        await this.loadMapBox();
    };

    componentWillUnmount = () => {
        //memory leak if don't do this
        window.removeEventListener('scroll', this.handleOnScroll);
    };

    handleOnScroll = (e) => {
        const whichAni = onScrollEvent(e)
        if (whichAni) {
            const {dataAni} = whichAni;
            this.setState({
                ...dataAni
            })
        }
    };

    playVideo = () => {
        this.props.openModal();
    };

    setCookie = (cname, cvalue, exdays) => {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toGMTString();
        document.cookie = cname + "=" + cvalue + "; " + expires;
    }

    getCookie = (cname) => {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i].trim();
            if (c.indexOf(name) === 0) return c.substring(name.length, c.length);
        }
        return "";
    }

    agreeCookiePolicy = () => {
        this.setCookie("user", "accepted", 90);
        this.setState({
            cookieAlertFlag: false
        })
    }

    cookieFalgToggle = () => {
        if (this.getCookie("user") && this.state.cookieAlertFlag === true) {
            return !this.state.cookieAlertFlag;
        } else {
            return this.state.cookieAlertFlag;
        }
    }

    render() {
        const {animationDevice, animationGroup, animationBadges} = this.state;

        return (
            <main className="home home-page">
                <section className="container container--banner">
                    <div className={"header-text"}>
                        {/*<Logo/>*/}
                        <h1>选择您想要的活动</h1>
                        <p>在“匠-恋物语”活动中找到您的一半.</p>
                        {!this.props.isAuth &&
                        <Link to="/sign-up" className="browse-button">
                            <button type="button" className="button--lg header__btn">现在开始体验</button>
                        </Link>}
                    </div>
                    <img alt="" className="img--full-size banner-bg"
                         src={bgimage}/>
                    <img alt="" className="banner-bg-img"
                         src={bgimg}/>
                    <Mouse/>

                </section>

                <section className="relative">

                    <div className="container--home">
                        <div className="home-how-it-works--header flex flex--vertical-center">
                            <div className={"join__card text-center"}>
                                <h2 className="clear-space">如何报名</h2>
                                <p className="clear-space">简单的三个步骤 开始您匠人-恋物语寻爱之旅</p>
                                <div className={"flex flex--space-between join__card-steps"}>
                                    <div><img src={icon1}/><p>注册</p></div>
                                    <div><img src={icon2}/><p>报名</p></div>
                                    <div><img src={icon3}/><p>出席</p></div>
                                </div>
                            </div>
                            <div className={"event__card"}>
                                <h2 className="clear-space">我们的活动</h2>
                                <p className="clear-space">我们努力创造高质量的活动让您们进入关系之前能够更多了解对方</p>
                                <a>了解更多></a>

                                <Hexagon size={"lg"} classes={"position-2"}/>
                                <Hexagon size={"lg"} classes={"position-3"}/>
                                <Hexagon size={"xs"} classes={"position-4 Animation--up-down"}/>
                            </div>
                        </div>
                    </div>
                </section>

                <section className="background--grey slider--section">
                    <div className="container--home fade">
                        <h2>我们的活动</h2>
                        <p>我们努力创造高质量的活动让您们进入关系之前能够更多了解对方</p>
                    </div>
                    <section className="home-map" id="map2">

                    </section>
                    <div className="container--home-animation fade">
                        <div className={"inner-container--home-animation animation--scroll-infinite"}>
                            <span>
                                {this.state.data.map((item, i) => <CardTask key={i} data={item}/>)}
                            </span>
                            <span>
                                {this.state.data.map((item, i) => <CardTask key={i} data={item}/>)}
                            </span>
                        </div>
                    </div>
                    <div className="container--home-animation fade">
                        <div className={"inner-container--home-animation animation--scroll-infinite--revert"}>
                            <span>
                                {this.state.data.map((item, i) => <CardTask key={i} data={item}/>)}
                            </span>
                            <span>
                                {this.state.data.map((item, i) => <CardTask key={i} data={item}/>)}
                            </span>
                        </div>
                    </div>
                    {!this.props.isAuth &&
                    <div className={"text-center padding--50"}>

                    </div>
                    }
                </section>


                <section className="background--grey ">
                    <div className="container--home fade">
                        <h2>为什么选择我们？</h2>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget
                            dolor. </p>

                        <Thing
                            animationDevice={animationDevice}
                            items={animationDevice.items}
                            aniName={animationDevice.aniName}
                            id={animationDevice.id}
                            title="长期关系"
                            content="我们平台专注于研究长期关系并创建活动来帮助您了解另一半"
                        />

                        <Thing
                            items={animationGroup.items}
                            aniName={animationGroup.aniName}
                            id={animationGroup.id}
                            title="自然"
                            content="我们的活动能让您在自然不尴尬的情况下去寻找并了解另一半"
                        />

                        <Thing
                            items={animationBadges.items}
                            aniName={animationBadges.aniName}
                            id={animationBadges.id}
                            title="认识"
                            content="在开始任何长期的恋爱关系之前，双方需要用时间去认识彼此，我们的活动能够为您提供这样一个平台"
                        />

                    </div>
                    {!this.props.isAuth &&
                    <div className="flex space-around v-t-34 flex--vertical-center container--home margin-auto fade divider--top">
                        <h4>深受信赖</h4>
                        <img className={"img--auto"} src={jr} alt="JR"/>
                        <Link to="/sign-up" className="browse-button">
                            <button type="button" className="button--lg fade">加 入 我 们</button>
                        </Link>
                    </div>
                    }
                </section>
                {this.cookieFalgToggle() && <div className="cookie-alert">
                    <div className="cookie-alert__container">
                        <label className="cookie-alert__label">Cookies用于确保在官方网站上获得更好的体验。
                            如果您继续使用此网站，则表示您同意根据我们的隐私和缓存政策在此设备上使用缓存。</label>
                        <button href="#" className="cookie-alert__button"
                                onClick={this.agreeCookiePolicy}>是的，谢谢
                        </button>
                    </div>
                </div>}
                <Footer/>
            </main>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        openModal: () => dispatch(action.openModal()),
        closeModal: () => dispatch(action.closeModal())
    };
};

const mapStateToProps = (state) => {
    return {
        isModalOpen: state.modal.isModalOpen,
        userId: state.auth.userId,
        isAuth: state.auth.token !== null,
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);


