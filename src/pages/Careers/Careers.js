import React from 'react';
import './Careers.css';
import Footer from "../../components/Footer/Footer";
import CareersVideo from "../../components/CareersVideo/CareersVideo";
import CareersListing from "../../components/CareersListing/CareersListing";

class Careers extends React.Component {
    constructor(){
        super();
        this.state=[
            {src:"https://www.youtube.com/embed/9YffrCViTVk",
             title:"Lorem ipsum d",
             subtitle:"Designer"

            },
            {src:"https://www.youtube.com/embed/9YffrCViTVk",
             title:"sit amet",
             subtitle:"Product Manager"
            },
            {src:"https://www.youtube.com/embed/9YffrCViTVk",
             title:"Nullam quis ",
             subtitle:"Engineer",
            },
            {src:"https://www.youtube.com/embed/9YffrCViTVk",
             title:"Donec sodales",
             subtitle:"Shield CEO"
            },
            {src:"https://www.youtube.com/embed/9YffrCViTVk",
             title:"Sed consequat",
             subtitle:"2018"
            },
            {src:"https://www.youtube.com/embed/9YffrCViTVk",
            title:"leo eget",
            subtitle:"2018"
           },
        ]
    };




    render() {
        return (
            <main className="careers-page">
                <div className="careers-banner">
                    <div className="careers-banner-main-container">
                        <div className="careers-banner-container">
                            <h1> Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. </h1>
                            <a href="#careers-listing" className="button-cta button-lrg"> See our roles</a>
                        </div>
                    </div>
                </div>
                <div className="careers-videos ">
                    <div className="careers-container regular">
                        <h3>Shield at heart</h3>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, </p>
                        <p>nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu</p>
                        <p>pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut</p>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit</p>
                        <p>Lorem ipsum dolor sit amet</p>
                    </div>
                    <div className="careers-container extra-wide">
                        <div className="careers-video-container fade">
                            <CareersVideo videoDetail={this.state[0]}/>
                            <CareersVideo videoDetail={this.state[1]}/>
                            <CareersVideo videoDetail={this.state[2]}/>
                        </div>
                    </div>
                </div>
                <div className="careers-extra fade">
                    <div className="careers-container narrow">
                        <h3>Extras at Shield</h3>
                        <div className="careers-extra-container">
                            <ul>
                                <li>
                                    <img src="https://via.placeholder.com/102" alt="tick checked"></img>
                                    <p>Phasellus viverra nulla ut metus varius laoreet</p>
                                </li>
                                <li>
                                    <img src="https://via.placeholder.com/102" alt="tick checked"></img>
                                    <p>Etiam ultricies nisi vel augue.</p>
                                </li>
                                <li>
                                    <img src="https://via.placeholder.com/102" alt="tick checked"></img>
                                    <p>Curabitur ullamcorper ultricies nisi. </p>
                                </li>
                                <li>
                                    <img src="https://via.placeholder.com/102" alt="tick checked"></img>
                                    <p> Nam eget dui. Etiam rhoncus.</p>
                                </li>
                                <li>
                                    <img src="https://via.placeholder.com/102" alt="tick checked"></img>
                                    <p>Maecenas tempus, tellus eget condimentum</p>
                                </li>
                                <li>
                                    <img src="https://via.placeholder.com/102" alt="tick checked"></img>
                                    <p>rhoncus, sem quam semper libero</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="careers-listing fade">
                    <div className="careers-listing-jobListingAnchor"></div>
                    <h3 class="careers-listing-jobListingHeading">What we're looking for</h3>
                    <div className="careers-listing-jobList">
                        <div className="careers-listing-jobListingFilter">
                            <div className="careers-listing-jobListingFilterWrapper jobs">
                                <span className="careers-listing-jobListingFilterTitle">Jobs</span>
                                <select>
                                    <option selected value="All positions">All positions</option>
                                    <option value="AirSupport">AirSupport</option>
                                    <option value="Data">Data</option>
                                    <option value="Finance">Finance</option>
                                    <option value="Product">Marketing</option>
                                    <option value="Engineering">Engineering</option>
                                    <option value="People">People</option>
                                </select>
                            </div>
                            <div className="careers-listing-jobListingFilterWrapper locations">
                                <span className="careers-listing-jobListingFilterTitle">Locations</span>
                                <select>
                                    <option selected value="All positions">All cities</option>
                                    <option value="Nullam quis">Nullam quis</option>
                                    <option value="Sydney, Australia">Sydney, Australia</option>
                                </select>
                            </div>
                        </div>
                        <CareersListing/>
                        <CareersListing/>
                        <CareersListing/>
                        <CareersListing/>
                        <CareersListing/>
                        <CareersListing/>
                        <CareersListing/>
                        <CareersListing/>
                    </div>
                    <a className="careers-listing-follow" href="#">Follow us on LinkedIn</a>
                </div>
                <div >
                    <div className="careers-values fade">
                        <div className="careers-container regular">
                            <h3> What we believe</h3>
                            <p className="text-center">Our by-words for greatness, we reckon that if we keep these in mind then we’re on the right track. Do you relate with them? If so, we’d love to meet you!</p>

                        </div>
                        <div className="careers-container extra-wide">
                            <div className="careers-values-container">
                                <div className="careers-values-group">
                                    <div className="careers-values-icon">
                                        <img alt="Stay open" src="https://via.placeholder.com/180"></img>
                                    </div>
                                    <div className="careers-values-caption">
                                        <h4 class="text-center"> Stay open</h4>
                                    </div>
                                </div>
                                <div className="careers-values-group">
                                    <div className="careers-values-icon">
                                        <img alt="Stay open" src="https://via.placeholder.com/180"></img>
                                    </div>
                                    <div className="careers-values-caption">
                                        <h4 class="text-center"> People matter</h4>
                                    </div>
                                </div>                                <div className="careers-values-group">
                                    <div className="careers-values-icon">
                                        <img alt="Stay open" src="https://via.placeholder.com/180"></img>
                                    </div>
                                    <div className="careers-values-caption">
                                        <h4 class="text-center"> When it's on it's on</h4>
                                    </div>
                                </div>                                <div className="careers-values-group">
                                    <div className="careers-values-icon">
                                        <img alt="Stay open" src="https://via.placeholder.com/180"></img>
                                    </div>
                                    <div className="careers-values-caption">
                                        <h4 class="text-center"> Own it</h4>
                                    </div>
                                </div>                                <div className="careers-values-group">
                                    <div className="careers-values-icon">
                                        <img alt="Stay open" src="https://via.placeholder.com/180"></img>
                                    </div>
                                    <div className="careers-values-caption">
                                        <h4 class="text-center"> Fit for puropse</h4>
                                    </div>
                                </div>
                            </div>
                            <div className="careers-container-regular">
                                <a href="/values" target="_blank" className="button-cta button-lrg button-center">Tell me more</a>
                            </div>
                        </div>
                    </div>
                    <div className="careers-videos">
                        <div className="careers-container regular">
                            <h3 class="text-center"> Behind the scenes</h3>
                            <p className="text-center">See some of the impact we’re having in the community.</p>
                        </div>
                        <div className="careers-container extra-wide">
                            <div className="careers-video-container fade">
                            <CareersVideo videoDetail={this.state[3]}/>
                            <CareersVideo videoDetail={this.state[4]}/>
                            <CareersVideo videoDetail={this.state[5]}/>
                            </div>
                        </div>

                    </div>
                </div>
                <Footer/>
            </main>
        );
    }
}

export default Careers;
