import React from "react";
import "./About.css";
import Footer from "../../components/Footer/Footer";
import bgimg from '../../img/header_bg.png';
import tempimg from '../../img/about/Frame.png';
import mountain from '../../img/about/Frame_monutaion.png';
import couple from '../../img/about/couple.png';


export default class About extends React.Component{
    constructor(props) {
        super(props);
        this.state={
            videoIsClose:true,
            fullScreen:false
        }
    }


    pauseVideo=()=> {
        this.refs.vidRef.pause();
        this.setState({
            videoIsClose:true
        })
    };

    playVideo=()=>{
        this.refs.vidRef.play();
        this.setState({
            videoIsClose:false
        })
    }
    playOrPauseVideo=()=>{

        if (this.state.videoIsClose){
            this.playVideo()
        }
        else {this.pauseVideo()};
    }
    fullScreen=()=>{
        this.refs.vidRef.requestFullscreen();
    }

    render(){

        return(
            <main>
                <div className="about">
                    <div className="about__intro">
                        <img alt="" className=""
                             src={bgimg}/>

                        <div className="about__intro-content about__content text-center">

                            <div className="about__intro-container">
                                <h1>让你有更好的约会体验 </h1>
                            </div>
                            <div className="attendButton">
                                <p>加入匠-恋物语</p>
                            </div>
                            <img alt="" className="img--auto"
                                 src={tempimg}/>
                        </div>
                    </div>
                    <div className="about__vision">
                        <div className="about__vision-content about__content">
                            <h3>关注你所关注的</h3>
                            <p>在<span>匠-恋物语</span>，我们致力于通过有意义的联系帮助人们找到长期，爱与及幸福的关系。我们独一无二的算法可帮助您发现你关注的价值。</p>
                        </div>
                    </div>
                    <div className={"flex flex--space-between flex--vertical-center about__advertising"}>
                        <div className="about__statistic">
                            <img className={"width--auto"} src={couple}/>
                        </div>
                        <div className="match-reason__container text--left">
                            <div>
                                <h4>我们喜欢问问题</h4>
                                <p>我们关注您所关注的价值</p>
                            </div>

                            <div>
                                <h4>无论你要找什么</h4>
                                <p>无论您是谁，无论您要找的是什么是什么，匠-恋物语都能帮助您找到有意义的联系联系，无论这对您意味着什么</p>
                            </div>

                            <div>
                                <h4>连接你所关注的</h4>
                                <p>在约会之前，您应该了解的一些事情事情。匠人-恋物语上，我们可以帮助您找到真正与您兼容的人。</p>
                            </div>

                            <div>
                                <h4>关注的你的故事</h4>
                                <p>我们喜欢聆听你的故事/建议使我们的平台变得更好</p>
                            </div>
                        </div>
                    </div>
                    <div className={"text-center flex--vertical-center background--light-grey" +
                    " company-milestone__section"}>
                        <h3 className={"text-center"}>公司里程碑</h3>
                        <div className={"flex container flex--vertical-center flex--space-between width--full"}>
                            <div>
                                <h4>团队价值</h4>
                                <ul>
                                    <li>喜欢聪明地工作not努力工作</li>
                                    <li>主动</li>
                                    <li>创意</li>
                                    <li>青春永驻</li>
                                    <li>沟通</li>
                                    <li>致力于您的团队</li>
                                    <li>最高标准</li>
                                </ul>
                            </div>
                            <img className={"img--auto"} src={mountain}/>
                        </div>
                    </div>
                </div>
                <Footer className="about_footer"/>
            </main>
        )

    };


};




