import React from 'react';
import Questions from "../../../components/Questions/Questions";
import Status from "../../../components/Status/Status";
import SocialMedia from "../../../components/SocialMedia/SocialMedia";
import axios from 'axios';
import {connect} from "react-redux";
import "./ViewEvent.css";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faCalendarDay, faMapMarkerAlt, faUserCircle} from '@fortawesome/free-solid-svg-icons';
import Offer from "../../../components/Offers/Offer/Offer";
import moment from 'moment';
import Modal from "../../../components/UI/Modal/Modal";
import Backdrop from "../../../components/UI/Backdrop/Backdrop";
import Loader from "../../../components/UI/Loader/Loader";
import * as action from "../../../store/actions";
import {completeEvent, viewEvent} from "../../../api/event";
import {Link} from "react-router-dom";
import configuration from "../../../config/config";
import {PosterState, TaskerState} from "../../../config/task";
import UpdateTaskWorkflow from "../../../workflow/UpdateEventWorkflow/UpdateEventWorkflow";
import UpdateOfferWorkflow from "../../../workflow/UpdateOfferWorkflow/UpdateOfferWorkflow";
import MoreOptions from '../../../components/MoreOptions/MoreOptions';
import CompleteOffer from "../../../components/Offers/CompleteOffer/CompleteOffer";
import FinishOffer from "../../../components/Offers/FinishOffer/FinishOffer";
import ConfirmModal from "../../../components/Modal/ConfirmModal/ConfirmModal";
import ViewTaskImage from "../../../components/ViewTaskImage/ViewTaskImage";
import {JoinEventWorkflow} from "../../../workflow/JoinEventWorkflow/JoinEventWorkflow";
import PaymentWorkflow from "../../../workflow/PaymentWorkflow/PaymentWorkflow";
import SuccessModal from "../../../components/Modal/SuccessModal/SuccessModal";


const UserEnum = Object.freeze({"POSTER": 1, "TASKER": 2});
Object.freeze(UserEnum);


class ViewEvent extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            isLoading: true,
            data: {},
            error: false,
            collapse: true,
            userState: '',
            taskState: '',
            showAttachment: true,
            collapseOptionStatus: false,
            openAlertModal: false,
            openCancelModal: false,
            openModal: false,
        };


        this.handleChange = this.handleFileChange.bind(this);
        this.modalClose = this.closeModal.bind(this);
    }

    getUserState(data) {
        if (typeof data === "undefined" || data.user_id === null) {
            return false;
        }
        return this.checkIsPoster(data) ? UserEnum.POSTER : UserEnum.TASKER;
    }

    getTaskState(data) {
        let user_id = this.props.userId;
        let userState = UserEnum.TASKER;
        if (typeof data !== "undefined" && data.user_id !== null) {
            userState = data.user_id._id === user_id ? UserEnum.POSTER : UserEnum.TASKER;
        }

        const hasAttendees = data.attendees.length !== 0;

        switch (userState) {
            case UserEnum.POSTER:
                if (!hasAttendees) {
                    return PosterState.AWAIT;
                } else if (data.status === 'open') {
                    return PosterState.REVIEWING;
                } else if (data.status === 'assign') {
                    return PosterState.INPROGRESS;
                } else {
                    return PosterState.COMPLETED;
                }
            case UserEnum.TASKER:
                if (!hasAttendees) {
                    return TaskerState.AWAIT;
                } else if (data.status === 'assign') {
                    return TaskerState.ASSIGNED_OFFER;
                } else if (data.status === 'complete') {
                    return TaskerState.COMPLETED;
                } else {
                    return TaskerState.REVIEWING;
                }
            default:
                if (!hasAttendees) {
                    return TaskerState.AWAIT;
                } else if (data.status === 'assign') {
                    return TaskerState.ASSIGNED_OFFER;
                } else {
                    return TaskerState.COMPLETED;
                }
        }

    }

    async componentDidMount() {
        let data = await viewEvent(this.props.match.params.slug);
        data = data.event;
        // console.log(data.event);
        // if (!data) {
        //     this.props.history.push(`/404`);
        //     return;
        // }
        //5dc0f6b9148a102309ac460e
        //5dc16e24f2be2a3511ce1f15

        this.setState(prevState => ({
            isLoading: false,
            data,
            userState: this.getUserState(data),
            taskState: this.getTaskState(data),
            showAttachment: prevState.showAttachment ? true : false,
        }));
    }

    async handleFileChange(selectorFiles) {
        let formData = new FormData();
        formData.append("photos", selectorFiles[0]);
        let res = await axios.post(configuration.api.backend_api + '/api/v1/upload', formData, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        });
        let location = res.data[0].location;
        this.setState({attachmentLocation: location});
        await axios.post(configuration.api.backend_api + '/api/v1/tasks/addAttachment/' + this.state.data._id, {
            attachments: location,
        }).then((response) => {
            let post = this.state.data;
            post.attachments = response.data.attachments;
            this.setState({
                data: post,
                showAttachment: true,
            })

        }).catch((error) => {
            // this.props.failed(error);
        });

        // TODO: single file upload is avialble, mutiple file upload still need to be fixed.
    }

    collapse = (e) => {
        e.preventDefault();
        if (this.state.collapse) {
            this.setState({
                collapse: false,
            })
        } else {
            this.setState({
                collapse: true,
            })
        }
    };

    openOfferModal = async () => {
        if (!this.props.isAuth) {
            this.props.history.push(`/login`);
            return;
        }
        //!this.state.data.openModal
        this.setState({openModal: !this.state.openModal});
        // console.log(this.state.openModal);
        // await this.props.openModal();
    };

    openAlertModal = async () => {
        if (!this.props.isAuth) {
            this.props.history.push(`/login`);
            return;
        }
        this.setState({
            openAlertModal: true,
        })

    };


    checkAuth = () => {
        if (!this.props.isAuth) {
            this.props.history.push(`/login`);
        }
    }

    hasJoinEvent = () => {
        //backend create offer does not get the userID

        let user_id = this.props.userId;
        return this.state.data.attendees.find(attend => attend.user_id._id === user_id);
    };


    useUpdateTaskBtn = () => {
        return this.state.userState === UserEnum.POSTER && this.state.taskState === PosterState.REVIEWING;
    };


    showJoinEventWorkflow = () => {

        return this.state.userState !== UserEnum.POSTER && !this.hasJoinEvent();
    };

    showJoinedEventWorkflow = () => {
        return this.state.userState !== UserEnum.POSTER && this.hasJoinEvent();
    };

    showEditEventWorkflow = () => {
        return this.state.userState !== UserEnum.TASKER;
    };

    useFinishOffer = () => {
        return this.state.data.status === 'complete';
    };

    toggleAlertModel = () => {
        this.setState({
            openAlertModal: false,
        })
    };

    toggleCancelModel = () => {
        this.setState({
            openCancelModal: false,
        })
    };

    closeModal = () => {
        this.setState({openModal: false});
    };


    addOffer = async (data) => {
        const post = this.state.data;
        post.offers.push(data);
        this.setState({data: post, taskState: this.getTaskState(post)});
        //, taskState: this.getTaskState(post)
    };

    updateOffer = async (data) => {

    };

    updateEvent = async (data) => {

    };

    completeOffer = async (rate, comments) => {
        try {
            await completeEvent({
                rate: rate,
                comments: comments,
                taskId: this.state.data._id,
            });
            let post = this.state.data;
            post.status = "complete";
            this.setState({data: post, taskState: TaskerState.COMPLETED});
            this.props.closeModal();
        } catch (error) {
            console.log("error", error);
        }
    };

    collapseOptions = () => {
        this.setState({
            collapseOptionStatus: (this.state.collapseOptionStatus === false ? true : false),
        })
    };

    elementHidden = (event) => {
        if (event.target.id !== 'moreOption-1' && event.target.id !== 'moreOption-2' && event.target.id !== 'moreOption-3') {
            this.setState({
                collapseOptionStatus: false,
            })
        }
    };

    closeAttachment = () => {
        let post = this.state.data;
        post.attachments = null;

        this.setState({data: post});
    };

    getLoadingComponment = () => {
        return (
            <div>
                <Backdrop show={true}/>
                <Loader/>
            </div>
        )
    };


    componentWillUpdate(nextProps, nextState) {
        if (this.state.data.offers !== nextState.data.offers) {
            this.setState({
                data: nextState.data,
            });
        }
        if (this.state.data.attachments !== nextState.data.attachments) {
            this.setState({
                data: nextState.data,
            });
        }
    }

    successPayment = () => {
        let post = this.state.data;
        post.status = "assign";
        this.setState({data: post, taskState: PosterState.INPROGRESS});
    };

    maxLengthCut(str, maxlength) {
        return str;
        // if (str.length > maxlength) {
        //     return str = str.substr(0, maxlength - 2) + '........'
        // } else return str
    }

    openCancelModal = () => {
        this.setState({openCancelModal: true});
    };

    cancelTask = async () => {
        const data = await axios.delete(configuration.api.backend_api + '/api/v1/tasks/' + this.state.data._id);
        if (data.status === 204) {
            return this.props.history.push(`/`);
        }
    };

    checkIsPoster(data) {
        return data.user_id._id === this.props.userId;
    }

    render() {
        let userName = "abcg";
        if (this.state.isLoading) return this.getLoadingComponment();
        // const offers = this.state.data.attendees.map((item, i) => <Offer key={i} data={item}
        //                                                               taskID={this.state.data._id}
        //                                                               successPayment={this.successPayment}
        //                                                               failPayment={this.failPayment}
        //                                                               state={this.state.taskState}
        //                                                               accept={this.acceptOffer}
        //                                                               image={this.state.data.user_id.images}
        //                                                               withdraw={this.withDrawOffer}/>);

        const attends = this.state.data.attendees.map((item, i) => <img id="user--icon--post"
                                                                        src={"https://via.placeholder.com/256"}
                                                                        alt="usericon"/>);
        //let userName = this.state.data.user_id.name.firstName + " " + this.state.data.user_id.name.lastName;
        userName = this.maxLengthCut(userName, 27);
        let address = this.maxLengthCut(this.state.data.address, 40);
        return (
            <div className={"container--task"} onClick={(e) => {
                this.elementHidden(e)
            }}>
                <section className="ContentHeader flex flex__wrap">
                    <div className={"event-container"}>
                        <div className="details-panel flex flex--space-between">
                            <img alt={""} className="img--full-size feature-img"
                                 src="https://via.placeholder.com/500x300"/>
                            <div className={"event-details-container"}>
                                <h1 className="event-title">{this.state.data.name}</h1>
                                <div className={"flex"}>
                                    <div>
                                        <div className="flex event-info">
                                            <FontAwesomeIcon className="fontawesome" size="2x"
                                                             icon={faUserCircle}/>
                                            <ul>
                                                <li><p className="bold small-title">作者</p></li>
                                                <li>
                                                    {/*<Link to={{*/}
                                                    {/*pathname: `/user/${this.state.data.user_id._id}`,*/}
                                                    {/*state: {id: `${this.state.data.user_id._id}`}*/}
                                                    {/*}}*/}
                                                    {/*className="browse-button">{userName}*/}
                                                    {/*</Link>*/}
                                                    <div className="Time">
                                                        <time
                                                            dateTime="6 days ago">{moment(new Date(this.state.data.createdAt)).format("ddd, D MMM")}</time>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div className="flex event-info">
                                            <FontAwesomeIcon className="fontawesome" size="2x"
                                                             icon={faCalendarDay}/>
                                            <ul>
                                                <li><p className="bold small-title">截止日期</p></li>
                                                <li><p
                                                    className="bold small-title">{moment(new Date(this.state.data.time)).format("ddd, D MMM")}</p>
                                                </li>
                                            </ul>
                                        </div>
                                        <div className="flex event-info">
                                            <FontAwesomeIcon className="fontawesome" size="2x"
                                                             icon={faMapMarkerAlt}/>
                                            <ul>
                                                <li><p className="bold small-title">地点</p></li>
                                                <li><p
                                                    className="bold small-title">{moment(new Date(this.state.data.time)).format("ddd, D MMM")}</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div className={"details--price text--left"}>
                                        <h2>价格</h2>
                                        <h3 id="price">${this.state.data.price}</h3>
                                        <div className="status-btn">
                                            <button onClick={this.openOfferModal}>
                                                {/*{(!!this.useUpdateTaskBtn() && "Update Task")}*/}
                                                {(!!this.showJoinEventWorkflow() && "参加活动")}
                                                {(!!this.showJoinedEventWorkflow() && "已参加")}
                                                {(!!this.showEditEventWorkflow() && "编辑活动")}
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className={"flex event-details-info"}>
                            <div className="description-section">
                                <h2>详情</h2>
                                <p id={"details__content--spread"}
                                   style={{height: this.state.collapse === true ? 10 + "px" : 90 + "px"}}>{this.state.data.comments}
                                </p>

                                <button className="collapse-btn"
                                        onClick={(e) => this.collapse(e)}>{this.state.collapse === true ? "更多" : "更少"}
                                    <div className="collapse-btn-style">
                                        <svg width="20px" height="14px" viewBox="0 0 24 24">
                                            <path
                                                d="M12 16.86a.9.9 0 0 1-.61-.25l-8-8a.86.86 0 0 1 1.22-1.22l7.39 7.4 7.39-7.4a.86.86 0 0 1 1.22 1.22l-8 8a.9.9 0 0 1-.61.25z"></path>
                                        </svg>
                                    </div>
                                </button>
                            </div>
                            <div className={"flex attendess"}>
                                <div className={"flex flex--vertical-center flex--space-between width--full"}>
                                    <h3>参会者({this.state.data.attendees.length})</h3>

                                    <a className={"link--blue"}>查看全部</a>
                                </div>
                                <div>
                                    {attends}
                                </div>
                            </div>
                        </div>
                        {/*<div className={"details-info"}>*/}
                        {/*<div className={"flex flex--vertical-center"}>*/}

                        {/*<div className="Options" onClick={(e) => this.collapseOptions(e)} id="moreOption-1">*/}
                        {/*<p id="moreOption-2">More Options</p>*/}
                        {/*/!* TODO: create list via url & li *!/*/}
                        {/*<div className="collapse-btn-style">*/}
                        {/*<svg width="20px" height="14px" viewBox="0 0 24 24" id="moreOption-3">*/}
                        {/*<path*/}
                        {/*d="M12 16.86a.9.9 0 0 1-.61-.25l-8-8a.86.86 0 0 1 1.22-1.22l7.39 7.4 7.39-7.4a.86.86 0 0 1 1.22 1.22l-8 8a.9.9 0 0 1-.61.25z"></path>*/}
                        {/*</svg>*/}
                        {/*</div>*/}
                        {/*<div className={this.state.collapseOptionStatus ? '' : 'componentUnVisible'}>*/}
                        {/*<MoreOptions openOfferModal={this.openOfferModal}*/}
                        {/*openAlertModal={this.openAlertModal}*/}
                        {/*openCancelModal={this.openCancelModal} taskStatus={this.state.data}*/}
                        {/*taskOwner={this.checkIsPoster() ? true : false}*/}
                        {/*awaitOffer={!!this.useAwaitOffer() ? true : false}/>*/}
                        {/*</div>*/}
                        {/*</div>*/}
                        {/*</div>*/}
                        {/*<div className="flex flex--space-between ">*/}


                        {/*/!*<div className="status-btn">*!/*/}
                        {/*/!*<button onClick={this.openOfferModal}>*!/*/}
                        {/*/!*{(!!this.useUpdateTaskBtn() && "Update Event")}*!/*/}
                        {/*{(!!this.useCreateOfferBtn() && "Join Event")}*/}
                        {/*/!*{(!!this.useUpdateOfferBtn() && "Cancel Event")}*!/*/}
                        {/*/!*{(!!this.useAwaitOffer() && "Awaiting Attendees")}*!/*/}
                        {/*/!*{(!!this.useFinishOffer() && "Finished")}*!/*/}
                        {/*/!*</button>*!/*/}
                        {/*/!*</div>*!/*/}


                        {/*</div>*/}

                        {/*<div className="SocialMedia">*/}
                        {/*<SocialMedia/>*/}
                        {/*</div>*/}
                        {/*<div*/}
                        {/*className={this.checkIsPoster() ? "Attachment" : "componentUnVisible"}>*/}
                        {/*<form method="post" encType="multipart/form-data">*/}
                        {/*<input onChange={(e) => this.handleFileChange(e.target.files)} type="file"*/}
                        {/*multiple/>*/}
                        {/*</form>*/}
                        {/*<button>Add attachment</button>*/}
                        {/*</div>*/}
                        {/*</div>*/}

                    </div>

                </section>
                {/*<section className={"Attachment--section"}>*/}
                {/*{!!this.state.data.attachments && this.state.data.attachments.map((item, i) => item &&*/}
                {/*<ViewTaskImage key={i} img={item} show={this.state.showAttachment}*/}
                {/*closeAttachment={this.closeAttachment}/>)}*/}
                {/*</section>*/}

                {/*<section className="offers">*/}
                {/*<div className="offers-container">*/}
                {/*<h2>Attendees</h2>*/}
                {/*{offers}*/}
                {/*</div>*/}
                {/*</section>*/}
                {/*<div className={this.props.isAuth ? '' : "componentUnVisible"}><Questions/></div>*/}
                {/*<Modal class="modal--offer relative" show={this.state.openCancelModal}*/}
                {/*modalClosed={this.toggleCancelModel}>*/}
                {/*<ConfirmModal confirmClick={this.cancelTask} closeClick={this.toggleCancelModel}/>*/}
                {/*</Modal>*/}
                {/*<Modal class="modal--offer relative" show={this.state.openAlertModal}*/}
                {/*modalClosed={this.toggleAlertModel}>*/}
                {/*<h1>Success</h1>*/}
                {/*<p>We have setup your notification</p>*/}
                {/*<button onClick={this.toggleAlertModel} className="button button-xs--spacing  button--green"*/}
                {/*data-test="updateEvent">Ok*/}
                {/*</button>*/}
                {/*</Modal>*/}
                {(!!this.state.openModal && this.showJoinEventWorkflow() &&
                    <PaymentWorkflow price={this.state.data.price} success={this.props.successPayment}
                                     close={this.modalClose} payerUserID={this.props.userId}
                                     payeeUserID={this.state.data.user_id._id} taskID={this.state.data._id}/>)}
                {(!!this.state.openModal && this.showJoinedEventWorkflow() &&
                    <Modal class="modal--offer relative" show={this.state.openModal}
                           modalClosed={this.modalClose}>
                        <SuccessModal click={this.modalClose}/>)
                    </Modal>
                )}

                {(!!this.state.openModal && this.showEditEventWorkflow() &&
                    <Modal class="modal--offer relative" show={this.state.openModal}>
                        <UpdateTaskWorkflow id={this.state.data._id} success={this.updateEvent} cancelClick={this.modalClose} data={this.state.data}/>
                    </Modal>
                )}
                {/*{(!!this.useUpdateOfferBtn() &&*/}
                {/*)}*/}

                {/*{(!!this.useUpdateTaskBtn() &&*/}
                {/*<UpdateTaskWorkflow id={this.state.data._id} success={this.updateEvent}/>)}*/}
                {/*/!*{(!!this.useAwaitOffer() && <AwaitOffer id={this.state.data._id} />)}*!/*/}

                {/*{this.useCompletedOffer() &&*/}
                {/*<CompleteOffer posterId={this.state.data.user_id} takerId={this.state.data.assigned_id}*/}
                {/*taskId={this.state.data._id} submitClick={this.completeOffer}/>}*/}
                {/*{(!!this.useFinishOffer() && <FinishOffer/>)}*/}
                {/*</Modal>*/}
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        openModal: () => dispatch(action.openModal()),
        closeModal: () => dispatch(action.closeModal())
    };
};

const mapStateToProps = (state) => {
    return {
        isModalOpen: state.modal.isModalOpen,
        userId: state.auth.userId,
        isAuth: state.auth.token !== null,
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ViewEvent);
