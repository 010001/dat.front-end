import React from 'react';

import * as action from "../../../store/actions";
import {connect} from "react-redux";
import DatePicker from "react-datepicker";
import Hexagon from "../../../components/Hexagon/Hexagon";
import {updateEvent} from "../../../api/event";


export class UpdateEvent extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            user_id: {
                value: '',
            },
            title:{
                value: this.props.data.name,
            },
            location: {
                elementConfig: {
                    placeholder: 'kitmanwork@gmail.com'
                },
                validation: {
                    required: true
                },
                errorMessage: {
                    email: "Not valid Email",
                    required: 'Email is required',
                },
                valid: false,
                value: this.props.data.location,
                cssClass: '',
            },
            dueDate: {
                elementConfig: {
                    placeholder: 'sdfsf'
                },
                validation: {
                    required: true,
                },
                errorMessage: {
                    email: "Not valid Email",
                    required: 'Email is required',
                },
                valid: false,
                value:new Date(),
                cssClass: '',
            },
            details: {
                elementConfig: {
                    placeholder: 'kitmanwork@gmail.com'
                },
                validation: {
                    required: true,
                },
                errorMessage: {
                    required: 'Email is required',
                },
                valid: false,
                value: this.props.data.comments,
                cssClass: '',
            },
            price: {
                elementConfig: {
                    placeholder: 'kitmanwork@gmail.com'
                },
                validation: {
                    required: true,
                },
                errorMessage: {
                    email: "Not valid Email",
                    required: 'Email is required',
                },
                valid: false,
                value: this.props.data.price,
                cssClass: '',
            },
            task_id: {
                value: props.id
            },
            startDate: new Date()
        };
        this.handleChange = this.handleChange.bind(this);
    }


    handleChange = (e) => {
        const updatedFormElement = {
            ...this.state[e.target.name]
        };
        let isValid = true;

        if (!isValid) {
            updatedFormElement.cssClass = 'color--red';
        } else {
            updatedFormElement.cssClass = '';
        }
        updatedFormElement.value = e.target.value;
        this.setState({[e.target.name]: updatedFormElement});
    };

    handleDateChange = date => {
        this.setState({
            startDate: date
        });
    };

    updateEvent = async () => {
        //loading animation
        try {
            let res = await updateEvent(this.state.task_id.value, {
                offerPrice: this.state.price.value,
                comments: this.state.details.value,
                time: this.state.dueDate.value,
                address: this.state.location.value
            });
            this.props.success(res.data.offer);
        }
        catch (error) {
            this.props.failed(error);
        }
    };


    render() {
        return (
            <React.Fragment>
                <h1>Update Task</h1>
                <p>Please Update me</p>

                <div className="container--form">
                    <label className="width--full">标题:</label>
                    <input onChange={this.handleChange} name="title" className="width--full" type="text" value={this.state.title.value}/>
                </div>
                <div className="container--form">
                    <label className="width--full">地点:</label>
                    <input onChange={this.handleChange} name="location" className="width--full" type="text" value={this.state.location.value}/>
                </div>
                <div className="container--form">
                    <label className="width--full">地点:</label>
                    <DatePicker selected={this.state.startDate}
                                onChange={this.handleDateChange}/>
                </div>
                <div className="container--form">
                    <label className="width--full">价钱:</label>
                    <input onChange={this.handleChange} name="price" className="width--full" type="text" value={this.state.price.value}/>
                </div>
                <div className="container--form">
                    <label className="width--full">注释:</label>
                    <textarea onChange={this.handleChange} name="details"
                              placeholder="eg. I will be great for this task. I have the necessary experience, skills and equipment required to get this done."
                              className="width--full" value={this.state.details.value}/>
                </div>
                <div className={"flex"}>
                    <button onClick={this.updateEvent} className="button button-xs--spacing  button--green" data-test="updateEvent">提交
                    </button>
                    <button onClick={this.props.cancelClick} className="button button-xs--spacing">取消</button>
                </div>
                <Hexagon size={"xs"} classes={"position-5 color--grey"}/>
                <Hexagon size={"xs"} classes={"position-6 color--grey"}/>
                <Hexagon size={"lg"} classes={"position-7 color--grey"}/>
            </React.Fragment>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        openModal: () => dispatch(action.openModal()),
        closeModal: () => dispatch(action.closeModal())
    };
};



export default connect(null, mapDispatchToProps)(UpdateEvent);
