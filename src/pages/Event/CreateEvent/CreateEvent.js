import React from 'react';
import Banner from "../../../components/Banner/Banner";
import Task from "../../../components/Task/Task";
import './CreateEvent.css';
import Recommendations from "../../../components/Recommendations/Recommendations";
import axios from 'axios';
import Footer from '../../../components/Footer/Footer'
import Hexagon from '../../../components/Hexagon/Hexagon';


class CreateEvent extends React.Component {

    submitTask() {
        axios.post('http://localhost:8000/tasks', {
            'name': 'abcc',
            'type_id': 1,
            'status': 'open',
            'assigned_id': 1,
            'user_id': 1,
            'address': '30 cowper strreet',
            'time': 'sdf',
            'comments': 'dfsdf',
        }).then((response) => {
            if (!response.data) {
                //redirect to 404
                console.log("emmmmmm");
            }
        });
    }

    render() {
        return (
            <React.Fragment>
                <main className="task--create">
                    <Banner />
                    <Task type={this.props.match.params.type} />
                    <div className="background" >
                    <Hexagon size={"lg"} classes={"position-2"} />
                    <Hexagon size={"lg"} classes={"position-3"} />
                    <Hexagon size={"xs"} classes={"position-6 Animation--up-down"} />
                    </div>
                    {/*<Recommendations />*/}
                </main>
                <Footer />
            </React.Fragment>
        );
    }
}
export default CreateEvent;
