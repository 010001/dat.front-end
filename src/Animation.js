let preScrollPosition = window.pageYOffset;


//whenever scroll occurs, fire the function
window.onscroll = function(){
    controlNavBar();
    let currentScrollPosition = window.pageYOffset;
    // animation event
    const animation=["fade","zoom","slide"];
    //find out all the animation element by className, which is a collection
    const animation_collections=animation.map(ani=>document.getElementsByClassName(ani));
    this.Array.from(animation_collections).forEach((ani_coll,i)=>{
        this.Array.from(ani_coll).forEach((ani_ele)=>{
        //the position of page bottom
          let currentBottomPosition = currentScrollPosition + window.innerHeight;
          // let currentElementPosition = getOffsetTop(ani_ele)+ani_ele.offsetHeight;
          let currentElementPosition = getOffsetTop(ani_ele)+ani_ele.offsetHeight/2;
          if(currentBottomPosition>currentElementPosition-50){
            //add the animation className to fire the animation
              ani_ele.classList.add(`${animation[i]}-in`);
              ani_ele.classList.remove(`${animation[i]}`);
          }
      })  
    })

    // example : fade-in animation
    // const fade = document.getElementsByClassName("fade");
    // this.Array.from(fade).forEach((ele, i)=>{
    //     let currentBottomPosition = currentScrollPosition + window.innerHeight;
    //     let currentElementPosition = getOffsetTop(ele)+ele.offsetHeight;
    //     // console.log(currentElementPosition);
    //     this.console.log(i);
    //     if(currentBottomPosition>currentElementPosition-50){
    //         ele.classList.add('fade-in');       
    //         }
    // });

}


const getOffsetTop = element => {
    let offsetTop = 0;
    while(element) {
      offsetTop += element.offsetTop;
      element = element.offsetParent;
    }
    return offsetTop;
  }

//Nav Bar
const controlNavBar =()=>{
    let currentScrollPosition = window.pageYOffset;
    if (preScrollPosition > currentScrollPosition ){
        //if page up, then down the nav bar
        downNavBar();
    } else {
        //if page down, then up the nav bar 
        upNavBar();
    }
    preScrollPosition = currentScrollPosition;

}

//if scroll up
const upNavBar = () => {
    let header = document.getElementsByTagName("header")[0];
    if (header){
    header.classList.add('nav-up');
    header.classList.remove('nav-down');
    // header.setAttribute("value", `${window.pageYOffset}`);
    }
};

// if scroll down
const downNavBar = () => {
    let header = document.getElementsByTagName("header")[0];
    if (header){
    header.classList.add('nav-down');
    header.classList.remove('nav-up');
    // header.setAttribute("value", `${window.pageYOffset}`);
    }

};

    //當網頁尾端碰到元素頂端，則動畫跳出
    // if (currentBottomPosition > currentElementPosition){
    //     Array.from(fade).forEach((element, i) => {
    //         //因為forEach setTimeout是設定觸發時間，所以要讓每個人加上不同觸發時間
    //         setTimeout(()=>{
    //             element.classList.add("fade-in");
    //             },i*600);
    //     });
    //     // console.log(currentScrollPosition+window.innerHeight, getOffsetTop(fade[0])+fade[0].offsetHeight);
    //      }