import React from 'react';

class Tab extends React.Component {

    render() {
        return (
            <span
                className={this.props.isActive ? 'tab half active' : 'tab half'}
                onClick={this.props.onActiveTab}
                data-ui-test="tab-incoming">
            {this.props.content}
        </span>);
    }
}

export default Tab;
