import React from 'react';
import Tab from "./Tab/Tab";

class Tabs extends React.Component {
    constructor(props) {
        super(props);
        this.state = {selectedTabId: 1};

    };

    isActive = (id) => {
        return this.state.selectedTabId === id;
    };

    setActiveTab = (selectedTabId,callback) => {
        this.setState({selectedTabId});
        callback();
    };

    render() {
        const tabs = this.props.data.map((el, i) => <Tab key={i}
                                                         content={el.name}
                                                         isActive={this.isActive(el.id)}
                                                         onActiveTab={() => {this.setActiveTab(el.id,el.clickCallback)}}
            />
        );

        return (
            <div className="tabs">
                {tabs}
            </div>
        );
    }
}


export default Tabs;
