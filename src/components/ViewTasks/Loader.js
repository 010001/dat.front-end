import React from 'react';
import './Loader.css';
export default function Loader() {
  return (
    <div className="tasks-loader center">
    <div className="tasks-spinner"></div>
    </div>
  );
}

