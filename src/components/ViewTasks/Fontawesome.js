import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { far } from '@fortawesome/free-regular-svg-icons'
library.add(fas, far)


export const search = <FontAwesomeIcon icon={['fas', 'search']} size="1x" color="grey" />;
export const position = <FontAwesomeIcon icon={['fas', 'map-marker-alt']} size="1x" color="grey" />;
export const calander = <FontAwesomeIcon icon={['far', 'calendar-alt']} size="1x" color="grey" />;
export const sortdown = <FontAwesomeIcon icon={['fas', 'sort-down']} size="1x" color="#008fb4" />;
export const pen = <FontAwesomeIcon icon={['fas', 'pen']} size="1x" color="white"/>;
export const plus = <FontAwesomeIcon icon={['fas', 'plus-circle']} size="2x" color="grey"/>;
