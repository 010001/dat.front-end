import React from "react";
import OpenTaskCard from "./OpenTaskCard/OpenTaskCard";
import axios from 'axios';
import Loader from "../../ViewTasks/Loader"
import "./OpenTasks.css"
import {getEvents} from "../../../api/event";

export class OpenTasks extends React.Component {

    constructor() {
        super();
        this.state = {
            isLoading: true,
            data: [],
            error: false,
            newTasksnum: 0,
            newTaskcontent: [" new tasks"," new task"]
        };
    }

    async componentDidMount() {
        let res = await getEvents();
        let data = res;
        this.setState({
            isLoading: false,
            data,
        })
        let TasknumNow=data.length;
        let Tasknumbefore = localStorage.getItem('realTasknum');
        localStorage.setItem("realTasknum", data.length);
        let newTasksnum=this.state.newTasksnum;
        if(TasknumNow<Tasknumbefore){
            newTasksnum=0
        }
        else if(TasknumNow>Tasknumbefore){
            newTasksnum=TasknumNow-Tasknumbefore;
        }
        else{
            newTasksnum=0
        }
        this.setState({
            newTasksnum:newTasksnum
        })
        if(newTasksnum===0){
            this.setState({
                newTaskcontent:[" No new task"],
                newTasksnum:''
            })
        }
        else if(newTasksnum===1){
            this.setState({
                newTaskcontent:[" new task"," new tasks"]
            })
        }
    }
    componentDidUpdate(prevProps) {
        if (this.props.filteredData !== prevProps.filteredData) {
          this.setState({
            data:this.props.filteredData
          })
          }
        if (this.props.priceFilteredData !== prevProps.priceFilteredData) {
        this.setState({
             data:this.props.priceFilteredData
          })
          }
        }

    render() {
        const cards = this.state.data.map((item, i) => <OpenTaskCard key={i} data={item}/>);
        return (
            <aside className="take-list  new-task-list show-more-tasks ">
                <div className="new-list-task-frame">
                    <div className="new-list-task-title">
                        <span className="new-list-task_count">{this.state.newTasksnum}</span>
                        <span className="new-list-task_message">{this.state.newTaskcontent[0]}</span>
                    </div>
                </div>
                <ul className="vertical-tasks-list">
                    <li>{cards}</li>
                </ul>
                <div className="view-tasks-loading">
+                    {this.state.isLoading ? (<Loader/>):("") }
+                </div>
            </aside>
        );
    }
}

export default OpenTasks;
