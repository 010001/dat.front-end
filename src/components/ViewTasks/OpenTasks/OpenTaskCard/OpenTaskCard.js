import React from "react";
import "./OpenTaskCard.css"
import moment from 'moment';
import {Link} from "react-router-dom";
import {
    position, calander
} from '../../Fontawesome';

class OpenTaskCard extends React.Component {

    maxLengthCut(str,maxlength){
        if(str.length>maxlength){
            return str = str.substr(0,maxlength-2)+'...'
        }
        else return str
    }

    render() {
        let {location, time, status, offers, price, name, slug} = this.props.data;
        const taskStatus = {'complete': 'new-task-list-item--complete', 'assign': 'new-task-list-item--assign'};
        const statusColor = {'complete': 'color--blue', 'assign': 'color--purple'};
        location = this.maxLengthCut(location,35);
        return (
            <Link to={`/event/${slug}`}>
                <div className={"new-task-list-item new-task-list-item--open " + taskStatus[status]}>
                    <div className="new-task-list-item_header">
                        <span className="new-task-list-item_title">{name} </span>
                        <div className="new-task-list-item_price">
                            <span>${price}</span>
                        </div>
                    </div>
                    <div className="new-task-list-item_body flex">
                        <div>
                            <div className="new-task-list-item__location at-icon-map-marker2">
                                {position}
                                <span className="new-task-list-item__detail">{location} </span>
                            </div>
                            <div className="new-task-list-item__date at-icon-calendar">
                                {calander}
                                <span
                                    className="new-task-list-item__detail">{moment(new Date(time)).format("ddd, D MMM")}</span>
                            </div>
                        </div>
                        <div className="User-avator">
                            {/*<img*/}
                                {/*src={this.props.data.user_id.images ? this.props.data.user_id.images : "https://via.placeholder.com/256"}*/}
                                {/*alt="usericon"></img>*/}
                        </div>

                    </div>
                    {/*<div className="new-task-list-item__footer">*/}
                        {/*<span className={"new-task-list-item__status " + statusColor[status]}>{status}</span>*/}
                        {/*<span className="new-task-list-item__bids">{offers.length} offer</span>*/}
                    {/*</div>*/}
                </div>
            </Link>
        );
    }
}

export default OpenTaskCard;
