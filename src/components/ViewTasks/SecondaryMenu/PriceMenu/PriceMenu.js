import React from "react";
import {Range} from 'rc-slider';
import "rc-slider/assets/index.css";
import "./PriceMenu.css";

const PriceMenu = (props) => {
    return (
        <div className="menu-flyout menu-flyout--price" id="priceMenu">
            <div className="menu-flyout__inner menu-flyout__inner--padded">
                <div>
                    <div className="menu-flyout__title">Task Price</div>
                    <div className="menu-flyout__slider-label">
                            {props.price[0] <= props.price[1] ? <div>${props.price[0]} - ${props.price[1]}</div>: <div>${props.price[1]} - ${props.price[0]}</div>}
                    </div>
                    <div className="slider slider--enabled">
                        <Range  defaultValue={props.price} value={props.price} count={1} allowCross={true} onChange={props.onChange} max={300} min={5} step={5}/>
                    </div>
                </div>
            </div>
            <div className="menu-flyout__confirm menu-flyout__confirm--price">
                <button className="menu-flyout__cancel" onClick={() => props.elementNotDisplay()}>Cancel</button>
                <button className="menu-flyout__apply button-sml button-cta" onClick={() => { props.elementNotDisplay(); props.priceFilter()}} >Apply</button>
            </div>
        </div>

    );
}

export default PriceMenu;