import React from "react";
import "./SecondaryMenu.css"
import DistanceMenu from "./DistanceMenu/DistanceMenu";
import PriceMenu from "./PriceMenu/PriceMenu";
import TypeMenu from "./TypeMenu/TypeMenu";
import {search, sortdown} from '../Fontawesome.js';


export class SecondaryMenu extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            menuItems: ["distance_menu", "priceMenu", "typeMenu"],
            distance: 0,
            price: [0, 10000],
        }
    }

    handleClickFirst = (props) => {
    }

    distanceChange = (value) => {
        this.setState({
            distance: value,
        })
    };

    // search = (e) => {
    //     let search = e.target.value;

    // };

    componentDidMount(){
        const searchInput = document.getElementsByClassName("search-input_input")[0];
        const searchInputBtn = document.getElementsByClassName("search-input_button")[0];
        //add event listener on input, if enter is pressed, clicked the button
        searchInput.addEventListener('keypress',(e)=>{
            if(e.keyCode===13){
                e.preventDefault();
                searchInputBtn.click();
            }
        })
    }

    render() {
        const {searchTasks} = this.props;
        return (
            <div className="secondary-menu">
                <div className="secondary-menu-inner">
                    <ul className="task-menu">
                        <li className="task-menu-sub">
                            <span className="task-choice" onClick={() => this.props.elementDisplay(0) } data-test="distance">Position</span>
                            {sortdown}
                            {this.props.distanceMenuFlag && <DistanceMenu onChange={this.distanceChange} elementNotDisplay = {this.props.elementNotDisplay} distance={this.state.distance}/>}
                        </li>
                        <li className="task-menu-sub">
                            <span className="task-choice" onClick={() => this.props.elementDisplay(1)} data-test="price">Any price</span>
                            {sortdown}
                            {this.props.priceMenuFlag && <PriceMenu price={this.props.price} elementNotDisplay = {this.props.elementNotDisplay} onChange={this.props.priceChange} priceFilter={this.props.priceFilter}/>}
                        </li>
                        <li className="task-menu-sub">
                            <span className="task-choice" onClick={() => this.props.elementDisplay(2)} data-test="type">Task type</span>
                            {sortdown}
                            {this.props.typeMenuFlag && <TypeMenu elementNotDisplay = {this.props.elementNotDisplay}/>}
                        </li>
                    </ul>
                    <div className="search-input">
                        <input className="search-input_input" id ="search-input" type="text" placeholder="Search for a task"  />
                        <button className="search-input_button" onClick={searchTasks}>
                            {search}
                        </button>
                    </div>
                    <div className="search-button" onChange={this.search}></div>
                </div>
            </div>
        );
    }
}


export default SecondaryMenu;

