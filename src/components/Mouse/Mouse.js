import React from 'react';
import "./Mouse.css";

class Mouse extends React.Component {

    render() {
        return (
            <div className="mouse">
                <div className="mouse-icon">
                    <span className="mouse-wheel"/>
                </div>
            </div>
        );
    }
}

export default Mouse;
