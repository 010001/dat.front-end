import React from 'react';

import './Info.css';

class Info extends React.Component {
    render() {
        return (
            <div className="Info flex">
                <img alt="" src="https://via.placeholder.com/55x45"/>
                <div>
                    <h5>You're the boss</h5>
                    <p>With thousands of tasks posted every month on Shieldtasker
                        there are lots of opportunities to earn. Choose the tasks you’d like to complete for people that
                        you're happy to work with.</p>
                </div>
            </div>
        );
    }
}

export default Info;
