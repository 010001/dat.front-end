import React from 'react';
import {connect} from "react-redux";
import './Questions.css';
class Questions extends React.Component {
    render() {
        return (
            <section>
                <div className="Question__Container">
                    {/* From API <Question/>*/}
                    <h2>Question(0)</h2>
                    <p>Please don't share personal info – insurance won't apply to tasks not done through Shieldtasker!</p>

                    <div className="comments__form flex flex__row">
                        <div className="avatar-img">
                            <img id="user--icon" src={this.props.userImage? this.props.userImage :"https://via.placeholder.com/256"} alt=""></img>
                        </div>
                        <div className="text-area">
                            <textarea placeholder="Add some info about your task" data-hj-whitelist="" maxLength="1500" data-ui-test="add-comment-textarea" spellCheck="false"></textarea>
                            <div className="text__comm-footer">
                                <form method="post" encType="multipart/form-data">
                                    {/* <input type="file" data-ui-test="upload-attachment-input" name="" className="comment--attachment" /> */}
                                    <button className="comment--btn">Send</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        userImage: state.images,
    }
};

export default connect(mapStateToProps, null)(Questions);
