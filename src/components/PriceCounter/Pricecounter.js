
let buffer = [0, 0]

function FindOldPrice(current) {
    setTimeout(() => {
        buffer = current
    }, 1)
    return buffer
}

const currentPrice = (iTemsNum) => {
    let CuMiniPrice;
    let CuMaxPrice;
    const [ibed, ibath, icheck] = iTemsNum;
    if (!icheck) {
        CuMiniPrice = 50 + (ibath - 1) * 15 + (ibed - 1) * 15;
        CuMaxPrice = 60 + (ibath - 1) * 10 + (ibed - 1) * 10;
        return [CuMiniPrice, CuMaxPrice]
    } else {
        CuMiniPrice = 65 + (ibath - 1) * 10 + (ibed - 1) * 15
        CuMaxPrice = 70 + (ibath - 1) * 10 + (ibed - 1) * 10
        return [CuMiniPrice, CuMaxPrice]
    }
}

export const PriceCounter = (iTemsNum) => {
    const current = currentPrice(iTemsNum);
    const old = FindOldPrice(current)
    return ({ miniPrice: current[0], maxPrice: current[1], oldPriceMin: old[0], oldPriceMax: old[1] })
}
