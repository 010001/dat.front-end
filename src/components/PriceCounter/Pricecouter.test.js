import { PriceCounter } from './Pricecounter.js'


test('testing if jest.fn() can work properly', () => {
  let mockFn = jest.fn((num1, num2) => {
    return num1 * num2;
  })
  expect(mockFn(10, 10)).toBe(100);
})

test('Whole function can be called without exploding', () => {

  // We won't care what the internal function is, what we only want to know is whether this module can be called or not..
  expect.assertions(1);
  let mockFn = jest.fn();
  PriceCounter(mockFn());
  expect(mockFn).toBeCalled();

});

// 这个function的 Unit testing 应该差不多了。。剩下的就是别的component的Unit teting 了。。。。









