import React from 'react';
import './RecommendationList.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStar, faAirFreshener } from '@fortawesome/free-solid-svg-icons'
class RecommentionList extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            starNos: 5,
            starArry: [],
            socialNo: 4,
            socialArry: []
        }
    }

    componentDidMount() {
        const { starNos } = this.state;
        const { starArry } = this.state;
        const { socialNo } = this.state;
        const { socialArry } = this.state;

        for (var i = 0; i < starNos; i++) {

            this.state.starArry.push(i)
        }

        for (var j = 0; j < socialNo; j++) {

            this.state.socialArry.push(j)
        }
        this.setState({
            starArry: starArry,
            socialArry: socialArry
        })
    }

    render() {
        return (
            <div className="recommendationList">
                <div className="recommendationList__profile">
                    <div className="recommendationList__profile__content">
                        <div className="recommendationList__profile__content--photo">
                        </div>
                        <div className="recommendationList__profile__content__info">
                            <p className="recommendationList__profile__content__info-name">Calum M</p>
                            <p className="recommendationList__profile__content__info-address">Leichhardt NSW</p>
                            <div className="recommendationList__profile__content__info--ratings">
                                {this.state.starArry.map((element, index) =>
                                    <FontAwesomeIcon key={index} style={{ color: "rgb(243,189,69)", fontSize: `13px`, float: "left" }} icon={faStar} />
                                )
                                }
                                <span> ({this.state.starArry.length})</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="recommendationList__contentBox">
                    <p className="recommendationList__contentBox__latestReview--title">LATEST REVIEW</p>
                    <p className="recommendationList__contentBox__latestReview--content">"Calum and his partner communicated well and quickly prior to the job. They were punctual (10mins early) and did a very thorough job. Would highly reco..."</p>
                    <p className="recommendationList__contentBox__badges--title">VERIFIED BADGES</p>
                    <div className="recommendationList__contentBox__SocialIcon">

                        {this.state.starArry.map((element, index) =>
                            <FontAwesomeIcon key={index} style={{ color: "rgb(243,189,69)", width: `34px`, height: `34px`, float: "left" }} icon={faAirFreshener} />
                        )
                        }
                    </div>
                    <button className="recommendationList__contentBox--requestButton">Request a  Quote</button>
                </div>
            </div>
        );
    }
}

export default RecommentionList;

