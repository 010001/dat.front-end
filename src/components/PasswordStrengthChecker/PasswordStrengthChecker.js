import React from 'react';
import zxcvbn from 'zxcvbn';
import './PasswordStrengthChecker.css'

class PasswordStrengthChecker extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            password: '',
            suggestion: [],
            score: 0,
        }
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.password.value !== nextProps.password.value) {
            this.props = nextProps;
            this.setState({
                password: this.props.password.value,
                suggestion: zxcvbn(this.props.password.value).feedback.suggestions,
                score: zxcvbn(this.props.password.value).score,
            })
        }
    }

    render() {
        const { suggestion } = this.state;
        return (
            <div className={this.state.score>=3?  "checkerbox--container--small" : 'checkerbox--container'}>
                <p className={this.state.score===0? '':'componentUnVisible'}><span className="font--bold">Password Strength:</span> <span className="font--bold font--red">Very Weak</span></p>
                <p className={this.state.score===1? '':'componentUnVisible'}><span className="font--bold">Password Strength:</span> <span className="font--bold font--lightRed">Weak</span></p>
                <p className={this.state.score===2? '':'componentUnVisible'}><span className="font--bold">Password Strength:</span> <span className="font--bold font--skyBlue">Normal</span></p>
                <p className={this.state.score===3? '':'componentUnVisible'}><span className="font--bold">Password Strength:</span> <span className="font--bold font--lightGreen">Healthy</span></p>
                <p className={this.state.score===4? '':'componentUnVisible'}><span className="font--bold">Password Strength:</span> <span className="font--bold font--green">Very Healthy</span></p>
                <div className={this.state.score===0? "alert-0" : 'componentUnVisible'}></div>
                <div className={this.state.score===1? "alert-1" : 'componentUnVisible'}></div>
                <div className={this.state.score===2? "alert-2" : 'componentUnVisible'}></div>
                <div className={this.state.score===3? "alert-3" : 'componentUnVisible'}></div>
                <div className={this.state.score===4? "alert-4" : 'componentUnVisible'}></div>
                <br></br>
                {suggestion.map((item, index) => <li className="font--white" key={index}>{item}</li>)}


            </div>
        )
    }
}
export default PasswordStrengthChecker;