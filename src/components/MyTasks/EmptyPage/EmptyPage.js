import React from "react";
import "./EmptyPage.css"
class EmptyPage extends React.Component{
        constructor(props){
                super(props);
                this.state = {

                }
        }

        render(){
               return (
                        <div className="empty-page-content">
                               <div className="empty-page-root">
                                       <img src="https://via.placeholder.com/412x452"  className="empty-root-img"alt="img"/>
                                       <p className="empty-page-text">
                                               Looks like you haven’t posted a task or made any offers just yet. How about posting one now?
                                        </p>
                                        <button className="empty-page-button" onClick={this.props.openPostModal}>Post a task</button>
                               </div>
                        </div>
               ) ;
        }
}

export default EmptyPage;
