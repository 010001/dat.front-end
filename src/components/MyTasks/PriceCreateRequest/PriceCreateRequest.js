import React from "react";
import "./PriceCreateRequest.css"
class PriceCreateRequest extends React.Component{
        constructor(props){
                super(props);
                this.state = {
                        radioClassName:["input-option-wrapper", "input-option-wrapper"],
                        currentRadio: 0,
                }
        }
        componentWillReceiveProps(nextProps){
                if (this.props.price !== nextProps.price
                        || this.props.budgetTime !== nextProps.budgetTime || this.props.totalBudget !== nextProps.totalBudget){
                         this.props = nextProps;
                        }
        }
        handleRadio = index => {
                let {radioClassName} = this.state;
                radioClassName[index] = "input-option-active ";
                radioClassName[1-index]= "input-option-wrapper";
                this.setState({radioClassName, currentRadio:index});
        }
        render(){
                return (
                        <div className="container-create-request">
                                 <h3 className="create-request-header">Suggest how much</h3>
                                 <div className="slide_screens__ProgressBar-e4qnpy-2 gGVmpI"></div>
                                 <ul className="create-request-modal">
                                         <label className="create-request-label">What is you budget?<span>Want help?</span></label>
                                        <li className="create-request-form-field">
                                                <p className="create-price-form-text">
                                                        Please enter the price you're comfortable to pay to get your task done. Taskers will use this as a guide for how much to offer.
                                                </p>
                                                <ul className="create-budget-tags">
                                                        <li className="create-budget-tag">
                                                                <input type="radio" id="option-in-total" className={this.state.radioClassName[0]}
                                                                onClick={()=>{this.handleRadio(0)}} />
                                                                <div className="create-budget-tag-p">Total</div>
                                                        </li>
                                                        <li className="create-budget-tag">
                                                                <input type="radio" id="option-in-hour" className={this.state.radioClassName[1]}
                                                                onClick={()=>{this.handleRadio(1)}} />
                                                                <div className="create-budget-tag-p">Hourly Rate</div>
                                                        </li>
                                                </ul>
                                        </li>
                                        <li className="create-request-form-field">
                                                {this.state.currentRadio === 0 ? 
                                                        <div className="budget-wrapper">
                                                                <div className="budget-input-icon">$</div>
                                                                <input type="number" className="budget-total" value={this.props.totalBudget.value}
                                                                name="totalBudget"  onChange={(e) => {this.props.onChange(e)}}/>
                                                        </div>:
                                                        <div className="total-time-budget-wrapper">
                                                                 <div className="budget-wrapper">
                                                                        <div className="budget-input-icon">$</div>
                                                                        <input type="number" className="budget-total" name="price"
                                                                        value={this.props.price.value} onChange={(e) => {this.props.onChange(e)}}/>
                                                                        <div className="budget-screen-hour-sign">/hr</div>
                                                                </div>
                                                                <div className="budget-screen-hourly-wrapper">
                                                                        <svg className="budget_screen__StyledCloseSimple-sc-3tdiag-4 chwtAu" width="24" height="24" viewBox="0 0 24 24">
                                                                                <path d="M13.17 12l6.41-6.42a.82.82 0 0 0-1.16-1.16L12 10.83 5.58 4.42a.82.82 0 0 0-1.16 1.16L10.83 12l-6.41 6.42a.8.8 0 0 0 0 1.16.8.8 0 0 0 1.16 0L12 13.17l6.42 6.41a.8.8 0 0 0 1.16 0 .8.8 0 0 0 0-1.16z"></path>
                                                                        </svg>
                                                                        <div className="budget-screen-hours">
                                                                                <input className="budget-hour-text-input" type="number"  name="budgetTime"
                                                                                value={this.props.budgetTime.value} onChange={(e) => {this.props.onChange(e)}}/>
                                                                                <div className="budget-screen-hour-sign">hrs</div>
                                                                        </div>
                                                                </div>
                                                        </div>}
                                        </li>
                                        <li className="create-request-form-field">
                                                <div className="banner-wrapper">
                                                        <div className="banner-subtitle-box">
                                                                <p className="estimated-budget-banner">ESTIMATED BUDGET</p>
                                                                <p className="estimated-budget-banner-sub">Final payment will be agreed later</p>
                                                        </div>
                                                        <div className="banner-budget-total">
                                                              <div className="banner-budget-currency">$</div>{this.state.currentRadio === 0 ?
                                                              this.props.totalBudget.value: (this.props.budgetTime.value * this.props.price.value)}
                                                        </div>
                                                </div>
                                        </li>
                                        <li className="create-request-form-button-field">
                                                <button onClick={this.props.openSecondModal} className="button">Back</button>
                                                <button  className="button button--green" onClick={() => {
                                                        this.props.handleSubmit()
                                                        this.props.closeModal()}}>Submit</button>
                                        </li>
                                 </ul>
                        </div>
                )
        }
}

export default PriceCreateRequest;