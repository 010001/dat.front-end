import React from "react";
import "./SecondaryMenu.css"

class SecondaryMenu extends React.Component {
        constructor(props) {
                super(props);
                this.state = {
                        tagsOpen:false,
                        showTagsName:"tags-none",
                }
        }
        onClickTags = (e) => {
                this.setState({ tagsOpen:!this.state.tagsOpen});
        }
        render() {
                const {titleList} = this.props;
                return (
                        <div className="secondary-menu">
                                <div className="secondary-menu-inner">
                                        <ul className="task-menu">
                                                {titleList.map((items, index) => 
                                                        <li className="task-menu-sub" key={index} onClick={()=>{
                                                               if (!this.state.tagsOpen) this.props.changeTypes(index);
                                                        }}>
                                                                <span className="task-choice">{items}</span>
                                                                {index === 0 ? <button className="show-tasks-button show" onClick={this.onClickTags}>
                                                                               {!this.state.tagsOpen? <svg width="24" className="pill__icon pill__icon--active" height="24" viewBox="0 0 24 24"><path d="M12.39 14.012a.5.5 0 0 1-.78 0l-2.96-3.7a.5.5 0 0 1 .39-.812h5.92a.5.5 0 0 1 .39.812z"></path></svg>
                                                                                :<svg width="24" className="pill__icon" height="24" viewBox="0 0 24 24"><path d="M12.39 14.012a.5.5 0 0 1-.78 0l-2.96-3.7a.5.5 0 0 1 .39-.812h5.92a.5.5 0 0 1 .39.812z"></path></svg>}
                                                                                        {this.state.tagsOpen &&
                                                                                         <div className="menu-flyout">
                                                                                                 <ul className="menu-flyout__inner">
                                                                                                         {titleList.map((itemsN, indexN) => 
                                                                                                                <li className="menu-bar__item menu-bar__item--active menu-bar__item--dropdown" key={indexN} onClick={()=>{
                                                                                                                        this.props.changeTypes(indexN);}}>
                                                                                                                        <span className="task-choice">{itemsN}</span>
                                                                                                                </li>)}
                                                                                                </ul>
                                                                                        </div>}
                                                                        </button>:null        
                                                                }
                                                        </li>
                                                )}
                                        </ul>
                                </div>
                        </div>
                );
        }
}


export default SecondaryMenu;

