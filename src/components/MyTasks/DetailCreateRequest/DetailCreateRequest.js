import React from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import "./DetailCreateRequest.css";
import {connect} from "react-redux";
import * as action from "../../../store/actions";
class DetailCreateRequest extends React.Component{
        constructor(props){
                super(props);
                this.state = {
                        radioClassName:["input-option-wrapper", "input-option-wrapper"],
                        currentRadio: 0,
                }
        }
        handleRadio = index => {
                let {radioClassName} = this.state;
                radioClassName[index] = "input-option-active ";
                radioClassName[1-index]= "input-option-wrapper";
                if (index){
                        let {address} = this.props;
                        address.value = "Online Discussion";
                }
                this.setState({radioClassName,currentRadio: index});
        }
        render(){
                const {suggestions,onClear, onSpecialChange} = this.props;
                return (
                        <div className="container-create-request">
                                <h3 className="create-request-header">Say where and when</h3>
                                <div className="slide_screens__ProgressBar-e4qnpy-2 gGVmpI"></div>
                                <ul className="create-request-modal">
                                        <li className="create-request-form-field">
                                                <label className="create-request-label">What do you need it done?</label>
                                                <ul className="create-request-box">
                                                        <li className="create-request-box-sub">
                                                                <label className="create-request-option">
                                                                        <input type="radio" id="option-in-person" className={this.state.radioClassName[0]}
                                                                               onClick={()=>{this.handleRadio(0)}} />
                                                                        <p className="create-request-text-person">In person</p>
                                                                </label>
                                                                <p className="create-request-option-content">
                                                                        Select this if you need the Tasker physically there.
                                                                </p>
                                                                <div>
                                                                        <svg fill="#008fb4" width="24" height="24" viewBox="0 0 24 24">
                                                                        <path d="M12 2.25a7.29 7.29 0 0 0-7.25 7.3C4.75 12.49 8.4 18 10.57 21a1.77 1.77 0 0 0 2.86 0c2.17-3 5.82-8.53 5.82-11.47A7.29 7.29 0 0 0 12 2.25zm.21 17.89q-.21.28-.42 0c-3.47-4.82-5.54-8.78-5.54-10.59a5.75 5.75 0 1 1 11.5 0c0 1.81-2.07 5.77-5.54 10.59z"></path><path d="M12 7.13a2.75 2.75 0 1 0 2.75 2.75A2.75 2.75 0 0 0 12 7.13zm0 4a1.25 1.25 0 1 1 1.25-1.25A1.25 1.25 0 0 1 12 11.13z"></path>
                                                                        </svg>
                                                                </div>     
                                                        </li> 
                                                        <li className="create-request-box-sub">
                                                                <label className="create-request-option">
                                                                        <input type="radio" id="option-online" className={this.state.radioClassName[1]}
                                                                               onClick={()=>{this.handleRadio(1)}}/>
                                                                        <p className="create-request-text-person">Online</p>
                                                                </label>
                                                                <p className="create-request-option-content">
                                                                        Select this if the Tasker can do it from home
                                                                </p>
                                                                <div>
                                                                        <svg fill="#008fb4" width="24" height="24" viewBox="0 0 24 24">
                                                                                <path d="M7.25 4c0-.686.564-1.25 1.25-1.25h7c.686 0 1.25.564 1.25 1.25v16c0 .686-.564 1.25-1.25 1.25h-7c-.686 0-1.25-.564-1.25-1.25zM8.5 1.25A2.756 2.756 0 0 0 5.75 4v16a2.756 2.756 0 0 0 2.75 2.75h7A2.756 2.756 0 0 0 18.25 20V4a2.756 2.756 0 0 0-2.75-2.75zm4.6 16.15a1.2 1.2 0 1 1-2.4 0 1.2 1.2 0 0 1 2.4 0z"></path>
                                                                        </svg>
                                                                </div>
                                                        </li>
                                                </ul>
                                        </li>
                                        <li className="create-request-form-field">
                                               {this.state.currentRadio === 0 && <input autoComplete="off" spellCheck="false" className="create-request-form-location"
                                                        value={this.props.address.value} name="address" onChange={(e) => {
                                                                this.props.onChangeText(e);
                                                                onSpecialChange(this.props.address.value);
                                                        }}/>} 
                                                <div className="suggestions">
                                                        {suggestions.map((items, index) =>
                                                        <div key={index} onClick = {() => {
                                                                this.props.onChangeAddress(items.suburb+","+items.state);
                                                                onClear();
                                                        }}>{items.suburb+","+items.state}</div>
                                                )}
                                </div>
                                        </li>
                                        <li className="create-request-form-field">
                                                <label htmlFor="" className="create-request-label">When do you need it done?</label>
                                                <div className="create-request-calendar-wrapper">
                                                        <div className="create-request-calendar-icon">
                                                                <svg id="calendar" width="20" height="20" viewBox="0 0 24 24">
                                                                        <path d="M20 3.59h-3.25v-.93a.75.75 0 1 0-1.5 0v.93h-6.5v-.93a.75.75 0 1 0-1.5 0v.93H4a2.75 2.75 0 0 0-2.75 2.75v13A2.75 2.75 0 0 0 4 22.09h16a2.75 2.75 0 0 0 2.75-2.75v-13A2.75 2.75 0 0 0 20 3.59zM4 5.09h3.25v1.07a.75.75 0 0 0 1.5 0V5.09h6.5v1.07a.75.75 0 0 0 1.5 0V5.09H20a1.25 1.25 0 0 1 1.25 1.25v2.25H2.75V6.34A1.25 1.25 0 0 1 4 5.09zm17.25 14.25A1.25 1.25 0 0 1 20 20.59H4a1.25 1.25 0 0 1-1.25-1.25v-9.25h18.5z"></path><path d="M8 16.84a2.5 2.5 0 1 0-2.5-2.5 2.5 2.5 0 0 0 2.5 2.5zm0-3.5a1 1 0 1 1-1 1 1 1 0 0 1 1-1z"></path>
                                                                </svg>
                                                        </div>
                                                        <div className="create-request-time-input">
                                                                <DatePicker selected={this.props.dateTime} 
                                                                        onChange={this.props.onChange}/>
                                                        </div>
                                                </div>
                                        </li>
                                        <li className="create-request-form-button-field">
                                                <button onClick={this.props.closeModal} className="button">Cancel</button>
                                                <button onClick={()=>{this.props.openThirdModal()}} className="button button--green">Submit</button>
                                        </li>
                                </ul>
                        </div>
                )
        }
}
const mapStateToProps = (state) => {
        return {
                suggestions: state.address.suggestions
        }
}
const mapDispatchToProps = dispatch => ({
        
        onSpecialChange(value) {
                if (value){
                        dispatch(
                                action.searchSuburbNames(value)
                        )
                } else {
                        dispatch(
                                action.clearSuggestions()
                        )
                }
        },
        onClear(){
                dispatch(
                        action.clearSuggestions()
                )
        },
        openThirdModal(){
                dispatch(
                        action.openThirdModal(),
                )
        }
})
export default connect(mapStateToProps,mapDispatchToProps)(DetailCreateRequest);