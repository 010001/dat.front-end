import React from "react";
import OpenTaskCard from "../../../ViewTasks/OpenTasks/OpenTaskCard/OpenTaskCard";
import "./OpenTask.css"

class OpenTasks extends React.Component {

    constructor() {
        super();
        this.state = {
            isLoading: true,
            data: [],
            error: false,
        };
    }
    async componentDidMount() {
        this.setState({
            isLoading: false,
            // data:this.props.tasks,
        })
    }

    render() {
        const cards = this.props.tasks.map((item, i) => <OpenTaskCard key={i} data={item}/>);

        return (
            <aside className="take-list  new-task-list show-more-tasks ">
                <ul className="vertical-tasks-list">
                    <li>{cards}</li>
                </ul>
            </aside>
        );
    }
}

export default OpenTasks;
