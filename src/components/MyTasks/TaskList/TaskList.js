import React from "react";
import axios from "axios";
import OpenTask from "./OpenTask/OpenTask"
class TaskLists extends React.Component{
        async loadMapBox() {
                var mapboxgl = require('mapbox-gl/dist/mapbox-gl.js');
                mapboxgl.accessToken = 'pk.eyJ1Ijoia2l0bWFuMjAwMjIwMDIiLCJhIjoiY2sxd3BjOG54MDQ3ajNucWw0NzBqajRyciJ9.oCz-m_TQXx68DtTXL07nSA';
                let map = new mapboxgl.Map({
                    container: 'map',
                    style: 'mapbox://styles/mapbox/streets-v11',
                    center: [-122.420679, 37.772537],
                    zoom: 15,
                });
                await this.getLocation();
                const center = this.state.data;
                this.addMarker(center, mapboxgl, map);
                map.setCenter(center);
            }
        
            getLocation = async () => {
                const url = 'https://api.mapbox.com/geocoding/v5/mapbox.places/10%20cowper%20street%20randwick%20sydney.json?access_token=pk.eyJ1Ijoia2l0bWFuMjAwMjIwMDIiLCJhIjoiY2sxd3BjOG54MDQ3ajNucWw0NzBqajRyciJ9.oCz-m_TQXx68DtTXL07nSA&limit=1';
                const response = await axios.get(url);
                let res = response.data.features[0].center;
                this.setState({data: res});
            };
        
            addMarker(center, mapboxgl, map) {
                var geojson = {
                    type: 'FeatureCollection',
                    features: [
                        {
                            type: 'Feature',
                            geometry: {
                                type: 'Point',
                                coordinates: center
                            },
                            properties: {
                                title: 'Mapbox',
                                description: 'Washington, D.C.'
                            }
                        }
                    ]
                };
                geojson.features.forEach(function (marker) {
        
                    // create a HTML element for each feature
                    var el = document.createElement('div');
                    el.className = 'marker';
        
                    // make a marker for each feature and add to the map
                    new mapboxgl.Marker(el)
                        .setLngLat(marker.geometry.coordinates)
                        .addTo(map);
                });
            }
            async componentDidMount() {
                await this.loadMapBox();
            }
            componentWillReceiveProps(nextProps){
                if (this.props !== nextProps){
                    this.props = nextProps
                }
            }
            render() {
                return (
                        <div id="page-and-screen-content">
                            <main>
                                <div className="view-tasks">
                                    <section className="open-tasks__allTasks">
                                            <OpenTask tasks={this.props.tasks}/>
                                    </section>
                                    <section className="click-view-page" id="map">
                                    </section>
                                </div>
                            </main>
                        </div>
                );
        }
}
export default TaskLists

