import React from 'react';
import './HowItWorks.css';
class   HowItWorks extends React.Component {
    render() {
        return (
            <div className="flex flex--vertical-center how-it-works">
                <img alt={""} src={this.props.img}/>
                <div>
                    <h3>{this.props.title}</h3>
                    <p>{this.props.content}</p>
                </div>
            </div>
        );
    }
}

export default HowItWorks;
