import React from 'react';
import axios from "axios/index";
import Service from "../Service/Service";

class Services extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [],
        }
    }

    componentDidMount() {
        //API View Event

        //this does not need to called twice it will be called in the parent

        axios.get('https://shield-backend.herokuapp.com/api/v1/services').then((response) => {
            if (!response.data) {
                //redirect to 404
                console.log("emmmmmm");
            }
            this.setState({ data: response.data });
            // console.log(this.state.data);
        });
    }

    render() {
        const services = this.state.data.map((item, i) => {
            return <Service name={item.name} slug={item.slug} key={i} />
        });
        return (
            <React.Fragment>
                {services}
            </React.Fragment>
        );
    }
}

export default Services;
