import React from 'react'
import axios from 'axios';
import StripeCheckout from 'react-stripe-checkout';
import icon from '../../img/icon2.png';
import config from '../../config/config';

const CURRENCY = 'AUD';

const convertToStripePayment = amount => parseInt(parseInt(amount) + "00");

const successPayment = (data, payer_id, payee_id, event_id) => {
    axios.post(config.stripe.backend_api + "/api/v1/attendees", {
        user_id: payer_id,
        event_id: event_id
    }).then(() => {
        data();
    }).catch(() => {
        console.log("error");
    });
};

const errorPayment = (data) => {
    data();
};

const onToken = (amount, description, success, failed, payer_id, payee_id, task_id) => token =>
    axios.post(config.stripe.backend_api + "/api/v1/payments",
        {
            description,
            source: token.id,
            currency: CURRENCY,
            amount: amount,
            payer_id: payer_id,
            payee_id: payee_id,
            task_id: task_id,
        })
        .then(() => {
            successPayment(success, payer_id, payee_id, task_id)
        })
        .catch(() => {
            errorPayment(failed)
        });


const Checkout = ({name, description, amount, success, failed, payerUserID, payeeUserID, taskID}) =>
    <StripeCheckout
        name={name}
        description={description}
        amount={convertToStripePayment(amount)}
        token={onToken(amount, description, success, failed, payerUserID,payeeUserID, taskID)}
        currency={CURRENCY}
        stripeKey={config.stripe.key}
        label="Pay with 💳"
        image={icon}
        zipCode
        email
        allowRememberMe
    />;

export default Checkout;
