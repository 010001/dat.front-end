import React from "react";
import "./CardTask.css"
import {Link} from "react-router-dom";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faMapMarker} from "@fortawesome/free-solid-svg-icons";

class CardTask extends React.Component {
    render() {
        const {offerPrice, name, slug, user_id} = this.props.data;
        return (
            <Link className="task__card task-card-animation" to={`/event/${slug}`}>
                <img alt={""}
                     src="https://via.placeholder.com/246x164"/>

                <div className="task__card--main-container">
                    <span>March 3rd, 2020 11:00 AM</span>
                    <h5 className="life_moments_task__TaskTitle-sc-153tdrc-2 dCMZFf">{name}</h5>
                    <hr className={"divider"}/>
                    <div className={"flex"}>
                        <FontAwesomeIcon className="location" icon={faMapMarker}/>
                        <p>Event Location</p>
                    </div>
                </div>
            </Link>
        )
    }
}

export default CardTask;

