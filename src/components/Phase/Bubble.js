import React from 'react';

class Bubble extends React.Component{
    render(){
        return(
            <div>
                <span className="bubble"></span>
                <span className="bubble"></span>
                <span className="bubble"></span>
                <span className="bubble"></span>
                <span className="bubble"></span>
            </div>
        )
    }
}
export default Bubble;
