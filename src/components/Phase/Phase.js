import React from 'react';
import Bubble from './Bubble';
import './Phase.css';

class Phase extends React.Component {
    render() {
        const phase = this.props.number==="1"? <Bubble imgDev={this.props.imgDev}/> : '';
        return (
            <div className="phase">
                <img alt="decreation" src={this.props.img}/>
                {phase}
                <div className="text-box center">
                    <h4>{this.props.title}</h4>
                    <p>{this.props.text}</p>
                </div>
            </div>
        );
    }
}

export default Phase;
