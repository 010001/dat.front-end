import React from 'react';
import './Hexagon.css';

class Hexagon extends React.Component {
    render() {
        const st = this.props.styles;
        return (
            <div className={"absolute " + this.props.classes}  style={st}>
                <div className={"Hexagon Hexagon--" + this.props.size}></div>
            </div>
        );
    }
}

export default Hexagon;
