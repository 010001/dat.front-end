// 其实这个页面需要测试的东西有： 

import React from 'react';
import { shallow, mount } from 'enzyme';
import Price from './Price';
import { BrowserRouter as Router } from 'react-router-dom';
import * as PriceModule from '../Pricecounter/Pricecounter'
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });


describe(`component: Price`, () => {

    class MockData {
        constructor() {

            this.modalPage = 0;
            this.minProps = {
                clickStatusForBed: [],
                clickStatusForBath: [],
                checkBoxStatus: [],
                otherInfo: [],
                callBackForFinderPage: jest.fn().mockReturnValue('modal'),
                modalPage: 1,
                callBackPrice: jest.fn().mockReturnValue('100')
            }
        }
    }

    const mockData = new MockData();
    const wrapper = shallow(
        <Price {...mockData.minProps} />
    );
    const instance = wrapper.instance()

    it(`should render without exploding`, () => {

        expect(wrapper.length).toEqual(1);
    })


    test(`should open the modal page when click`, () => {

        // need to use mount to fully render the component rather than just shallow: 
        instance.handlePage = jest.fn()
        wrapper.find(`.dropDownBt`).simulate('click');
        expect(instance.handlePage).toHaveBeenCalled();

    })

    test(`should calculate the price while trigger Setprice fn`, () => {

        const spy = jest.spyOn(PriceModule, 'PriceCounter');
        spy.mockReturnValue(`$100`)
        instance.setPriceState();
        expect(PriceModule.PriceCounter()).toBe(`$100`);
    })

    test(`trigger the life-cycle funtion with windows`, () => {

        global.document.addEventListener = jest.fn();
        const component = mount(
            <Router>
                <Price {...mockData.minProps} />
            </Router>);
        expect(global.document.addEventListener).toHaveBeenCalled(); // In order to test componentDidmount 是否执行了这个eventListener。。。
        // 也就是说当mount后，addEventListener 确实执行了，在componentDidMount之后。。。

    })

    // test(`should trigger scroll event with correct scroll position`, () => {

    //     // const map = {
    //     //     keyup: null
    //     // };
    //     // global.document.addEventListener = jest.fn((event, cb) => {
    //     //     map[event] = cb;
    //     // });

    //     // const wrapper = shallow(
    //     //     <Price {...mockData.minProps} />
    //     // );
    //     // map.keyup({ which: 27 }); 
    //     /////////////////////////Can not be solved by myself///////////////////
    // })
})