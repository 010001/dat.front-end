import React from 'react';
import './Price.css';
// import FindCleaner from "../Modal/FindCleaner/FindCleaner";
import { NavLink } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import CountUp from 'react-countup';
import { PriceCounter } from '../PriceCounter/Pricecounter';

class Price extends React.Component {
    constructor(props) {
        super(props)
        this.minBuffer = 0;
        this.maxBuffer = 0;
        this.miniPrice = 50;
        this.maxPrice = 60;
        this.state = {
            clickStatusForBed: [],
            clickStatusForBath: [],
            checkBoxStatus: [],
            miniBuffer: 0,
            maxBuffer: 0,
            modalPage: ``,
            dropDown: ``
        }
    }

    handlePage = () => {
        this.setState((state) => { return { modalPage: `flex` } },
            () => {
                this.props.callBackForFinderPage(this.state.modalPage)
            }
        )
    }

    serviceNum = (...targetServices) => {
        const newArray = [];
        for (let targetService of targetServices) {
            const serviceNum = targetService.findIndex(Items => Items.clickStatus === true || Items.clickStatus === "block") + 1;
            newArray.push(serviceNum)
        }
        return newArray
    } // This function canbe re-usable....If we need other service .......etc...

    setPriceState = () => {
        const { clickStatusForBed, clickStatusForBath, checkBoxStatus } = this.props;
        const iTemsNum = this.serviceNum(clickStatusForBed, clickStatusForBath, checkBoxStatus)
        const { miniPrice, maxPrice, oldPriceMin, oldPriceMax } = PriceCounter(iTemsNum)
        this.minBuffer = oldPriceMin;
        this.maxBuffer = oldPriceMax;
        this.miniPrice = miniPrice;
        this.maxPrice = maxPrice;
    }

    handleOnScroll = (e) => {
        const dropDownContent = this.props.onScrollEvent(e)
        if (dropDownContent) {
            this.setState({
                dropDown: dropDownContent.dropDown,
                hidden: dropDownContent.hidden,
                dropDownBtn: dropDownContent.dropDownBtn
            })
        }
    }

    static getDerivedStateFromProps(nextProps) {
        return {
            clickStatusForBed: nextProps.clickStatusForBed,
            clickStatusForBath: nextProps.clickStatusForBath,
            checkBoxStatus: nextProps.checkBoxStatus,
        }
    }

    componentDidUpdate() {
        this.props.callBackPrice(this.miniPrice, this.maxPrice)
    }

    componentDidMount() {
        window.addEventListener('scroll', this.handleOnScroll);
    }

    componentWillUnmount = () => {
        window.removeEventListener('scroll', this.handleOnScroll);
    };


    render() {
        const { modalPage, dropDown, hidden, dropDownBtn } = this.state;
        this.setPriceState() //Unit testing for this.

        return (
            <div className={`priceCard ${dropDown} zoom`}>
                <div className="priceCard__Estimate">
                    <h3>ESTIMATED PRICE*</h3>
                    <div className="PriceSection">
                        <div className="priceCard__Estimate--minimum">
                            {
                                !!(this.maxPrice > this.miniPrice) && <span> $<CountUp start={this.minBuffer} end={this.miniPrice} duration={3} /></span>
                            }
                            {
                                !!(this.maxPrice <= this.miniPrice) && <span> $<CountUp start={this.maxBuffer} end={this.maxPrice} duration={3} /></span>
                            }
                        </div>
                        <span>-</span>
                        <div className="priceCard__Estimate--maximum">
                            {
                                !!(this.maxPrice > this.miniPrice) && <span> $<CountUp start={this.maxBuffer} end={this.maxPrice} duration={3} /></span>
                            }
                            {
                                !!(this.maxPrice <= this.miniPrice) && <span> $<CountUp start={this.minBuffer} end={this.miniPrice} duration={3} /></span>
                            }
                        </div>
                    </div>
                </div>

                <ul className="priceCard__Estimate--li">
                    <li className="li">
                        <FontAwesomeIcon icon={faCheck} />
                        <p>Get instant quotes in minutes</p></li>
                    <li className="li">
                        <FontAwesomeIcon icon={faCheck} />
                        <p>1000 &acute;s of reviewed cleaners </p>
                    </li>

                    <li className="li">
                        <FontAwesomeIcon icon={faCheck} />
                        <p>Its free to try - give it a go!</p>
                    </li>
                </ul>

                <button className="dropDownBt" onClick={() => { this.handlePage(modalPage) }} id={`${dropDownBtn}`}>Find a cleaner</button>
                <p id={`${hidden}`}>*Based on average cleaning task prices, marketplace prices may vary. T&C &acute; s apply.</p>

                <div id="hidden-link" className="price__card">
                    <NavLink to='/event' className="button--red button-xs">发布活动</NavLink>
                </div>


            </div>
        );
    }
}




export default Price;

