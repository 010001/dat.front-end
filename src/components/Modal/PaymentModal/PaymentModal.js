import React from 'react';
import './PaymentModal.css';

import {connect} from "react-redux";
import * as action from "../../../store/actions";
import Checkout from "../../Checkout/Checkout";

export class PaymentModal extends React.Component {

    render() {
        console.log(this.props.payerUserID);
        return (
            <React.Fragment>
                <h1>付款</h1>
                <p>Are you sure you want to it for free?</p>
                <div className={"flex flex--space-between"}>
                    <Checkout description={"Hire skilled people & earn extra money today on ShieldTasker.com"}
                              amount={this.props.price} name={"Shield"} success={this.props.successPayment}
                              failed={this.props.failPayment} payerUserID={this.props.payerUserID}
                              payeeUserID={this.props.payeeUserID} taskID={this.props.taskID}/>
                    <button className={"button-md"} onClick={this.props.modalClose}>Cancel</button>
                </div>
            </React.Fragment>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        closeModal: () => dispatch(action.closeModal())
    };
};

const mapStateToProps = (state) => {
    return {
        isModalOpen: state.modal.isModalOpen,
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(PaymentModal);

