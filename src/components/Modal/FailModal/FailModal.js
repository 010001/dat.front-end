import React from 'react';
import './FailModal.css';

export class FailModal extends React.Component {
    render() {
        return (
            <React.Fragment>
                <h1>{this.props.title || "Error"}</h1>
                <p>{this.props.content || "Retry"}</p>
                <button className={"button-md button-xs--spacing"} onClick={this.props.click} data-test="fail">Ok</button>
            </React.Fragment>
        );
    }
}

export default FailModal;

