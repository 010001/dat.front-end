import React from 'react';
import './ConfirmModal.css';

export class ConfirmModal extends React.Component {
    render() {
        return (
            <div className={"completed--modal"} data-test={"confirm"}>
                <h1 className="pop--animation--title">Cancel Task</h1>
                <div className="bid_success__StyledTickWrapper-sc-8wnihp-1 dkgvt">
                    <p color="#292b32" className="text--large pop--animation--p">Are you sure ?</p>
                    <div className={"flex"}>
                    <button onClick={this.props.confirmClick} className="button button-xs--spacing pop--animation--btn  button--red">Confirm</button>
                    <button onClick={this.props.closeClick} className="button button-xs--spacing pop--animation--btn">Close</button>
                    </div>
                </div>
            </div>
        );
    }
}


export default ConfirmModal;

