import React from 'react';
import './AcceptModal.css';
import Modal from "../../UI/Modal/Modal";
import {connect} from "react-redux";
import * as action from "../../../store/actions";


class AcceptModal extends React.Component {

    render() {
        return (
            <Modal show={true} modalClosed={this.props.modalClose} class={"modal--accept"}>
                <h1>Cancel Offer</h1>
                <p>Are you sure you want to cancel your offer?</p>
                <div className={"flex"}>
                <button className={"button-md button-xs--spacing button--red"} onClick={this.props.accept} value={this.props.value}>Confirm</button>
                <button className={"button-md button-xs--spacing"} onClick={this.props.modalClose}>Cancel</button>
                </div>
            </Modal>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        closeModal: () => dispatch(action.closeModal())
    };
};

const mapStateToProps = (state) => {
    return {
        isModalOpen: state.modal.isModalOpen,
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AcceptModal);

