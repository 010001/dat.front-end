import React from 'react';
import './FindCleaner.css';
import { connect } from "react-redux";
import axios from "axios/index";
import ViewTasks from '../../../pages/Tasks/ViewTasks/ViewTasks'
import { Redirect } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import LocationSearch from '../../LocationSearch/LocationSearch';
import Loader from '../../ViewTasks/Loader';

class FindCleaner extends React.Component {
    constructor(props) {
        super(props)
        this.pageCounter = 0;
        this.clickTimes = 0;
        this.state = {
            button: [`forward`, `backward`, `exit-first`, `exit-secondary`, `continue`, `discard`],
            page: `none`,
            title: ``,
            details: ``,
            address: ``,
            date: ``,
            responseStatus: 0,
            errorBlock: `none`,
            checkBoxes: [`Bring cleanning Supply`, `Bring a vacuum,mop etc`, `Have a verified ID`],
            checkedStatus: [`none`, `none`, `none`],
            anything: ``,
            ifExit: `none`
        }
    }

    buttonJudge = (whichButton) => {
        if (whichButton === `forward`) {
            this.clickTimes++;
            if (this.pageCounter === -1) {
                this.pageCounter = 0;
                this.pageCounter++
                return this.pageCounter
            } else {
                this.pageCounter++
                return this.pageCounter
            }

        } if (whichButton === `backward`) {
            this.pageCounter = this.clickTimes - (this.clickTimes - this.pageCounter) - 1;
            this.setState({
                responseStatus: 0
            })
            return this.pageCounter

        } if (whichButton === `exit-first`) {
            if (this.pageCounter === -1) {
                this.pageCounter = 0;
                this.setState({
                    ifExit: `block`
                })
            } else {
                this.setState({
                    ifExit: `block`
                })
            }

        } if (whichButton === `exit-secondary` || whichButton === `continue`) {
            this.setState({
                ifExit: `none`
            })
        } if (whichButton === `discard`) {
            this.pageCounter = -1
            this.setState({
                ifExit: `none`
            })
        }
    }

    pageJudge = (pageCounter) => {

        if (this.pageCounter === -1) {
            this.setState({
                page: `none`,
                ifExit: `none`,
                responseStatus: 0,
                title: ``,
                details: ``,
                address: ``,
                date: ``,
                errorBlock: `none`,

            })

        } if (pageCounter === 1) {

            if (this.state.page === `common` || this.state.page === `when`) {
                this.setState({
                    page: `where`,
                    ifExit: `none`
                })
            }

        } if (pageCounter >= 2) {

            this.pageCounter = 1
            if (this.state.address) {
                this.pageCounter = 2;
                this.setState({
                    page: `when`,
                    errorBlock: `none`
                })
            } else {
                this.setState({
                    errorBlock: `block`
                })
            }

        } if (pageCounter >= 3) {
            this.pageCounter = 2
            if (this.state.date) {
                this.pageCounter = 3
                this.setState({
                    page: `taskerOptions`,
                    errorBlock: `none`
                })
            } else {
                this.setState({
                    errorBlock: `block`
                })
            }

        } if (/^[4-9]/.test(pageCounter)) {

            this.pageCounter = 3;

            if (this.ifChecked === `checked`) {
                this.pageCounter = 4;
                this.setState({
                    page: `photos`
                })
            } else {
                this.setState({
                    errorBlock: `block`
                })
            }

        } if (/^[5-9]/.test(pageCounter)) {
            this.pageCounter = 5;
            this.setState({
                page: `anything`
            })

        } if (/^[6-9]/.test(pageCounter)) {
            this.pageCounter = 5
            if (this.state.anything) {
                this.pageCounter = 6
                this.setState({
                    page: `details`
                })
            } else {
                this.setState({
                    errorBlock: `block`
                })
            }

        } if (pageCounter >= 7) {
            this.pageCounter = 7
            if (this.state.details) {
                this.postTask()
            } else {
                this.setState({
                    errorBlock: `block`
                })
            }
        }
    }

    handleFormat = (title) => {
        const reg = /\s/g;
        const FormattedTitle = title.replace(reg, '-');
        return FormattedTitle
    }

    postTask = () => {

        const { title, details, address, date } = this.state
        const { miniPrice, maxPrice } = this.props
        this.setState({
            responseStatus: `loading`
        })
        const FormattedTitle = this.handleFormat(title)

        axios.post('https://shield-backend.herokuapp.com/api/v1/tasks',
            {
                "name": title,
                "type_id": 1,
                "status": "post",
                "user_id": this.props.userId,
                "address": address,
                "time": date,
                "comments": details,
                "offerPrice": (miniPrice + maxPrice) / 2,
                "slug": FormattedTitle
            }
        )
            .then((response) => {

                this.setState({
                    responseStatus: response.status
                })
            })
            .catch((error) => {
                console.log(error);

                this.setState({

                    responseStatus: 422
                })
            });
    }

    handleInputChange = (e) => {

        const { value, id } = e.target
        if (id === `input-title`) {
            this.setState({
                title: value,
                errorBlock: `none`
            })

        } else if (id === `input-details`) {
            this.setState({
                details: value,
                errorBlock: `none`
            })

        } else if (id === `input-when`) {
            this.setState({
                date: value,
                errorBlock: `none`
            })
        } else if (id === `input-anything`) {
            this.setState({
                anything: value,
                errorBlock: `none`
            })
        }
    }


    handlePageCHange = (whichButton) => {

        this.buttonJudge(whichButton)
        this.pageJudge(this.pageCounter, whichButton)
    }

    selectAdressCallBack = (address) => {

        this.setState({
            address: address,
            errorBlock: `none`
        })
    }

    callBackForAddress = (address) => {
        this.setState({
            address: address,
            errorBlock: `none`
        })
    }

    onCheckOptions = (e) => {

        const { checkedStatus } = this.state;

        if (/none none none/.test(...checkedStatus) === false) {
            this.ifChecked = `checked`
            if (/checkBox-0/.test(e.target.className)) {
                this.setState({
                    checkedStatus: [`checked`, `none`, `none`]
                })
            } if (/checkBox-1/.test(e.target.className)) {
                this.setState({
                    checkedStatus: [`none`, `checked`, `none`]
                })
            } else if (/checkBox-2/.test(e.target.className)) {
                this.setState({
                    checkedStatus: [`none`, `none`, `checked`]
                })
            }
        } else {
            this.ifChecked = `none`
        }
    }

    static getDerivedStateFromProps(nextProps) {
        if (nextProps.modalPage === `flex`) {
            return {
                page: `common`
            }
        } else {
            return null
        }
    }

    componentDidUpdate() {
        this.props.callBack(this.state.page)
    }

    render() {

        const { page, button, title, details, date, responseStatus, errorBlock, checkedStatus, anything, ifExit } = this.state;

        if (responseStatus === 0 || responseStatus === `loading`) {
            return (
                <div style={{ display: page === `none` ? 'none' : 'block' }} className={`modal__Wrapper modal__Wrapper--display`}>
                    <div id={`modal__Content-${ifExit}`} className="modal__Content modal__Content--display relative">
                        <div className={`modal__exit modal__exit-${ifExit} absolute`}>
                            <div className="modal__exit__header center">
                                <p >Sorry to see you go...</p>
                                <svg onClick={() => { this.handlePageCHange(button[3]) }} style={{ width: `30`, height: `30` }} className="exitButton-secondary right">
                                    <path id="cros" d="M13.17 12l6.41-6.42a.82.82 0 0 0-1.16-1.16L12 10.83 5.58 4.42a.82.82 0 0 0-1.16 1.16L10.83 12l-6.41 6.42a.8.8 0 0 0 0 1.16.8.8 0 0 0 1.16 0L12 13.17l6.42 6.41a.8.8 0 0 0 1.16 0 .8.8 0 0 0 0-1.16z"></path>
                                </svg>
                            </div>

                            <div className="modal__exit__body center">
                                <span>Are you sure? You're almost done and it's free to post a task...</span>
                            </div>

                            <div className="modal__exit__footer center">
                                <button onClick={() => { this.handlePageCHange(button[4]) }} id="continueTask">Continue Task</button>
                                <button onClick={() => { this.handlePageCHange(button[5]) }} id="exit"> Discard & Exit</button>
                            </div>
                        </div>

                        <div className="screens__Wrapper screens__Wrapper--responseStatus" >
                            <div className="view-tasks-loading">
                                {responseStatus === `loading` ? (<Loader />) : ("")}
                            </div>
                            <div className={`screens__ProgressBar-page-${page}`}></div>
                            <div className="screens__buttonSection">
                                <button style={{ display: this.pageCounter >= 2 ? 'block' : 'none' }} id="screens__buttonSection__backwardButton" onClick={() => { this.handlePageCHange(button[1]) }} > {`<`} </button>
                                <button id="screens__buttonSection__exitButton" onClick={() => { this.handlePageCHange(button[2]) }}>
                                    <svg id="cros">
                                        <path id="cros" d="M13.17 12l6.41-6.42a.82.82 0 0 0-1.16-1.16L12 10.83 5.58 4.42a.82.82 0 0 0-1.16 1.16L10.83 12l-6.41 6.42a.8.8 0 0 0 0 1.16.8.8 0 0 0 1.16 0L12 13.17l6.42 6.41a.8.8 0 0 0 1.16 0 .8.8 0 0 0 0-1.16z"></path>
                                    </svg>
                                </button>
                            </div>
                            <div className="screens__content">
                                <div className="screens__content__body">
                                    <div className="screens__content__body__wrapper">

                                        <div className="markDown__Wraped">
                                            <div style={{ display: page === `common` ? `flex` : `none` }} id="page-common" className="markdown__Content">
                                                <img src="https://via.placeholder.com/290x214" alt="dsad" title="On boarding" />
                                                <span>Start getting offers in no time</span>
                                                <p>"We've got a few questions to find the right Tasker to help you move - it'll only take a few minutes."</p>
                                            </div>

                                            <div style={{ display: page === `where` ? `flex` : `none` }} id="page-where" className="markdown__Content">

                                                <span>Where do you need the cleaning?</span>
                                                <LocationSearch errorBlock={this.state.errorBlock} callBackForAddress={this.callBackForAddress} />
                                            </div>
                                            <div style={{ display: page === `when` ? `flex` : `none` }} id="page-when" className="markdown__Content">
                                                <section>
                                                    <span>When do you need it done?</span>
                                                    <input id="input-when" type="date" className={`input-hover input-when-${errorBlock}`} value={date} onChange={this.handleInputChange}></input>
                                                </section>
                                                <p style={{ display: errorBlock === `none` ? `none` : `block` }} id="errorBlock">Enter Date</p>
                                                <section className="checkBox-list">

                                                    <div className="checkBox">
                                                        <FontAwesomeIcon className="checked-checked" icon={faCheck} />
                                                    </div>
                                                    <p id="needDay">I need a certain day</p>
                                                </section>
                                            </div>

                                            <div style={{ display: page === `taskerOptions` ? `flex` : `none` }} id="page-taskerOptions" className="markdown__Content">
                                                <span>Any of these the Tasker must have or do?</span>
                                                <ul>
                                                    {
                                                        this.state.checkBoxes.map((element, index) =>
                                                            <li key={index} className="checkBox-list">
                                                                <div onClick={this.onCheckOptions} className={`checkBox checkBox-${index}`}>
                                                                    <FontAwesomeIcon className={`checked-${checkedStatus[index]}`} icon={faCheck} />
                                                                </div>
                                                                <p>{element}</p>
                                                            </li>)
                                                    }
                                                </ul>
                                            </div>

                                            <div style={{ display: page === `photos` ? `flex` : `none` }} id="page-photos" className="markdown__Content">
                                                <span>Do you have any photos to show waht you need cleaned ?</span>
                                                <h3>You can upload JPG,JPEG,PNG,TXT or PDF files (5MB per image)</h3>
                                                <input id="input-photos" type="file" placeholder="drag & drop files here"  ></input>
                                            </div>

                                            <div style={{ display: page === `anything` ? `flex` : `none` }} id="page-anything" className="markdown__Content">
                                                <span>Is there anything else the Tasker should know?</span>
                                                <textarea id="input-anything" className={`input-hover input-anything-${errorBlock}`} placeholder="E.g.I've got a dog etc" value={anything} onChange={this.handleInputChange}></textarea>
                                                <p style={{ display: errorBlock === `none` ? `none` : `block` }} id="errorBlock">Can not be Blank</p>
                                            </div>

                                            <div style={{ display: page === `details` ? `flex` : `none` }} id="page-details" className="markdown__Content">
                                                <span>Tell us what you need done?</span>
                                                <span>This will be the title of your task - e.g. Help move my sofa</span>
                                                <input id="input-title" type="text" className={`input-hover input-title-${errorBlock}`} value={title} onChange={this.handleInputChange} ></input>

                                                <span>What are the details?</span>
                                                <span>Be as specific as you can about what needs doing</span>
                                                <textarea id="input-details" value={details} className={`input-hover input-details-${errorBlock}`} onChange={this.handleInputChange} ></textarea>
                                                <p style={{ display: errorBlock === `none` ? `none` : `block` }} id="errorBlock">Can not be Blank</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="screens__content__footer">
                                    <div className="footer__button__Wrapper">
                                        <button className="footer__button__Wrapper__button button--style" onClick={() => { this.handlePageCHange(button[0]) }}> Next</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div >
            );
        } if (responseStatus === 201) {

            return (
                <Redirect to='/view-tasks' component={ViewTasks}></Redirect>
            )
        } if (responseStatus === 422) {

            return (
                <div style={{ display: page === `none` ? 'none' : 'block' }} className="modal__Wrapper modal__Wrapper--display">
                    <div style={{ opacity: `1` }} className="modal__Content modal__Content--display">
                        <div className="screens__Wrapper screens__Wrapper--display">
                            <div className={`screens__ProgressBar-page-${page}`}></div>
                            <div className="screens__buttonSection">
                                <button style={{ display: this.pageCounter >= 2 ? 'block' : 'none' }} id="screens__buttonSection__backwardButton" onClick={() => { this.handlePageCHange(button[1]) }} > {`<`} </button>
                                <button id="screens__buttonSection__exitButton" onClick={() => { this.handlePageCHange(button[2]) }}>
                                    <svg style={{ color: `white` }}>
                                        <path d="M13.17 12l6.41-6.42a.82.82 0 0 0-1.16-1.16L12 10.83 5.58 4.42a.82.82 0 0 0-1.16 1.16L10.83 12l-6.41 6.42a.8.8 0 0 0 0 1.16.8.8 0 0 0 1.16 0L12 13.17l6.42 6.41a.8.8 0 0 0 1.16 0 .8.8 0 0 0 0-1.16z"></path>
                                    </svg>
                                </button>
                            </div>
                            <div className="screens__content">
                                <div className="screens__content__body">
                                    <div className="screens__content__body__wrapper">

                                        <div className="markDown__Wraped">
                                            <h1> Error Code ： 422 </h1>
                                            <h1>Error Please create again！！</h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div >
            )
        }
    }
}

const mapStateToProps = (state) => {
    return {
        userId: state.auth.userId
    }
};


export default connect(mapStateToProps)(FindCleaner);




