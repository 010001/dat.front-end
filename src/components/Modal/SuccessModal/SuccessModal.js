import React from 'react';
import './SuccessModal.css';

export class SuccessModal extends React.Component {
    render() {
        return (
            <div className={"completed--modal"} data-test={"success"}>
                <h1 className="pop--animation--title">你已经成功参加本次活动</h1>
                <div className="bid_success__StyledTickWrapper-sc-8wnihp-1 dkgvt">
                    <div className={"text-center"}>
                        <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                            <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none"/>
                            <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8"/>
                        </svg>
                    </div>
                    <p color="#292b32"
                       className="text--large pop--animation--p">如果您有任何疑问或疑虑请联系kitman20022002(Wechat)<br/>或发送电子邮件至support@theinvulnerable.com.au
                    </p>
                    <button onClick={this.props.click} className="button button-xs--spacing pop--animation--btn">关闭
                    </button>
                </div>
            </div>
        );
    }
}


export default SuccessModal;

