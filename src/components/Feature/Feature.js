import React from 'react';


import './Feature.css';

class Feature extends React.Component {
    render() {
        return (
            <div className={this.props.text === "center" ? "text-center feature": "feature"}>
                <img alt="" src="https://via.placeholder.com/55x45"/>
                <h4 >Top rated insurance</h4>
                <p>Shieldtasker Insurance is provided by CGU. This means Taskers on Shieldtasker are covered for liability to
                    third parties when it comes to personal injury or property damage (terms and conditions apply) - so
                    you can post or earn with peace of mind!*</p>
            </div>
        );
    }
}

export default Feature;
