import React from "react";
import './TaskFormCss.css';
import CleanTimes from "../Button/CleanTimes/CleanTimes";
import Bedroom from "../Button/Bedroom/Bedroom";
import Bathroom from "../Button/Bathroom/Bathroom";
import OtherService from "../Button/OtherService/OtherService";
import ComButton from '../Button/ComButton'

class TaskForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            cleanTimes: {
                buttonType: `cleanTimes`,
                clickStatus: [`once-click-state`, `regular-basis`]
            },
            bedrooms: {
                buttonType: `bedrooms`,
                clickStatus: [`one-click-state`, `two`, `three`, `four`, `five`]
            },
            bathrooms: {
                buttonType: `bathrooms`,
                clickStatus: [`one-click-state`, `two`, `three`]
            },
            otherServices: [`oven`, `cabinets`, `windows`, `carpets`],
            title: ``,
            details: ``,
            address: ``,
            date: ``,
        }
    }

    callBackForClickStatus = (cat, arry) => {
        this.setState((state) => {
            return state[cat].clickStatus = arry
        }, () => { this.props.callBack(cat, arry) })
    }

    render() {
        const { cleanTimes, bedrooms, bathrooms } = this.state;
        return (
            <div className="TaskForm TaskForm--page fade">


                <OtherService callBack={this.callBackForClickStatus} data={this.state.otherServices} />
            </div >
        )
    }
}

export default TaskForm;

