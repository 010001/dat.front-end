import React from 'react';
import './MenuExpand.css';
import {connect} from "react-redux";
import {NavLink} from "react-router-dom";

class Navigation extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            userName:"",
        };
        this.setWrapperRef = this.setWrapperRef.bind(this);
        this.handleClickOutside = this.handleClickOutside.bind(this);
    }
    async componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside);
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }

    setWrapperRef(node) {
        this.wrapperRef = node;
    }

    handleClickOutside(event) {
        if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
            this.props.click();
        }
    }

    render() {
        return (
            <div className="menu-folder expanded" ref={this.setWrapperRef}>
                <div className="menu-folder-items showing">
                    <NavLink className="double-line" to='/user' onClick={this.props.click}>
                        <div className="main-label">{localStorage.getItem("userName")}</div>
                        <div className="sub-label">个人资料</div>
                    </NavLink>
                    <NavLink onClick={this.props.click} to='/view-tasks'>浏览活动</NavLink>
                    <NavLink onClick={this.props.click} to='/my-tasks/'>我的活动</NavLink>
                    <a className="" href="/account/payment-history/">付款记录</a>
                    <NavLink onClick={this.props.click} to='/logout'>登出</NavLink>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        userId: state.auth.userId,
        token: state.auth.token,
        userName: state.auth.profileName,
        isAuth: state.auth.token !== null,
    }
};

export default connect(mapStateToProps, null)(Navigation);

