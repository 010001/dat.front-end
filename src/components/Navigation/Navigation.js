import React from 'react';
import './Navigation.css';
import {NavLink} from "react-router-dom";
import {connect} from "react-redux";
import logo from '../../img/dpng.svg';
import MenuExpand from "./MenuExpand/MenuExpand";

class Navigation extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            openNav: false
        };
        this.toggleMenu = this.toggleMenu.bind(this);
    }


    toggleMenu = (e) => {
        this.setState({'openNav': !this.state.openNav});
    };

    render() {
        const {userImage} = this.props;
        return (
            <div>
                <header>
                    <div className="header-container">
                        <NavLink to='/'><img alt={"logo"} className="img--logo" src={logo}/>
                        </NavLink>
                        <div className="menu-container">
                            <div className="menu">
                                <NavLink to='/create-event/clean' className="button-xs">发布活动</NavLink>
                                <nav>
                                    <ul>
                                        <li><NavLink to='/view-tasks' className="browser-task">浏览活动</NavLink>
                                        </li>
                                        {!!this.props.isAuth &&
                                        <li><NavLink to='/my-tasks' className="browser-task">我的活动</NavLink></li>}
                                        {/*{!this.props.isAuth &&*/}
                                        {/*<li><NavLink to='/how-it-works' className="browser-task">How it works</NavLink>*/}
                                        {/*</li>}*/}
                                    </ul>
                                </nav>
                            </div>
                            <div className="menu-user relative">
                                {!this.props.isAuth && <NavLink to='/sign-up'>注册</NavLink>}
                                {!this.props.isAuth && <NavLink to='/login'>登入</NavLink>}
                                {!!this.props.isAuth && ((userImage !== null && userImage.length > 0) ?
                                    <img className="nav-avatar-img"
                                         src={userImage}
                                         alt={"profile"} onClick={this.toggleMenu}/> :
                                    <img className="nav-avatar-img"
                                         src="https://via.placeholder.com/256"
                                         alt={"profile"} onClick={this.toggleMenu}/>)}
                                {!!this.state.openNav && <MenuExpand click={this.toggleMenu}/>}
                            </div>
                            <div className={"menu--mobile"}>
                                {!this.props.isAuth && <NavLink to='/sign-up'>Sign up</NavLink>}
                                {!this.props.isAuth && <NavLink to='/login'>Log in</NavLink>}
                            </div>
                        </div>
                    </div>
                </header>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        userImage: state.images,
        isAuth: state.auth.token !== null,
        token: state.auth.token,
    }
};

export default connect(mapStateToProps, null)(Navigation);
