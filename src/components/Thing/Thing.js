import React from 'react';
import './Thing.css';

function ThingDevice(props) {
    const aniName = props.aniName
    const aniItems = props.items
    const id = props.id

    return (
        <div className="thing flex space-around">
            <div className="home-thing-container">
                <div className="home-thing-bg">
                    <div id={`thing-${id}`} style={{ animationName: aniName[0] }} className={`home-thing-bg-${id}-wrapper`}>
                        {
                            aniItems.map((ele, index) => {
                                return index > aniName.length * 2 - aniItems.length - 1 ?
                                    <div key={index} style={{ animationName: aniName[aniName.length - 1] }}
                                        className={`home-thing-bg-${id}-${aniItems[index]}`}>
                                    </div> :
                                    <div key={index} style={{ animationName: aniName[index + 1] }}
                                        className={`home-thing-bg-${id}-${aniItems[index]}`}>
                                    </div>
                            })
                        }
                    </div>
                </div>
            </div>

            <div className="home-thing-content">
                <h4>{props.title}</h4>
                <p>{props.content}</p>
            </div>
        </div>
    );
}


export default ThingDevice;

