class ThingAniSheet {
    constructor() {
        this.animationDevice = {
            id: `device`,
            items: [`left`, `right`, `padLock`, `-light-1`, `-light-2`, `-light-3`],
            aniName: ["device"]
        }

        this.animationGroup = {
            id: `group`,
            items: [`buooi`, `wave`],
            aniName: ["device"]
        }
        this.animationBadges = {
            id: `badges`,
            items: [`id`, `coles`, `card`, `empty`, `facebook`, `twitter`, `police`, `removals`, `child`, `paint`, `electrician`, `ikea`],
            aniName: ["device"]
        }
    }

    judgeAniName = (aniName, positionSig) => {
        aniName = aniName || ["device"]
        if (positionSig === `device`) {
            return aniName = ["device-wrapper",
                "device-left",
                "device-right ",
                "device-padLock",
                "device-light"
            ]
        } if (positionSig === `group`) {
            return aniName = ["group-wrapper", "group-buooi", "group-wave"]
        } if (positionSig === `badges`) {
            return aniName = ["badges-wrapper",
                "badges-id",
                "badges-coles",
                "badges-card",
                "badges-empty",
                "badges-facebook",
                "badges-police",
            ]
        } if (positionSig === `nothing`) {
            return aniName = ["device"]
        }
    }
}

export const thingAniSheet = new ThingAniSheet()