import React from 'react';
import { shallow, mount } from 'enzyme';
import ThingDevice from './ThingDevice.js'

import 'styled-components'

describe(`component: ThingDevice`, () => {

    const minProps = {
        animationDevice: [],
        title: ``,
        content: `,`
    }

    it(`should render without exploding`, () => {
        const wrapper = shallow(<ThingDevice {...minProps} />)
        expect(wrapper.length).toEqual(1);
    })

    test('Nodes render with the correct props', () => {

        const nextProps = {
            animationDevice: ["device-wrapper",
                "device-left",
                "device-right",
                "device-padLock",
                "device-light "
            ]
        }
        const component = mount(<ThingDevice {...nextProps} />);
        expect(component.find(`.home-thing-bg-device-wrapper`).props().style.animationName).toEqual(nextProps.animationDevice[0])
    });
})







