import React from 'react';


import './HowItWorkBanner.css';
import Phase from "../Phase/Phase";
import ShieldVideo from "../ShieldVideo/ShieldVideo";
import * as action from "../../store/actions";
import {connect} from "react-redux";

class HowItWorkBanner extends React.Component {
    render() {
        return (
            <div className="how-it-works-banner">
                <div className="container--how-it-work">
                    <h2 className="center">
                        <span>Neque porro quisquam est qui dolorem ipsum</span><br/>
                        <span>quia dolor sit</span>
                    </h2>
                    <div className="how-it-works-video-btn">
                        <div className="image-wrapper">
                            <button className={"button--video"} onClick={() => {
                                this.props.openModal();
                            }}><img alt=""
                                    src="https://via.placeholder.com/290x210"/>
                            </button>
                        </div>
                        <p>Neque porro quisquam</p>
                    </div>
                    <div className="flex phases">
                        <Phase img={"https://via.placeholder.com/263x319"} number="1"
                               text={"Lorem ipsum dolor sit amet.\n" +
                               "                   Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\n" +
                               "                    Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat\n" +
                               "                   Duis aute irure."} title={" voluptate velit esse?"}/>
                        <Phase img={"https://via.placeholder.com/121x352"}
                               text={"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Ut enim ad minim veniam"}
                               title={"Lorem ipsum dolor sit amet, "}/>
                        <Phase img={"https://via.placeholder.com/358x354"}
                               text={" Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur"}
                               title={"Lorem ipsum "}/>
                    </div>
                    {!!this.props.isModalOpen && <ShieldVideo/>}
                </div>
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        openModal: () => dispatch(action.openModal()),
        closeModal: () => dispatch(action.closeModal())
    };
};

const mapStateToProps = (state) => {
    return {
        isModalOpen: state.modal.isModalOpen,
        userId: state.auth.userId
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(HowItWorkBanner);
