import React from 'react';
import './Service.css';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux'

class Service extends React.Component {

    render() {
        return (
            <div className="service--container text-center">
                <NavLink className="service--link" to={Boolean(this.props.userId) ? '/create-task/' + this.props.slug : '/sign-up'}>
                    <div className="service">
                        <svg width="24" height="24" viewBox="0 0 24 24">
                            <path
                                d="M15.18 10.9l-.38-.47a.76.76 0 0 0-.58-.28h-1.46v-2h.71a3.63 3.63 0 0 0 3.8-3.44 3.64 3.64 0 0 0-3.79-3.45H6.55A.76.76 0 0 0 5.8 2v1.66a1.87 1.87 0 0 0 1.93 1.79A1.37 1.37 0 0 1 9.2 6.67v.73a.75.75 0 0 0 .75.75h1.31v2H9.78a.76.76 0 0 0-.58.28l-.38.47a14.28 14.28 0 0 0-3.12 8.89 3 3 0 0 0 3 3h6.66a3 3 0 0 0 3-3 14.28 14.28 0 0 0-3.18-8.89zM7.73 4c-.25 0-.43-.15-.43-.29v-.96h6.18a2.15 2.15 0 0 1 2.29 2 2.14 2.14 0 0 1-2.3 1.94H10.7A2.86 2.86 0 0 0 7.73 4zm7.6 17.3H8.67a1.47 1.47 0 0 1-1.47-1.51 12.74 12.74 0 0 1 2.8-7.95l.15-.19h1.12v4a1.13 1.13 0 0 1-.33.79l-.75.75a.75.75 0 0 0 0 1.06.75.75 0 0 0 1.06 0l.75-.75a2.61 2.61 0 0 0 .77-1.85v-4h1.1l.15.19a12.74 12.74 0 0 1 2.79 8 1.47 1.47 0 0 1-1.48 1.41z"></path>
                        </svg>
                    </div>
                    {this.props.name}</NavLink>
            </div>
        );
    }
}


const mapStateToProps = (state) => {
    return {
        isModalOpen: state.modal.isModalOpen,
        userId: state.auth.userId
    }
};

export default connect(mapStateToProps)(Service);
