import React from 'react';
import {createEvent} from "../../api/event";
import * as action from "../../store/actions";
import connect from "react-redux/es/connect/connect";

class Task extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            name: {
                elementConfig: {
                    placeholder: 'kitmanwork@gmail.com'
                },
                validation: {
                    required: true,
                    isEmail: true
                },
                errorMessage: {
                    email: "Not valid Email",
                    required: 'Email is required',
                },
                valid: false,
                value: 'fdgdfgfdvcb',
                cssClass: '',
            },
            location: {
                elementConfig: {
                    placeholder: 'kitmanwork@gmail.com'
                },
                validation: {
                    required: true,
                    isEmail: true
                },
                errorMessage: {
                    email: "Not valid Email",
                    required: 'Email is required',
                },
                valid: false,
                value: 'sdfsdff',
                cssClass: '',
            },
            start_date: {
                elementConfig: {
                    placeholder: 'kitmanwork@gmail.com'
                },
                validation: {
                    required: true,
                },
                errorMessage: {
                    required: 'Password is required',
                },
                valid: false,
                value: 'sdfdsf',
                cssClass: '',
            },
            details: {
                elementConfig: {
                    placeholder: 'kitmanwork@gmail.com'
                },
                validation: {
                    required: true,
                },
                errorMessage: {
                    required: 'Password is required',
                },
                valid: false,
                value: 'sdfsdf',
                cssClass: '',
            },
            price: {
                elementConfig: {
                    placeholder: 'kitmanwork@gmail.com'
                },
                validation: {
                    required: true,
                },
                errorMessage: {
                    required: 'Password is required',
                },
                valid: false,
                value: '154',
                cssClass: '',
            },

            checkForget: false,
            checked: true,
            images: ""
        };

    }


    checkValidity(value, rules) {
        let isValid = true;
        if (!rules) {
            return true;
        }

        if (rules.required) {
            isValid = value.trim() !== '' && isValid;
        }

        if (rules.minLength) {
            isValid = value.length >= rules.minLength && isValid
        }

        if (rules.maxLength) {
            isValid = value.length <= rules.maxLength && isValid
        }

        if (rules.isEmail) {
            const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
            isValid = pattern.test(value) && isValid
        }

        if (rules.isNumeric) {
            const pattern = /^\d+$/;
            isValid = pattern.test(value) && isValid
        }

        return isValid;
    }

    onChange = (e) => {
        const updatedFormElement = {
            ...this.state[e.target.name]
        };

        let isValid = this.checkValidity(e.target.value, updatedFormElement.validation);
        if (!isValid) {
            updatedFormElement.cssClass = 'color--red';
        } else {
            updatedFormElement.cssClass = '';
        }
        updatedFormElement.value = e.target.value;
        this.setState({[e.target.name]: updatedFormElement});
    };

    handleSubmit = async (e) => {
        e.preventDefault();
        let res = await
            createEvent(
                this.state.name.value,
                'start',
                this.props.userId,
                this.state.location.value,
                this.state.start_date.value,
                this.state.details.value,
                this.state.price.value,
                this.state.name.value,
                1
            );
    };

    render() {
        return (
            <section className="task-details--create">
                <h1>See how little it could cost...</h1>
                <p>Event Name</p>
                <input name="name" type="input" placeholder="location"
                       value={this.state.name.value} className={this.state.name.cssClass}
                       onChange={this.onChange}></input>
                <p>Location</p>
                <input name="location" type="input" placeholder="location"
                       value={this.state.location.value} className={this.state.location.cssClass}
                       onChange={this.onChange}></input>
                <p>Start date?</p>
                <input name="due_date" type="input" placeholder="location"
                       value={this.state.start_date.value} className={this.state.start_date.cssClass}
                       onChange={this.onChange}></input>
                <p>Comments?</p>
                <input name="details" type="input" placeholder="details"
                       value={this.state.details.value} className={this.state.details.cssClass}
                       onChange={this.onChange}></input>
                <p>Price?</p>
                <input name="price" type="input" placeholder="price"
                       value={this.state.price.value} className={this.state.price.cssClass}
                       onChange={this.onChange}></input>
                <button className="submit-btn login-btn" onClick={this.handleSubmit}>Login</button>
            </section>
        );
    }
}


const mapDispatchToProps = dispatch => {
    return {
        openModal: () => dispatch(action.openModal()),
        closeModal: () => dispatch(action.closeModal())
    };
};

const mapStateToProps = (state) => {
    return {
        isModalOpen: state.modal.isModalOpen,
        userId: state.auth.userId,
        isAuth: state.auth.token !== null,
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Task);

