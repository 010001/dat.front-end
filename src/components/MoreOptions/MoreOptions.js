import React from 'react';
import './MoreOptions.css'

class MoreOptions extends React.Component{


    render(){
        const {taskStatus, taskOwner, awaitOffer} = this.props;
        return(
            <ul className="option--list">
                {taskStatus.status==='open'&&taskOwner&&!awaitOffer&&<li className="option--item--edit" onClick={this.props.openOfferModal}><p>Edit task</p></li>}
                <li className={taskStatus.status==='open'&&taskOwner&&!awaitOffer? "option--item--post":"option--item--edit"} onClick={this.props.openOfferModal}><p>Post a similar task</p></li>
                <li className={taskStatus.status==='open'&&taskOwner&&!awaitOffer? "option--item--alerts":"option--item--alters--collapse"} onClick={this.props.openAlertModal}><p>Set up alerts</p></li>
                {taskStatus.status==='open'&&taskOwner&&<li className={awaitOffer? "option--item--alerts":"option--item--cancel"} onClick={this.props.openCancelModal}><p>Cancel task</p></li>}
            </ul>
        )
    }
}

export default MoreOptions;