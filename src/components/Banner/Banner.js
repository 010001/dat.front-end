import React from 'react';
import bgimage from '../../img/office-cleaning-bg.jpg';
class Banner extends React.Component {
    render() {
        return (
            <React.Fragment>
                <div className={"max-height-800"}>
                <img alt="bg" className={"banner-bg"}
                     src={bgimage}/>
                </div>
            </React.Fragment>
        );
    }
}

export default Banner;
