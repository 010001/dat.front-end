import React from 'react';
import './OtherService.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faWindowMaximize, faSitemap, faCannabis, faDove, faCheck } from '@fortawesome/free-solid-svg-icons'

class OtherService extends React.Component {

    constructor(props) {
        super(props)

        this.state = {

            checkIfNeedOther: `none`,

            checkOthersBoxIdStatus: this.props.data,

            checkOthers: [`none`, `none`, `none`, `none`],

            iconArry: [faDove, faCannabis, faSitemap, faWindowMaximize],
        }
    }

    checkOthersSelection = (id, checkOthersArry) => {

        let { checkOthers } = this.state

        for (let i = 0; i < checkOthersArry.length; i++) {

            if (id === checkOthersArry[i]) {

                checkOthers[i] = `block`;
                this.setState({
                    checkOthers: checkOthers
                })
            } else {
                checkOthers[i] = `none`;
                this.setState({
                    checkOthers: checkOthers
                })
            }
        }
    }


    handleOnclick = (e) => {
        const checkOthersArry = this.state.checkOthersBoxIdStatus;
        const { checkIfNeedOther } = this.state;

        if (e.target.id === `checkOthers__ifNeedOthers`) {

            if (checkIfNeedOther === `none`) {

                this.setState({
                    checkIfNeedOther: `block`
                })
            } else if (checkIfNeedOther === `block`) {

                this.setState({
                    checkIfNeedOther: `none`
                })
            }
        }
        this.checkOthersSelection(e.target.id, checkOthersArry)

        this.props.callBack(`otherServices`, this.state.checkOthers)

    }

    render() {

        const { checkOthers, checkIfNeedOther, iconArry } = this.state

        return (
            <React.Fragment>
                <div className="TaskContainter TaskContainer--judgeIfEndOfLease" >
                    <div>
                        <div id="checkOthers__ifNeedOthers" className={`TaskContainter__checkBox`} onClick={this.handleOnclick}>
                            <FontAwesomeIcon id="checkOthers__ifNeedOthers" style={{ display: checkIfNeedOther, color: `rgb(0, 143, 180)` }} icon={faCheck} />
                        </div>
                    </div>
                    <p id="endOflease">This is an end-of-lease clean</p>
                </div>

                <p>Do you need any of these cleaned?</p>
                <ul className="TaskContainter TaskContainer--otherService" >
                    <div className="TaskContainer__ovenCover" style={{ display: checkIfNeedOther }}></div>

                    {
                        this.props.data.map((element, index) => {
                            return (
                                <li key={index} className={`list list--otherService`}>
                                        <div className={` checkBox-wrapper list__checkBox--${element} ${element}--${checkOthers[index]}`}>

                                            <div id={element} style={{ position: "relative" }} className="TaskContainter__checkBox TaskContainter__checkBox--display" onClick={this.handleOnclick}>
                                                <FontAwesomeIcon className={`list--checkIcon`} style={{ display: checkOthers[index] }} icon={faCheck} />
                                            </div>
                                            <p><FontAwesomeIcon className={`list--otherIcon`} icon={iconArry[index]} /></p>
                                            <p style={{ marginTop: "7px", fontSize: "14px" }}>{element}</p>
                                    </div>
                                </li>)
                        })
                    }
                </ul>
            </React.Fragment>
        )
    }
}

export default OtherService;