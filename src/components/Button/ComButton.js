import React from 'react';
class Bathroom extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            currentwhichButton: []
        }
    }

    handleOnclick = (e) => {
        const { buttonType, clickStatus } = this.props.data;
        if (e.target.className.includes(buttonType)) {
            const nextwhichButton = this.checkSelection(e.target.id, clickStatus)
            this.props.callBack(buttonType, nextwhichButton)
        }
    }

    checkSelection = (id, data) => {
        for (let [index, value] of data.entries()) {
            if (typeof this.bufferIndex !== `undefined`) {
                if (id === value && id.includes(`-click-state`)) {
                    this.currentIndex = index
                    this.bufferIndex = this.currentIndex
                } if (id === value && !id.includes(`-click-state`)) {
                    data[index] = `${value}-click-state`
                    data[this.bufferIndex] = data[this.bufferIndex].replace('-click-state', ``)
                    setTimeout(() => {
                        this.currentIndex = index;
                        this.bufferIndex = this.currentIndex
                    },1)    
                }
            } else {
                data[index] = `${value}-click-state`
                data[0] = data[0].replace('-click-state', ``)
                this.currentIndex = index;
                this.bufferIndex = this.currentIndex
            }
        }
        return data
    }

    static getDerivedStateFromProps(nextProps) {
        return {
            currentwhichButton: nextProps.data
        }
    }

    render() {
        const currentwhichButton = this.state.currentwhichButton.clickStatus
        return (
            <div>
                {
                    this.props.render(currentwhichButton, this.handleOnclick)
                }
            </div>
        )
    }
}

export default Bathroom;