import React from 'react';
import './Bedroom.css';

function Bedroom(props) {
    const buttonQ = props.updataData.clickStatus;
    return (
        <React.Fragment>
            {
                buttonQ.map((element, index) => {
                    return <button key={index} id={element} onClick={props.handleOnclick} className="TaskContainer__button TaskContainer__button--bedrooms">
                        {index + 1}
                    </button>
                })
            }
        </React.Fragment>
    )
}
export default Bedroom;
