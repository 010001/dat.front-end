import React from 'react'
import Bedroom from './Bedroom'
import { shallow, render, mount } from 'enzyme';
import toJson from "enzyme-to-json";


describe('Bedroom Test', () => {

    beforeEach(() => {
        jest.clearAllMocks();
    });

    const minProps = {
        data: [`one-click-state`, `two`, `three`, `four`, `five`]
    }

    const Bedroom__render = render(<Bedroom  {...minProps} />)
    expect(toJson(Bedroom__render)).toMatchSnapshot();

    const wrapper = shallow(<Bedroom {...minProps} />);
    const instance = wrapper.instance()

    const findByTestAttr = (wrapper, attr) => {
        return wrapper.find(`${attr}`);
    };

    test('renders without error', () => {
        expect(wrapper.find('.TaskContainer__button').exists());
    });
    // Cannot pass and don't know why/////
    test(`the clickStatus should be reset, when clicking on any of the BedRoom buttons`, () => {
        instance.checkSelection = jest.fn()
        const mockedEvent = { target: { id: `two` } }
        const btnContainer = findByTestAttr(wrapper, "#two")
        // btnContainer.simulate('click', mockedEvent)   
        //Cannot be simulate with click........
    })
})