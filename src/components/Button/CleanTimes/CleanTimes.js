import React from 'react';
import './CleanTimes.css';

function CleanTimes(props) {

    const buttonQ  = props.updataData.clickStatus;
    return (
        <React.Fragment>
            {
                buttonQ.map((element, index) => {
                    return <button key={index} id={element} onClick={props.handleOnclick} className={`TaskContainer__button  TaskContainer__button--cleanTimes`}>
                        {element.replace('-click-state', ``)}
                    </button>
                })
            }
        </React.Fragment>
    );
}
export default CleanTimes;

