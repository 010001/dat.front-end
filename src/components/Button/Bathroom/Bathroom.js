import React from 'react';
import './Bathroom.css';

function Bathroom(props) {
    const buttonQ = props.updataData.clickStatus;
    return (
        <React.Fragment>
            {
                buttonQ.map((element, index) => {
                    return <button key={index} id={element} onClick={props.handleOnclick} className="TaskContainer__button TaskContainer__button--bathrooms">
                        {index + 1}
                    </button>
                })
            }
        </React.Fragment>
    )
}
export default Bathroom;
