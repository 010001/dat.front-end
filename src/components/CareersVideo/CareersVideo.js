import React from 'react';
import "./CareersVideo.css";
export default class CareersVideo extends React.Component {
    constructor(props){
        super(props);
        // create ref 
        this.video = React.createRef();
        this.playVideo = this.playVideo.bind(this);
    }

    playVideo(){
        this.refs.vidRef.src+=";autoplay=1";
        setTimeout(() => {
            this.refs.vidBtn.className+=" none"
        }, 2000);
        // console.log('success')
    }
  
    render(){
        const {videoDetail}=this.props

        return(
                <div className="careers-video-group">
                    <div className="careers-video-player"> 
                        <div className="careers-video-wrapper">
                            {/* <div className="careers-video-button"  onClick={this.playVideo} ref="vidBtn"></div> */}
                            <iframe type="text/html" width="auto" height="auto" ref="vidRef" src={videoDetail.src} title={videoDetail.title}></iframe>
                        </div>
                    </div>
                    <div className="careers-video-caption">
                        <h4 className="text-center">{videoDetail.title}</h4>
                        <h5 className="text-center">{videoDetail.subtitle}</h5>
                    </div>   
                </div>
        );
        
    }

}







