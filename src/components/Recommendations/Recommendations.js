import React from 'react';
import RecommentionList from '../RecommendationList/RecommendationList';
import './Recommendations.css';

class Recommentions extends React.Component {

    render() {
        return (
            <div className="recommendations-wrapper zoom">
                <div className="recommendations">
                    <div className="recommendations__header">
                        <p>Best rated Cleaners</p>
                    </div>
                    <div className="recommendations__Lists animation-scroll">
                        <RecommentionList />
                        <RecommentionList />
                        <RecommentionList />
                        <RecommentionList />
                        <RecommentionList />
                        <RecommentionList />
                        <RecommentionList />
                    </div>
                </div>
            </div>

        );
    }
}

export default Recommentions;
