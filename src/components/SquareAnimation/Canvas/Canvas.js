import React from "react";
import './Canvas.css';
class Canvas extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isDown: false,
            previousPointX: '',
            previousPointY: '',
            base_image: {},
            circleConfig: {
                maxCircle: 4,
            },
            circles: this.genrateRandomCircle(100),
            canvasId: this.props.canvasid,
            rotate: this.props.rotate,

        };

        this.saveContext = this.saveContext.bind(this);
    }

    saveContext(ctx) {
        this.ctx = ctx;
    }

    getRandomInt = (min, max) => {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    };
    getRandomFloat = (min, max) => {

        return Math.random() * (max - min + 1) + min;
    };

    genrateRandomCircle = (total) => {
        const a = [];
        for (let i = 0; i < total; i++) {
            let x = this.getRandomInt(0, 1920);
            let y = this.getRandomInt(0, 1080);
            let radius = this.getRandomInt(0, 5);
            let velocity = this.getRandomFloat(0.1, 0.2);
            let opacity = this.getRandomFloat(0.1, 0.9);
            let opacityVelocity = 0.01;
            a.push({
                x,
                y,
                radius,
                velocity,
                opacity,
                opacityVelocity
            });
        }
        return a;
    };

    drawCircle = (circles, ctx) => {
        const circ = this.state.circles;


        for (let i = 0; i < circ.length; i++) {
            let y = circ[i].y -= circ[i].velocity;
            if (y <= 0) {
                let x = this.getRandomInt(0, 1920);
                let y = 1080 + 50;
                let velocity = this.getRandomFloat(0.1, 0.15);
                circ[i].x = x;
                circ[i].y = y;
                circ[i].velocity = velocity;
            }

            if (circ[i].opacity > 1 || circ[i].opacity < 0) {
                circ[i].opacityVelocity *= -1;
            }

            ctx.beginPath();
            ctx.arc(circ[i].x, circ[i].y -= circ[i].velocity, circ[i].radius, 0, 2 * Math.PI);
             circ[i].opacity += circ[i].opacityVelocity;
            ctx.fillStyle = `rgba( 251, 124,54, ${circ[i].opacity} )`;
            ctx.fill();
        }
    };

    componentDidUpdate() {
        // let circle = new Path2D();
        // circle.moveTo(125, 35);
        // circle.arc(100, 35, 25, 0, 2 * Math.PI);
        // this.ctx.fill(circle);


        const width = this.ctx.canvas.width;
        const height = this.ctx.canvas.height;
        this.ctx.save();
        //    this.ctx.beginPath();
        this.ctx.clearRect(0, 0, width, height);
        this.drawCircle(100, this.ctx);
        this.ctx.restore();
    }

    render() {
        return <canvas className={"canvas--bg " +  this.props.style } width="1920" height="1080"
                       ref={node => node ? this.saveContext(node.getContext('2d')) : null}
        />;
    }
}

export default Canvas;
