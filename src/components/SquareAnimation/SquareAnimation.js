import React from "react";
import Canvas from './Canvas/Canvas';
let start = null;
class SquareAnimation extends React.Component {
    constructor(props) {
        super(props);
        this.state = {angle: 0,delta:0};
        this.updateAnimationState = this.updateAnimationState.bind(this);
    }

    componentDidMount() {
        this.rAF = requestAnimationFrame(this.updateAnimationState);
    }

    updateAnimationState(timestamp) {
        if (!start) start = timestamp;
        var progress = timestamp - start;
        this.setState(prevState => ({angle: prevState.angle + 1,delta:progress}));
        this.rAF = requestAnimationFrame(this.updateAnimationState);
    }

    componentWillUnmount() {
        cancelAnimationFrame(this.rAF);
    }

    render() {
        return <Canvas angle={this.state.angle} delta={this.state.delta} style={this.props.cssStyle}/>
    }
}

    export default SquareAnimation;
