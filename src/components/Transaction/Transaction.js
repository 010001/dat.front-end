import React from 'react';
import "./Transaction.css";
import moment from "moment";


class Transaction extends React.Component {

    render() {
        return (
            <div className={"flex flex--space-between"}>
                <p>{moment(new Date(this.props.data.createdAt)).format("ddd, D MMM")}</p>
                <p>${this.props.data.amount}</p>
            </div>
        );
    }
}

export default Transaction;
