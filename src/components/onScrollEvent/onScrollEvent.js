import { thingAniSheet } from '../Thing/ThingAnimationStylesheet/ThingAnimationSheet'
const judgeLocation = (e) => {
    return e.target.location.pathname
}

export const onScrollEvent = (e) => {
    const whichPath = judgeLocation(e) // this point to window at this moment.
    const bottomPosition = window.pageYOffset + window.innerHeight;
    const classList = document.getElementsByClassName(`thing`)
    const scrollPosition = window.scrollY;

    if (whichPath === `/create-task/clean`) {
        if (scrollPosition > 760) {
            return { dropDown: `dropDown`, hidden: `hidden`, dropDownBtn: `dropDown-btn` }
        } if (scrollPosition < 350) {
            return { dropDown: ``, hidden: ``, dropDownBtn: `` }
        } else {
            return null
        }
    } if (whichPath === `/`) {
        const Arry = [];
        for (let i = 0; i < classList.length; i++) {
            const ElementCenter = classList[i].offsetHeight / 2 + classList[i].offsetTop
            Arry.push(ElementCenter)
        }
        if (Arry[0] - 100 < bottomPosition && Arry[1] - 100 > bottomPosition) {
            const aniName = thingAniSheet.judgeAniName(``, `device`)
            thingAniSheet.animationDevice.aniName = aniName
            return ({
                dataAni: {
                    animationDevice: thingAniSheet.animationDevice
                }
            })
        }
        if (Arry[0] - bottomPosition > 400) {
            const aniName = thingAniSheet.judgeAniName(``, `nothing`)
            thingAniSheet.animationDevice.aniName = aniName
            thingAniSheet.animationGroup.aniName = aniName
            thingAniSheet.animationBadges.aniName = aniName
            return ({
                dataAni: {
                    animationDevice: thingAniSheet.animationDevice,
                    animationGroup: thingAniSheet.animationGroup,
                    animationBadges: thingAniSheet.animationBadges
                }
            })
        } if (Arry[1] - 100 < bottomPosition && Arry[2] - 100 > bottomPosition) {
            const aniName = thingAniSheet.judgeAniName(``, `group`)
            thingAniSheet.animationGroup.aniName = aniName
            return ({
                dataAni: {
                    animationGroup: thingAniSheet.animationGroup,
                }
            })
        }
        if (Arry[2] - 100 < bottomPosition && bottomPosition > 3800) { 
            //page animation will change elementcenter.
            // So have to limit to 3800
            const aniName = thingAniSheet.judgeAniName(``, `badges`)
            thingAniSheet.animationBadges.aniName = aniName
            return ({
                dataAni: {
                    animationBadges: thingAniSheet.animationBadges
                }
            })
        }
    }
}



