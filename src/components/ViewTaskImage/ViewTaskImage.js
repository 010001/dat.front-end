import React from 'react';
import "./ViewTaskImage.css";

class ViewTaskImage extends React.Component {
    
    render() {
        return (
            <React.Fragment>
                <div className={this.props.show? "attachment--container":"componentUnVisible"}>
                <img className={"attachment--img"}
                     src={this.props.img} alt="attachmentFile" height="100" width="100"/>
                <svg id="cross-circle"
                     viewBox="0 0 16 16" width="16" height="16"
                     onClick={(e) => this.props.closeAttachment(e.target)}>
                    <path
                        d="M8 8.571l-2.31 2.31a.404.404 0 1 1-.572-.57L7.428 8l-2.31-2.31a.404.404 0 1 1 .571-.572L8 7.428l2.31-2.31a.404.404 0 1 1 .572.571L8.572 8l2.31 2.31a.404.404 0 1 1-.571.572L8 8.572zM16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0z"
                        fillRule="evenodd"/>
                </svg>
                </div>
            </React.Fragment>
        );
    }
}

export default ViewTaskImage;
