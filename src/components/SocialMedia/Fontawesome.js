import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
// import {faFacebook} from '@fortawesome/free-regular-svg-icons';
import { fab } from '@fortawesome/free-brands-svg-icons'
import { library } from '@fortawesome/fontawesome-svg-core'
library.add(fab)

export const Facebook = <FontAwesomeIcon icon={['fab', 'facebook']} size="2x" color="lightblue" />;
export const Twitter = <FontAwesomeIcon icon={['fab', 'twitter']} size="2x" color="lightblue" />;
export const Linkedin = <FontAwesomeIcon icon={['fab', 'linkedin']} size="2x" color="lightblue" />;
export const Instagram = <FontAwesomeIcon icon={['fab', 'instagram']} size="2x" color="lightblue" />;
export const FileAttachment = <FontAwesomeIcon icon={['fab', 'paperclip']} size="2x" color="lightblue" />;
