import React from 'react';
import './SocialMedia.css';
import {
    Facebook, Twitter, Linkedin, Instagram

} from './Fontawesome';

class SocialMedia extends React.Component {
    render() {
        const url = "https://www.google.com";
        const facebookUrl = `https://www.facebook.com/sharer/sharer.php?u=${url}`;
        const twitterUrl = `https://twitter.com/home?status=${url}`;
        const linkedinUrl = `https://www.linkedin.com/shareArticle?mini=true&url=${url}`;

        return (
            <div className="task__share__ShareContainer">
                <div className="task__share__TextBackground"><p color="#545a77" className="Text__StyledTypographyComponent" >share</p></div>
                <div>
                <ul className="socialMedia flex flex__wrap justifySpaceAround">
                    <li><a className={"fb--hover"} target="_blank" rel="noopener noreferrer" href={facebookUrl}>{Facebook}</a></li>
                    <li><a className={"twitter--hover"} target="_blank" rel="noopener noreferrer" href={twitterUrl}>{Twitter}</a></li>
                    <li><a className={"linkedin--hover"} target="_blank" rel="noopener noreferrer" href={linkedinUrl}>{Linkedin}</a></li>
                    <li><a className={"ig--hover"} target="_blank" rel="noopener noreferrer" href={linkedinUrl}>{Instagram}</a></li>
                </ul>
                </div>
            </div>
        );
    }
}

export default SocialMedia;
