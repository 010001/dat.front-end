import React from 'react';
import './FinishOffer.css';
import axios from 'axios';
import * as action from "../../../store/actions";
import configuration from "../../../config/config";
import {connect} from "react-redux";

class FinishOffer extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            user_id: {
                value: '',
            },
            details: {
                elementConfig: {
                    placeholder: 'kitmanwork@gmail.com'
                },
                validation: {
                    required: true,
                },
                errorMessage: {
                    required: 'Email is required',
                },
                valid: false,
                value: 'kitmanwork@gmail.com',
                cssClass: '',
            },
            price: {
                elementConfig: {
                    placeholder: 'kitmanwork@gmail.com'
                },
                validation: {
                    required: true,
                },
                errorMessage: {
                    email: "Not valid Email",
                    required: 'Email is required',
                },
                valid: false,
                value: 500,
                cssClass: '',
            },
            offer_id: {
                value: this.props.id
            },
        };
        this.handleChange = this.handleChange.bind(this);
    }


    handleChange = (e) => {
        const updatedFormElement = {
            ...this.state[e.target.name]
        };
        let isValid = true;
        //let isValid = this.checkValidity(e.target.value, updatedFormElement.validation);
        if (!isValid) {
            updatedFormElement.cssClass = 'color--red';
        } else {
            updatedFormElement.cssClass = '';
        }
        updatedFormElement.value = e.target.value;
        this.setState({[e.target.name]: updatedFormElement});

    };

    completeOffer = async () => {
        await axios.put(configuration.api.backend_api + '/api/v1/offers/' + this.state.offer_id.value, {
            price: this.state.price.value,
            comments: this.state.details.value,
        });
        this.props.closeModal();
    };

    render() {
        return (
            <div>
                <h3>Offer Completed</h3>
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        openModal: () => dispatch(action.openModal()),
        closeModal: () => dispatch(action.closeModal())
    };
};

export default connect(null, mapDispatchToProps)(FinishOffer);
