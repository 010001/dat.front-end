import React from 'react';
import './RateStar.css';

class WKRateStar extends React.Component {
    static defaultProps = {
        canClick: false,
        rateNum: 5,
        handleSelectRate: null,
        rateValue: 0
    };

    constructor (props) {
        super(props);
        this.state = {
            canClick: this.props.clickable,
            rateValue: this.props.rate,
            rateArray: new Array(Number(props.rateNum)).fill('')
        }
    }

    handleSelectRate (value) {
        if (!this.state.canClick) {
            return
        }
        this.setState({
            rateValue: value
        });
        this.props.handleSelectRate && this.props.handleSelectRate(value)
    }

    render () {
        const {rateArray, rateValue} = this.state;
        const {rateNum} = this.props;
        return (
            <div className="rate">
                <div className="rate__bg">
                    {rateArray.map((item, index) => <span onClick={() => this.handleSelectRate(index+1)} key={`rate_${index}`}>☆</span>)}
                    <div className="bg__realrate" style={{width: `calc(${rateValue ? rateValue : this.props.rateValue} / ${rateNum} * 100%)`}}>
                        {rateArray.map((item, index) => <span onClick={() => this.handleSelectRate(index+1)} key={`rate_selected_${index}`}>★</span>)}
                    </div>
                </div>
                <p id="MarkScore">{ parseFloat(this.state.rateValue).toFixed(1)} ({this.props.totalPeople})</p>
            </div>
        )
    }
}

export default WKRateStar
