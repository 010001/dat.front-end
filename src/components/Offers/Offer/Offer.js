import React from 'react';
import RateStar from '../RateStar/RateStar';
import "./Offer.css";
import {connect} from "react-redux";

import ErrorBoundary from "../../../hoc/ErrorBoundary/ErrorBoundary";
import AcceptModal from "../../Modal/AcceptModal/AcceptModal";
import {PosterState, TaskerState} from "../../../config/task";
import PaymentWorkflow from "../../../workflow/PaymentWorkflow/PaymentWorkflow";
import {NavLink} from "react-router-dom";


class Offer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            collapse: true,
            toggleAcceptModal: false,
            togglePaymentWorkflowModal: false,
            showAcceptBtn: false,
            paymentSuccess: false,
        };
    }

    collapse = (e) => {
        e.preventDefault();
        if (this.state.collapse) {
            this.setState({
                collapse: false,
            })
        } else {
            this.setState({
                collapse: true,
            })
        }
    };

    offerBelongToUser = () => {
        let currentUserID = this.props.userId;
        return currentUserID === this.props.data.user_id._id;
    };

    toggleAcceptModal = () => {
        this.setState({'toggleAcceptModal': !this.state.toggleAcceptModal})
    };
    togglePaymentWorkflowModal = () => {
        this.setState({'togglePaymentWorkflowModal': !this.state.togglePaymentWorkflowModal})
    };

    shouldShowOffer(){
        return this.props.data.user_id._id === this.props.userId && this.props.state === TaskerState.CREATED_OFFER  ;
    }

    render() {

        return (
            <ErrorBoundary>
                <div className="offer">
                    <div className="flex  offer-header-container flex--space-between">
                        <div className="responsive--collapse user-details flex flex--vertical-center">
                            <div className="avatar-img avatar--position">
                                <img id="user--icon"
                                         src={ this.props.data.user_id.images || "https://via.placeholder.com/256"}
                                     alt="usericon"></img>
                            </div>
                            <div className="flex flex-wrap">
                                <p className="width--full">
                                    <NavLink to={'/user/' + this.props.data.user_id._id}>{this.props.data.user_id.name.firstName + ' ' + this.props.data.user_id.name.lastName}</NavLink>
                                </p>
                                <RateStar clickable={false} rate={this.props.data.user_id.rating}
                                          totalPeople={this.props.data.user_id.totalPeople}/>
                                <p className="width--full">100% Completion Rate</p>
                            </div>
                        </div>
                        <div className="flex flex--vertical-center responsive--collapse">
                            {/*TODO Fixed:100*/}
                            <p className="offer-price">${this.props.data.price}</p>

                            <div className="status-btn">
                                {this.props.state === PosterState.REVIEWING &&
                                <button className="button--green" onClick={this.togglePaymentWorkflowModal}>Accept
                                </button>}
                                { this.shouldShowOffer() &&
                                <button className="button--green" onClick={this.toggleAcceptModal}
                                        value={this.props.data._id}>Withdraw
                                </button>}
                            </div>
                        </div>
                    </div>

                    <div className="offer-comment">
                        <p style={{height: this.state.collapse === true ? 10 + "px" : 50 + "px"}}
                           id={"details__content--collapse"}
                           className="details__content">
                            {this.props.data.comments}
                            <br/>
                            {/*{this.props.data.user_id.name}*/}
                        </p>

                        <button className="collapse-btn"
                                onClick={(e) => this.collapse(e)}>{this.state.collapse === true ? "More" : "Less"}
                            <div className="collapse-btn-style">
                                <svg width="20px" height="14px" viewBox="0 0 24 24">
                                    <path
                                        d="M12 16.86a.9.9 0 0 1-.61-.25l-8-8a.86.86 0 0 1 1.22-1.22l7.39 7.4 7.39-7.4a.86.86 0 0 1 1.22 1.22l-8 8a.9.9 0 0 1-.61.25z"></path>
                                </svg>
                            </div>
                        </button>
                        {
                            //show withDrawButton
                            this.state.toggleAcceptModal &&
                            <AcceptModal modalClose={this.toggleAcceptModal} accept={this.props.withdraw}
                                         value={this.props.data._id}/>
                        }
                        {
                            this.state.togglePaymentWorkflowModal &&
                            <PaymentWorkflow price={this.props.data.price} success={this.props.successPayment} close={this.togglePaymentWorkflowModal}  currentUserID={this.props.userId} paymentUserID={this.props.data.user_id._id} taskID={this.props.taskID}/>
                        }
                    </div>
                </div>
            </ErrorBoundary>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        isModalOpen: state.modal.isModalOpen,
        userId: state.auth.userId
    }
};

export default connect(mapStateToProps, null)(Offer);
