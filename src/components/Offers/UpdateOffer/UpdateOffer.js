import React from 'react';
import './UpdateOffer.css';
import axios from 'axios';
import * as action from "../../../store/actions";
import configuration from "../../../config/config";
import {connect} from "react-redux";
import Hexagon from "../../Hexagon/Hexagon";


export class UpdateOffer extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            user_id: {
                value: '',
            },
            details: {
                elementConfig: {
                    placeholder: 'kitmanwork@gmail.com'
                },
                validation: {
                    required: true,
                },
                errorMessage: {
                    required: 'Email is required',
                },
                valid: false,
                value: 'kitmanwork@gmail.com',
                cssClass: '',
            },
            price: {
                elementConfig: {
                    placeholder: 'kitmanwork@gmail.com'
                },
                validation: {
                    required: true,
                    isNumeric: true,
                    min:5,
                },
                errorMessage: {
                    email: "Not valid Email",
                    required: 'Email is required',
                },
                valid: false,
                value: 500,
                cssClass: '',
            },
            offer_id: {
                value: this.props.id
            },
        };
        this.handleChange = this.handleChange.bind(this);
    }


    checkValidity(value, rules) {
        let isValid = true;
        if (!rules) {
            return true;
        }

        if (rules.required) {
            isValid = value.trim() !== '' && isValid;
        }

        if (rules.min) {
            isValid = value >= rules.min && isValid
        }

        if (rules.minLength) {
            isValid = value.length >= rules.minLength && isValid
        }

        if (rules.maxLength) {
            isValid = value.length <= rules.maxLength && isValid
        }

        if (rules.isEmail) {
            const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
            isValid = pattern.test(value) && isValid
        }

        if (rules.isNumeric) {
            const pattern = /^\d+$/;
            isValid = pattern.test(value) && isValid
        }

        return isValid;
    }

    handleChange = (e) => {
        const updatedFormElement = {
            ...this.state[e.target.name]
        };
        let isValid = this.checkValidity(e.target.value, updatedFormElement.validation);
        if (!isValid) {
            updatedFormElement.cssClass = 'color--red';
        } else {
            updatedFormElement.cssClass = '';
        }
        updatedFormElement.value = e.target.value;
        this.setState({[e.target.name]: updatedFormElement});

    };

    updateOffer = () => {
        axios.put(configuration.api.backend_api + '/api/v1/offers/' + this.state.offer_id.value, {
            price: this.state.price.value,
            comments: this.state.details.value,
        }).then((response) => {
            this.props.success(response.data.offer);
        }).catch((error) => {
            this.props.failed(error);
        });
    };


    render() {
        return (
            <React.Fragment>
                <h1>Update Offers</h1>
                <div className="container--form">
                    <label className="width--full">Price:</label>
                    <input onChange={this.handleChange} name="price" className={this.state.price.cssClass + " width--full"} type="text"/>
                </div>
                <div className="container--form">
                    <label className="width--full">Comments:</label>
                    <textarea onChange={this.handleChange} name="comments"
                              placeholder="eg. I will be great for this task. I have the necessary experience, skills and equipment required to get this done."
                              className="width--full"/>
                </div>
                <div className={"flex"}>
                    <button onClick={this.updateOffer} className="button button-xs--spacing  button--green" data-test="updateOffer">Submit
                    </button>
                    <button onClick={this.props.cancelClick} className="button button-xs--spacing">Cancel</button>
                </div>
                <Hexagon size={"xs"} classes={"position-5 color--grey"}/>
                <Hexagon size={"lg"} classes={"position-7 color--grey"}/>
            </React.Fragment>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        openModal: () => dispatch(action.openModal()),
        closeModal: () => dispatch(action.closeModal())
    };
};

export default connect(null, mapDispatchToProps)(UpdateOffer);
