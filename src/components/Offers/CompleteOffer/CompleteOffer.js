import React from 'react';
import './CompleteOffer.css';

import * as action from "../../../store/actions";
import PropsTypes from "prop-types";
import {connect} from "react-redux";
import RateStarSimple from "../RateStarSimple/RateStarSimple";
import Hexagon from "../../Hexagon/Hexagon";

class UpdateOffer extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            user_id: {
                value: '',
            },
            details: {
                elementConfig: {
                    placeholder: 'kitmanwork@gmail.com'
                },
                validation: {
                    required: true,
                },
                errorMessage: {
                    required: 'Email is required',
                },
                valid: false,
                value: 'kitmanwork@gmail.com',
                cssClass: '',
            },
            rate: {
                elementConfig: {
                    placeholder: 'kitmanwork@gmail.com'
                },
                validation: {
                    required: true,
                },
                errorMessage: {
                    email: "Not valid Email",
                    required: 'Email is required',
                },
                valid: false,
                value: 500,
                cssClass: '',
            },
            rating:0,
        };
        this.handleChange = this.handleChange.bind(this);
    }


    handleChange = (e) => {
        const updatedFormElement = {
            ...this.state[e.target.name]
        };
        let isValid = true;
        //let isValid = this.checkValidity(e.target.value, updatedFormElement.validation);
        if (!isValid) {
            updatedFormElement.cssClass = 'color--red';
        } else {
            updatedFormElement.cssClass = '';
        }
        updatedFormElement.value = e.target.value;
        this.setState({[e.target.name]: updatedFormElement});

    };

    onStarClick = (rate) =>{
        this.setState({rating: rate});
    };

    render() {
        return (
            <React.Fragment>
                <h3>Completing Offers</h3>

                <div className="container--form">
                    <label className="width--full">Rate:</label>
                    <RateStarSimple clickable={true} rate={this.state.rate} onStarClick={this.onStarClick}
                              totalPeople={0}/>
                </div>
                <div className="container--form">
                    <label className="width--full">Comments:</label>
                    <textarea onChange={this.handleChange} name="comments"
                              placeholder="eg. I will be great for this task. I have the necessary experience, skills and equipment required to get this done."
                              className="width--full"/>
                </div>
                <button onClick={() => {this.props.submitClick(this.state.rating,this.state.details.value)}} className="button button--green">Submit</button>
                <Hexagon size={"xs"} classes={"position-5 color--grey"}/>
                <Hexagon size={"lg"} classes={"position-7 color--grey"}/>
            </React.Fragment>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        openModal: () => dispatch(action.openModal()),
        closeModal: () => dispatch(action.closeModal())
    };
};

UpdateOffer.propsTypes = {
    id: PropsTypes.number,
    taskId: PropsTypes.number
};

export default connect(null, mapDispatchToProps)(UpdateOffer);
