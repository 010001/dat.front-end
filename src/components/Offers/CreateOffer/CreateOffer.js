import React from 'react';
import './CreateOffer.css';

import axios from "axios";
import configuration from "../../../config/config";
import Hexagon from "../../Hexagon/Hexagon";

export class CreateOffer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user_id: {
                value: this.props.userId,
            },
            price: {
                elementConfig: {
                    placeholder: ''
                },
                validation: {
                    required: true,
                    isNumeric: true
                },
                errorMessage: {
                    email: "Not valid Email",
                    required: 'Email is required',
                },
                valid: false,
                value: '123',
                cssClass: '',
            },
            comments: {
                elementConfig: {
                    placeholder: 'kitmanwork@gmail.com'
                },
                validation: {
                    required: true,
                },
                errorMessage: {
                    email: "Not valid Email",
                    required: 'Email is required',
                },
                valid: false,
                value: 'sdfdsffs',
                cssClass: '',
            },
            task_id: {
                value: this.props.id
            },
        };
        this.handleChange = this.handleChange.bind(this);
    }

    checkValidity(value, rules) {
        let isValid = true;
        if (!rules) {
            return true;
        }

        if (rules.required) {
            isValid = value.trim() !== '' && isValid;
        }

        if (rules.minLength) {
            isValid = value.length >= rules.minLength && isValid
        }

        if (rules.maxLength) {
            isValid = value.length <= rules.maxLength && isValid
        }

        if (rules.isEmail) {
            const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
            isValid = pattern.test(value) && isValid
        }

        if (rules.isNumeric) {
            const pattern = /^\d+$/;
            isValid = pattern.test(value) && isValid
        }

        return isValid;
    }

    handleChange = (e) => {
        const updatedFormElement = {
            ...this.state[e.target.name]
        };
        let isValid = this.checkValidity(e.target.value, updatedFormElement.validation);
        if (!isValid) {
            updatedFormElement.cssClass = 'width--full color--red';
        } else {
            updatedFormElement.cssClass = 'width--full';
        }

        updatedFormElement.value = e.target.value;
        this.setState({[e.target.name]: updatedFormElement});
    };

    submitOffer = async () => {
        //mock promise

        axios.post(configuration.api.backend_api + '/api/v1/offers', {
            user_id: this.state.user_id.value,
            price: this.state.price.value,
            comments: this.state.comments.value,
            taskId: this.state.task_id.value
        }).then((response) => {
            this.props.success(response.data.offer);
        }).catch((error) => {
            this.props.failed(error);
        });
    };

    render() {
        return (
            <React.Fragment>
                <h3 data-test="fxxk">参加活动</h3>
                <div className="container--form">
                    <label className="width--full">Price:</label>
                    <input onChange={this.handleChange} name="price" className={this.state.price.cssClass} type="text"
                           data-test="price"/>
                </div>
                <div className="container--form">
                    <label className="width--full">Comments:</label>
                    <textarea onChange={this.handleChange} name="comments"
                              placeholder="eg. I will be great for this task. I have the necessary experience, skills and equipment required to get this done."
                              className="width--full" data-test="comments"/>
                </div>
                <div className={"flex"}>
                    <button onClick={() => {
                        this.submitOffer();
                    }} className="button button--green" data-test="submitOffer">参加
                    </button>
                </div>
                <Hexagon size={"xs"} classes={"position-5 color--grey"}/>
                <Hexagon size={"xs"} classes={"position-6 color--grey"}/>
                <Hexagon size={"lg"} classes={"position-7 color--grey"}/>
            </React.Fragment>
        );
    }
}

export default CreateOffer;

