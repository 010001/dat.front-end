import React from "react";
import "./Badges.css";
class Badges extends React.Component{
        constructor(props){
                super(props);
                this.state = {

                }
        }
        render(){
                return(
                        <div className="splitter-section-badges">
                                <h3 className="splitter-badges-name">badges</h3>
                                <div className="splitter-section-content">
                                        <ul className="badge-group">
                                                <label className="badge-label">id badges</label>
                                                <li className="badge-item">
                                                        <img className="badge-image" alt="" src="https://via.placeholder.com/114"></img>
                                                         <p className="badge-item-block">Mobile</p>
                                                </li>
                                                <li className="badge-item">
                                                        <img className="badge" alt="" src="https://via.placeholder.com/114"></img>
                                                         <p className="badge-item-block">Facebook</p>
                                                </li>
                                                <a className="badge-button-min" href="/">Get a badge</a>
                                        </ul>
                                        <ul className="badge-group">
                                                <label className="badge-label">license badges</label>
                                                <p>Show off your hard-earned qualifications and licenses</p>
                                                <a className="button-min button-sml bold" href="/">Get a badge</a>
                                        </ul>
                                        <ul className="badge-group">
                                                <label className="badge-label">partnership badges</label>
                                                <p>Get exclusive access to partnership tasks.</p>
                                                <a className="button-min button-sml bold" href="/">Get a badge</a>
                                        </ul>
                                </div>
                        </div>
                );
        }
}

export default Badges;
