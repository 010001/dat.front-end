import React from "react";
import "./Reviews.css";

class Reviews extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            taskTabClassname: ['tab half worker small', 'tab half worker small active'],
            postTabClassname: ['tab half poster small', 'tab half poster small active'],
            taskReviewbottomtabClassname: ['user_profile_reviews_contentin hidden', 'user_profile_reviews_contentin']
        }
    }

    handleTaskTab = classname => {
        if (classname === 'tab half worker small active' || classname === 'tab half poster small active') return "";
        let { taskTabClassname, postTabClassname, taskReviewbottomtabClassname } = this.state;
        [taskTabClassname[0], taskTabClassname[1]] = [taskTabClassname[1], taskTabClassname[0]];
        [postTabClassname[0], postTabClassname[1]] = [postTabClassname[1], postTabClassname[0]];
        [taskReviewbottomtabClassname[0], taskReviewbottomtabClassname[1]] = [taskReviewbottomtabClassname[1], taskReviewbottomtabClassname[0]];
        this.setState({ taskTabClassname, postTabClassname, taskReviewbottomtabClassname });
    }

    render() {
        return (
            <div className="splitter-section reviews-section">
                <div className="splitter-section-name">REVIEWS</div>
                <div className="splitter-section-content" >
                    <div className="splitter-section-content-inner clearfix">
                        <div className="user_profile_reviews_out">
                            <div className="user_profile_reviews_in">
                                <div className="tabs">
                                    <span className={this.state.taskTabClassname[1]} onClick={() => { this.handleTaskTab(this.state.taskTabClassname[1]) }} id="taskTabn" >As a Tasker</span>
                                    <span className={this.state.postTabClassname[0]} onClick={() => { this.handleTaskTab(this.state.postTabClassname[0]) }} id="postTabn" >As a Poster</span>
                                </div>
                            </div>
                        </div>
                        <div className="user_profile_reviews_content">
                            <div className={this.state.taskReviewbottomtabClassname[1]} id="taskReviewn">
                                <p>This user has no reviews as a Tasker yet</p>
                            </div>
                            <div className={this.state.taskReviewbottomtabClassname[0]} id="postReviewn">
                                <p>This user has no reviews as a Job Poster yet</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Reviews;