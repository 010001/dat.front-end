import React from "react";
import "./Skills.css";
import {
    pen
} from '../../../ViewTasks/Fontawesome';

export class Skills extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            skillsFlag: true,
            transportation: ["Bicycle", "Car", "Online", "Scooter", "Truck", "Walk"]
        }
    }

    skillsShow = () => {
        const { skillsFlag } = this.state;
        return skillsFlag;
    }

    skillsEdit = () => {
        return !this.skillsShow();
    }

    skillsToggle = () => {
        this.setState({
            skillsFlag: !this.state.skillsFlag
        })
    }
    componentWillReceiveProps(nextProps){
        if (this.props !== nextProps){
            this.props  = nextProps;
        }
    }
    render() {
        return (
            <div className=" skills-section">
                <div className="splitter-section-name">SKILLS</div>
                <div className="splitter-section-content" >
                    <div className="splitter-section-content-inner clearfix">
                        {this.skillsShow() && <div>
                            {!this.props.otherUser && <div className="pen-container pen4" onClick={this.skillsToggle} data-test="skilltoggle">
                                <div>{pen}</div>
                            </div>}
                            {(this.props.education === "" && this.props.specialities === "" && this.props.languages) &&
                                <div className="skills">This user has not added any skills yet.</div>}
                            {this.props.education && <div className="skills-category-dd">
                                <div className="skill-title-dd">EDUCATION</div>
                                <div className="tag-selector-static-dd">
                                    <span>{this.props.education}</span>
                                </div>
                            </div>}
                            {this.props.specialities && <div className="skills-category-dd">
                                <div className="skill-title-dd">SPECIALITIES</div>
                                <div className="tag-selector-static-dd">
                                    <span>{this.props.specialities}</span>
                                </div>
                            </div>}
                            {this.props.languages && <div className="skills-category-dd">
                                <div className="skill-title-dd">LANGUAGES</div>
                                <div className="tag-selector-static-dd">
                                    <span>{this.props.languages}</span>
                                </div>
                            </div>}
                            {this.props.work && <div className="skills-category">
                                <div className="skill-title-dd">WORK</div>
                                <div className="tag-selector-static-dd">
                                    <span>{this.props.work}</span>
                                </div>
                            </div>}

                            {this.props.allTransports && this.props.allTransports.length > 0 && <div className="skills-category">
                                <div className="skill-title-dd">TRANSPORTATION</div>
                                {this.props.allTransports.length > 0 &&
                                    this.props.allTransports.map((items, index) =>
                                        <div className="tag-selector-static-dd" key={index}>
                                            <span>{items}</span></div>)}
                            </div>}

                        </div>}
                       {this.skillsEdit() && <div className="user-skills">
                            <div className="skill-category education editable">
                                <div className="skill-title">Education</div>
                                <input className="tag-selector editable" type="text" name="education"
                                    value={this.props.education} onChange={
                                        this.props.handleEducationValue
                                    }></input>
                                <div className="suggestions">
                                    {this.props.universities.map((items, index) =>
                                        <div key={index}
                                            onClick={() => this.props.handleEducationhintValue(items)}
                                        >{items.name}</div>
                                    )}
                                </div>
                                <div className="skill-category education editable">
                                    <div className="skill-title">SPECIALITIES</div>
                                    <input className="tag-selector editable" type="text" name="specialities"
                                        value={this.props.specialities} onChange={(e) => {this.props.handleChange(e)}}></input>
                                </div>
                                <div className="skill-category education editable">
                                    <div className="skill-title">LANGUAGES</div>
                                    <input className="tag-selector editable" type="text" name="languages"
                                        value={this.props.languages} onChange={(e) => {this.props.handleChange(e)}}></input>
                                </div>
                                <div className="skill-category education editable">
                                    <div className="skill-title">WORK</div>
                                    <input className="tag-selector editable" type="text" name="work"
                                        value={this.props.work} onChange={(e) => {this.props.handleChange(e)}}></input>
                                </div>
                            </div>
                            <div className="skill-category transportation editable">
                                <div className="skill-title">TRANSPORTATION</div>
                                <ul className="checkbox-selector editable">
                                    {this.props.transportation.map((items, index) => <div className="checkbox-item" key={index} onClick={() => this.props.handleCheckBox(index)}>
                                        <input type="checkbox" checked={this.props.checkboxList[index]}></input>
                                        <label name={items}>{items}</label>
                                    </div>)}
                                </ul>
                            </div>
                            {!this.props.otherUser &&<div className="button-container">
                                <button className="button-min button-sml" onClick={this.skillsToggle} data-test="skilltoggle__cancel">Cancel</button>
                                <button className="button-cta button-sml" onClick={() => { this.skillsToggle(); this.props.saveSkills() }}>Save</button>
                            </div>}
                        </div>}
                    </div>
                </div>
            </div>
        );


    }
}

export default Skills;