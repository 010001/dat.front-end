import React from "react";
import "./About.css";
import {
    pen
} from '../../../ViewTasks/Fontawesome';

export class About extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            descriptionFlag: true,
        }
    }

    descriptionShow = () => {
        const { descriptionFlag } = this.state;
        return descriptionFlag;
    }

    descriptionEdit = () => {
        return !this.descriptionShow();
    }

    descriptionToggle = async () => {
        this.setState({
            descriptionFlag: !this.state.descriptionFlag
        })
    }


    render() {
        return (
            <div className="splitter-section">
                <div className="splitter-section-name">About</div>
                <div className="splitter-section-content">
                    <div className="splitter-section-content-inner clearfix ">
                        {this.descriptionShow() && <div>
                            {!this.props.otherUser && <div className="pen-container pen2" onClick={this.descriptionToggle} data-test="abouttoggle">
                                <div>{pen}</div>
                            </div>}
                            <div>
                                {this.props.tagLine === "" ? <div></div> : <div className="my-tagLine">
                                    <p>{this.props.tagLine}</p>
                                </div>}
                                {this.props.description === "" ? <div className="skills">Edit your description now.</div> :
                                    <div className="my-description">
                                        <p className="description-more">{this.props.description}</p>
                                    </div>}
                            </div>
                        </div>}
                        {this.descriptionEdit() && 
                            <div className="editingDescription anchovy-form">
                                <div className="anchovy-form-item">
                                    <label>Tagline</label>
                                    <input type="text" maxLength="50" name="tagLine" value={this.props.tagLine}
                                        onChange={this.props.handleChange}></input>
                                </div>
                                <div className="anchovy-form-item">
                                    <label>Description</label>
                                    <textarea name="description" value={this.props.description} onChange={this.props.handleChange}></textarea>
                                </div>
                                {!this.props.otherUser &&<div className="button-container">
                                    <button className="button-min button-sml" onClick={this.descriptionToggle} data-test="abouttoggle__cancel">Cancel</button>
                                    <button className="button-cta button-sml" onClick={() => { this.descriptionToggle(); this.props.saveDescription() }}>Save</button>
                                </div>}
                            
                        </div>}
                    </div>
                </div>
            </div>
        );


    }
}

export default About;