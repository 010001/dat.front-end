import React from "react";
import "./Portfolio.css";
import {
    pen, plus
} from '../../../ViewTasks/Fontawesome';

class Portfolio extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            portfolioFlag: true,
        }
    }

    portfolioShow = () => {
        const { portfolioFlag } = this.state;
        return portfolioFlag;
    }

    portfolioEdit = () => {
        return !this.portfolioShow();
    }

    portfoliotoggle = () => {
        this.setState({
            portfolioFlag: !this.state.portfolioFlag
        })
    }

    render() {
        return (
            <div className="portfolio-section">
                <div className="splitter-section-name splitter-left">Portfolio</div>
                <div className="splitter-section-content" >
                    <div className="splitter-section-content-inner clearfix">
                        {this.portfolioShow() && <div>
                            <div className="pen-container pen3">
                                <div onClick={this.portfoliotoggle}>{pen}</div>
                            </div>
                        </div>}
                        {this.portfolioEdit() && <div>
                                <p>Add items to your portfolio. Upload a maximum of 30 items. File formats accepted include JPG/PNG/PDF/TXT and must not be larger than 5MB.</p>
                                <div className="attachment-input-holder">
                                    <div className="attachment-input">
                                        <button className="attachmentPic">{plus}</button>
                                    </div>
                                </div>
                            <div className="button-container">
                                <button className="button-cta button-sml" onClick={this.portfoliotoggle}>Save</button>
                            </div>

                        </div>}
                    </div>
                </div>
            </div>
        );


    }
}

export default Portfolio;