import React from "react";
import "./UserBasicInformation.css";
import {Link} from "react-router-dom";
import {
        position, pen
} from '../../ViewTasks/Fontawesome';

export class UserBasicInformation extends React.Component {
        constructor(props) {
                super(props);
                this.state = {
                        nameTagFlag: true,
                        taskTabClassname: ['tab half worker small', 'tab half worker small active'],
                        postTabClassname: ['tab half poster small', 'tab half poster small active'],
                        taskReviewtabClassname: ['stats-runner false breakdown', 'stats-runner true breakdown'],
                        postReviewtabClassname: ['stats-sender true breakdown', 'stats-sender false breakdown'],
                }
        }

        handleTaskTab = classname => {
                if (classname === 'tab half worker small active' || classname === 'tab half poster small active') return "";
                let { taskTabClassname, postTabClassname, taskReviewtabClassname, postReviewtabClassname } = this.state;
                [taskTabClassname[0], taskTabClassname[1]] = [taskTabClassname[1], taskTabClassname[0]];
                [postTabClassname[0], postTabClassname[1]] = [postTabClassname[1], postTabClassname[0]];
                [taskReviewtabClassname[0], taskReviewtabClassname[1]] = [taskReviewtabClassname[1], taskReviewtabClassname[0]];
                [postReviewtabClassname[0], postReviewtabClassname[1]] = [postReviewtabClassname[1], postReviewtabClassname[0]];
                this.setState({ taskTabClassname, postTabClassname, taskReviewtabClassname, postReviewtabClassname });
        }

        nameTaglineShow = () => {
                const { nameTagFlag } = this.state;
                return nameTagFlag;
        }
        nameTaglineEdit = () => {
                return !this.nameTaglineShow();
        }
        nametoggle = () => {
                this.setState({
                        nameTagFlag: !this.state.nameTagFlag
                })
        }



        render() {
                const updateDate = new Date(this.props.updateTime);
                const createDate = new Date(this.props.createTime);
                return (
                        <div className="splitter-section personals-section">
                                <div className="splitter-section-content">
                                        <div className="splitter-section-content-inner clearfix contentSize">
                                                <div className="personals-holder">
                                                        <div className="user-profile-header">
                                                                <div className="personal-header">
                                                                        <div className="header-image"></div>
                                                                        <div className="avatar-uploader ">
                                                                                <div className="loaderific-not-loading"></div>
                                                                                <div className="avatarImgUser large">
                                                                                        {this.props.userImage === "" ?
                                                                                                <img src="https://via.placeholder.com/256" alt="" width="128" height="128" className="avatorPicture"></img>
                                                                                                : <img src={this.props.userImage} alt="" width="128" height="128" className="avatorPicture" />}
                                                                                     {this.nameTaglineEdit() && <div className="attachment-input">
                                                                                               { !this.props.otherUser && <form method="post">
                                                                                                        <input onChange={(e) => {
                                                                                                                this.props.handleFileChange(e.target.files)
                                                                                                        }} type="file"
                                                                                                                multiple></input>
                                                                                                </form>}
                                                                                                <svg width="42" height="42" viewBox="0 0 42 42" version="1.1" xmlns="http://www.w3.org/2000/svg" xlink="http://www.w3.org/1999/xlink" className="upload-icon" data-ui-test="upload-icon-svg">
                                                                                                        <use x="0" y="0" width="42" height="42" href="/images/icons/icon_definitions.svg#upload"></use></svg>
                                                                                        </div>}
                                                                                </div>
                                                                                <div className="clear"></div>
                                                                        </div>
                                                                </div>
                                                        </div>
                                                        {this.nameTaglineShow() && <div className="name-tagline" id="nameTaglineShow">
                                                                <div className="name" data-ui-test="user-name">{this.props.firstName + " " + this.props.lastName}</div>
                                                                <div className="summary">
                                                                        <div className="last-online">
                                                                                                <div className="last-on">Last online<time itemProp="" dateTime="now"> {updateDate.toLocaleDateString()}</time></div>
                                                                                {position}
                                                                                <span>{this.props.user.address.suburb + " " + this.props.user.address.state + " " + this.props.user.address.country}</span>
                                                                                                <div className="member-since">Member since {createDate.toLocaleDateString()}</div>
                                                                        </div>
                                                                </div>
                                                                {!this.props.otherUser && <div className="pen-container pen1" onClick={this.nametoggle} data-test="nametoggle"><div>{pen}</div></div>}
                                                               <div className="stats">
                                                                        {!this.props.otherUser &&
                                                                        <div className="tabs">
                                                                                <span className={this.state.taskTabClassname[1]} onClick={() => { this.handleTaskTab(this.state.taskTabClassname[1]) }} id="taskTab" data-test="user__tasktab">As a Tasker</span>
                                                                                <span className={this.state.postTabClassname[0]} onClick={() => { this.handleTaskTab(this.state.postTabClassname[0]) }} id="postTab" data-test="user__posttab">As a Poster</span>
                                                                        </div>}
                                                                        <div className="clearFix"></div>
                                                                        <div className={this.state.taskReviewtabClassname[1]} id="taskReview">
                                                                                <div className="reviews-container">
                                                                                        <p className="no-reviews">Looks like you haven’t received any reviews just yet.
                                                    <Link to="/view-tasks" data-test="user__linkToViewTasks">Let’s browse available tasks.</Link>
                                                                                        </p>
                                                                                </div>
                                                                        </div>
                                                                        <div className={this.state.postReviewtabClassname[1]} id="postReview">
                                                                                <div className="reviews-container">
                                                                                        <p className="no-reviews">Looks like you haven’t received any reviews just yet.
                                    <Link to="/" data-test="user__linkToPostTask">Let’s go post a task.</Link>
                                                                                        </p>
                                                                                </div>
                                                                        </div>
                                                                </div>
                                                        </div>}
                                                        {this.nameTaglineEdit() && <div className="name-tagline" id="nameTaglineEdit">
                                                                <div className="anchovy-form">
                                                                        <div className="editable-text anchovy-form-item">
                                                                                <label>First name</label>
                                                                                <input type="text" maxLength="20" name="firstName" value={this.props.firstName}
                                                                                        onChange={this.props.handleChange}></input>
                                                                        </div>
                                                                </div>
                                                                <div className="anchovy-form">
                                                                        <div className="editable-text anchovy-form-item">
                                                                                <label>Last name</label>
                                                                                <input type="text" maxLength="20" name="lastName" value={this.props.lastName}
                                                                                        onChange={this.props.handleChange}></input>
                                                                        </div>
                                                                </div>
                                                                {!this.props.otherUser &&<div className="button-container">
                                                                        <button className="button-min button-sml" onClick={this.nametoggle} data-test="nametoggle__cancel">Cancel</button>
                                                                        <button className="button-cta button-sml" onClick={() => { this.nametoggle(); this.props.saveNameChange() }} >Save</button>
                                                                </div>}
                                                        </div>
                                                        }
                                                </div>
                                        </div>
                                </div >
                        </div>
                );


        }
}

export default UserBasicInformation;
